const config = require('platformsh').config();
const routes = config.routes;
var fs = require("fs");

// Todo: This should be an env var
const configFileName = "./config/environment.json";

for (var url in routes) {
  if (routes.hasOwnProperty(url)) {
    route = routes[url];
    if (route.type == 'upstream' && route.upstream == 'mouseplannerapi') {
      var backendUrl = url;
      var name = config.environment
      break;
    }
  }
}

// Modify this object to match the structure needed by your application.
var configOutput = {
  "url": backendUrl,
  "name": name
};

fs.writeFileSync(configFileName, JSON.stringify(configOutput));