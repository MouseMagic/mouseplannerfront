// Generated on 2016-05-10 using generator-angular 0.14.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {
  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Automatically load required Grunt tasks
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin',
    ngtemplates: 'grunt-angular-templates',
    cdnify: 'grunt-google-cdn'
  });

  // Configurable paths for the application
  var appConfig = {
    app: require('./bower.json').appPath || 'app',
    dist: 'mpservices'
  };

  var appEnvironment = function () {
    if (grunt.file.exists('environment.json')) {
      return grunt.file.readJSON('environment.json');
    }
    return "{'name': 'platform', 'url': 'api'}";
  };

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    yeoman: appConfig,

    //sprites
    sprite: {
      all: {
        src: '<%= yeoman.app %>/images/assets/*.png',
        dest: '<%= yeoman.app %>/images/sprite.png',
        destCss: '<%= yeoman.app %>/styles/_utilities/_var-images.sass',
        padding: 20,
        imgPath: '../../mpservices/images/sprite.png'
      }
    },

    // Jade Templates
    jade: {
        compile: {
            options: {
                client: false,
                pretty: true
            },
            files: [ {
              cwd: '<%= yeoman.app %>/views/jade/partials',
              src: '*.jade',
              dest: '<%= yeoman.app %>/views/partials',
              expand: true,
              ext: '.html'
            }, {
              cwd: '<%= yeoman.app %>/views/jade/general',
              src: '*.jade',
              dest: '<%= yeoman.app %>/views/general',
              expand: true,
              ext: '.html'
            }, {
              cwd: '<%= yeoman.app %>/views/jade/templates',
              src: '*.jade',
              dest: '<%= yeoman.app %>/views/templates',
              expand: true,
              ext: '.html'
            } ]
        }
    },

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
    //  jade:{
    //   files: [
    //      '<%= yeoman.app %>/views/jade/**/*.jade',
    //      '<%= yeoman.app %>/views/jade/partials/*.jade',
    //     '<%= yeoman.app %>/views/jade/general/*.jade',
    //      '<%= yeoman.app %>/views/jade/templates/*.jade'
    //    ],
    //    tasks: ['jade:compile'],
    //    options: {
    //      livereload: '<%= connect.options.livereload %>'
    //    }
    //  },
      injectSass: {
        files: [
          '<%= yeoman.app %>/styles/**/*.sass'
        ],
        tasks: ['injector:sass']
      },
      js: {
        files: ['<%= yeoman.app %>/scripts/{,*/}*.js'],
        tasks: ['newer:jshint:all', 'newer:jscs:all'],
        options: {
          livereload: '<%= connect.options.livereload %>'
        }
      },
      jsTest: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['newer:jshint:test', 'newer:jscs:test', 'karma']
      },
      compass: {
        files: ['<%= yeoman.app %>/styles/{,*/}*.{scss,sass}'],
        tasks: ['compass:server', 'postcss:server']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= yeoman.app %>/{,*/}*.html',
          '.tmp/styles/{,*/}*.css',
          '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9001,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: 'localhost',
        livereload: 35729
      },
      livereload: {
        options: {
          open: true,
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect().use(
                '/node_modules',
                connect.static('./node_modules')
              ),
              connect().use(
                '/.tmp',
                connect.static('./.tmp')
              ),
              connect().use(
                '/mpservices',
                connect.static('./app')
              ),
              connect().use(
                '/app/styles',
                connect.static('./app/styles')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      test: {
        options: {
          port: 9002,
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              connect.static('test'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect().use(
                '/node_modules',
                connect.static('./node_modules')
              ),
              connect().use(
                '/.tmp',
                connect.static('./.tmp')
              ),
              connect().use(
                '/mpservices',
                connect.static('./app')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      dist: {
        options: {
          open: true,
          base: '<%= yeoman.dist %>'
        }
      }
    },

    // Make sure there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= yeoman.app %>/scripts/{,*/}*.js'
        ]
      },
      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: ['test/spec/{,*/}*.js']
      }
    },

    // Make sure code styles are up to par
    jscs: {
      options: {
        config: '.jscsrc',
        verbose: true
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= yeoman.app %>/scripts/{,*/}*.js'
        ]
      },
      test: {
        src: ['test/spec/{,*/}*.js']
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= yeoman.dist %>/{,*/}*',
            '!<%= yeoman.dist %>/.git{,*/}*'
          ]
        }]
      },
      server: '.tmp'
    },

    // Add vendor prefixed styles
    postcss: {
      options: {
        processors: [
          require('autoprefixer-core')({browsers: ['> 1%','last 2 versions']})
        ]
      },
      server: {
        options: {
          map: true
        },
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      }
    },

    // Automatically inject Bower components into the app
    wiredep: {
      app: {
        src: ['<%= yeoman.app %>/index.html', '<%= yeoman.app %>/index.php'],
        ignorePath:  /\.\.\//
      },
      test: {
        devDependencies: true,
        src: '<%= karma.unit.configFile %>',
        ignorePath:  /\.\.\//,
        fileTypes:{
          js: {
            block: /(([\s\t]*)\/{2}\s*?bower:\s*?(\S*))(\n|\r|.)*?(\/{2}\s*endbower)/gi,
              detect: {
                js: /'(.*\.js)'/gi
              },
              replace: {
                js: '\'{{filePath}}\','
              }
            }
          }
      },
      sass: {
        src: ['<%= yeoman.app %>/styles/{,*/}*.{scss,sass}'],
        ignorePath: /(\.\.\/){1,2}bower_components\//
      }
    },

    // Compiles Sass to CSS and generates necessary files if requested
    compass: {
      options: {
        sassDir: '<%= yeoman.app %>/styles',
        cssDir: '.tmp/styles',
        generatedImagesDir: '.tmp/images/generated',
        imagesDir: '<%= yeoman.app %>/images',
        javascriptsDir: '<%= yeoman.app %>/scripts',
        fontsDir: '<%= yeoman.app %>/styles/fonts',
        importPath: './bower_components',
        httpImagesPath: '/images',
        httpGeneratedImagesPath: '/images/generated',
        httpFontsPath: '/styles/fonts',
        relativeAssets: false,
        assetCacheBuster: false,
        raw: 'Sass::Script::Number.precision = 10\n'
      },
      dist: {
        options: {
          generatedImagesDir: '<%= yeoman.dist %>/images/generated'
        }
      },
      server: {
        options: {
          sourcemap: true
        }
      }
    },

    // Renames files for browser caching purposes
    //filerev: {
    //  dist: {
    //    src: [
    //      '<%= yeoman.dist %>/scripts/{,*/}*.js',
    //      '<%= yeoman.dist %>/styles/{,*/}*.css',
    //      '<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
    //      '<%= yeoman.dist %>/styles/fonts/*'
    //    ]
    //  }
    //},

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: '<%= yeoman.app %>/index.html',
      options: {
        dest: '<%= yeoman.dist %>',
        flow: {
          html: {
            steps: {
              js: ['concat', 'uglifyjs'],
              css: ['cssmin']
            },
            post: {}
          }
        }
      }
    },

    // Performs rewrites based on filerev and the useminPrepare configuration
    usemin: {
      html: ['<%= yeoman.dist %>/{,*/}*.html', '<%= yeoman.dist %>/{,*/}*.php'],
      css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
      js: ['<%= yeoman.dist %>/scripts/{,*/}*.js'],
      options: {
        assetsDirs: [
          '<%= yeoman.dist %>',
          '<%= yeoman.dist %>/images',
          '<%= yeoman.dist %>/styles'
        ],
        patterns: {
          js: [[/(images\/[^''""]*\.(png|jpg|jpeg|gif|webp|svg))/g, 'Replacing references to images']]
        }
      }
    },

    // The following *-min tasks will produce minified files in the dist folder
    // By default, your `index.html`'s <!-- Usemin block --> will take care of
    // minification. These next options are pre-configured if you do not wish
    // to use the Usemin blocks.
    // cssmin: {
    //   dist: {
    //     files: {
    //       '<%= yeoman.dist %>/styles/main.css': [
    //         '.tmp/styles/{,*/}*.css'
    //       ]
    //     }
    //   }
    // },
    // uglify: {
    //   dist: {
    //     files: {
    //       '<%= yeoman.dist %>/scripts/scripts.js': [
    //         '<%= yeoman.dist %>/scripts/scripts.js'
    //       ]
    //     }
    //   }
    // },
    // concat: {
    //   dist: {}
    // },

    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.{png,jpg,jpeg,gif}',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.svg',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },

    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true,
          conservativeCollapse: true,
          collapseBooleanAttributes: true,
          removeCommentsFromCDATA: true
        },
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>',
          src: ['*.html'],
          dest: '<%= yeoman.dist %>'
        }]
      }
    },

    ngtemplates: {
      dist: {
        options: {
          module: 'mpservicesApp',
          htmlmin: '<%= htmlmin.dist.options %>',
          usemin: 'scripts/scripts.js'
        },
        cwd: '<%= yeoman.app %>',
        src: 'views/{,*/}*.html',
        dest: '.tmp/templateCache.js'
      }
    },

    // ng-annotate tries to make the code safe for minification automatically
    // by using the Angular long form for dependency injection.
    ngAnnotate: {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat/scripts',
          src: '*.js',
          dest: '.tmp/concat/scripts'
        }]
      }
    },

    // Replace Google CDN references
    cdnify: {
      dist: {
        html: [
          '<%= yeoman.dist %>/*.html',
          '<%= yeoman.dist %>/*.php'
        ]
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: '<%= yeoman.dist %>',
          src: [
            '*.{ico,png,txt}',
            '*.html',
            '*.php',
            'views/{,*/}*.html',
            'images/{,*/}*.{webp}',
            'fonts/{,*/}*.*',
            'file/{,*/}*.*',
            'file/{,*/}*.*',
            'images/**/*.*',
            'json/*.*',
            'json/scraper/*.*'
          ]
        }, {
          expand: true,
          cwd: '.tmp/images',
          dest: '<%= yeoman.dist %>/images',
          src: ['generated/*']
        }, {
          expand: true,
          cwd: '.',
          src: 'bower_components/bootstrap-sass-official/assets/fonts/bootstrap/*',
          dest: '<%= yeoman.dist %>'
        }]
      },
      styles: {
        expand: true,
        cwd: '<%= yeoman.app %>/styles',
        dest: '.tmp/styles/',
        src: '{,*/}*.css'
      }
    },

    buildcontrol: {
      options: {
        dir: 'dist',
        commit: true,
        push: true,
        connectCommits: false,
        message: 'Built %sourceName% from commit %sourceCommit% on branch %sourceBranch%'
      },
      heroku: {
        options: {
          //remote: 'heroku',
          remote: 'git@heroku.com:mouse-planner.git',
          branch: 'master'
        }
      }
    },

    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: {
        tasks: [
          'compass:server'
        ]
      },
      test: {
        tasks: [
          'compass'
        ]
      }
    },

    // Test settings
    karma: {
      unit: {
        configFile: 'test/karma.conf.js',
        singleRun: true
      }
    },

    injector: {
      options: {

      },

      // Inject component sass into main.scss
      sass: {
        options: {
          transform: function(filePath) {
            console.log(filePath);
            filePath = filePath.replace('app/styles/', '');
            filePath = filePath.replace('app/styles/01-atoms/', '');
            filePath = filePath.replace('app/styles/02-molecules/', '');
            filePath = filePath.replace('app/styles/03-organisms/', '');
            filePath = filePath.replace('app/styles/04-templates/', '');
            filePath = filePath.replace('app/styles/05-pages/', '');
            return '@import \'.' + filePath + '\';';
          },
          starttag: '// injector:sass',
          endtag: '// endinjector:sass'
        },
        files: {
          '<%= yeoman.app %>/styles/main.scss': [
            '<%= yeoman.app %>/styles/**/_index.sass',
            '!<%= yeoman.app %>/styles/main.scss'
          ]
        }
      }
    },
    env: appEnvironment(),
    ngconstant: {
      // Options for all targets
      options: {
        space: '  ',
        wrap: '"use strict";\n\n {\%= __ngModule %}',
        name: 'config'
      },
      // Environment targets
      environment: {
        options: {
          dest: '<%= yeoman.app %>/scripts/config.js'
        },
        constants: {
          ENV: {
            name: '<%= env.name %>',
            apiEndpoint: '<%= env.url %>'
          }
        }
      },
      // developAltexsoft: {
      //   options: {
      //     dest: '<%= yeoman.app %>/scripts/config.js'
      //   },
      //   constants: {
      //     ENV: {
      //       name: 'develop',
      //       apiEndpoint: '//api-kristin-msplnr-dev.webdev2.altexsoft.pro',
      //       client_secret: 'CPmqTymY4dtYyjUwLF5HRNrHgkH7Rtrj1E6m9Fd5'
      //     }
      //   }
      // }
        developAltexsoft: {
            options: {
                dest: '<%= yeoman.app %>/scripts/config.js'
            },
            constants: {
                ENV: {
                    name: 'develop',
                    apiEndpoint: '//mouseplanner-api.192.168.102.138.xip.io',
                    client_secret: 'eGWN8Lb130sAsun2OCqUFcF5WyF47XOpnUvKRhw5'
                }
            }
        }
      //   developAltexsoft: {
      //       options: {
      //           dest: '<%= yeoman.app %>/scripts/config.js'
      //       },
      //       constants: {
      //           ENV: {
      //               name: 'develop',
      //               apiEndpoint: '//mouseplanner.192.168.102.218.xip.io',
      //               client_secret: 'eGWN8Lb130sAsun2OCqUFcF5WyF47XOpnUvKRhw5'
      //           }
      //       }
      //   }
    }
  });

  grunt.registerTask('serve', 'Compile then start a connect web server', function (target, ENV_NAME) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'clean:server',
      ENV_NAME ? 'ngconstant:' + ENV_NAME + '' : 'ngconstant:environment',
      'wiredep',
      'concurrent:server',
      'postcss:server',
      'connect:livereload',
      'injector:sass',
      'injector',
      'watch'
    ]);
  });

  grunt.registerTask('server', 'DEPRECATED TASK. Use the "serve" task instead', function (target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve:' + target]);
  });

  grunt.registerTask('test', [
    'clean:server',
    'wiredep',
    'concurrent:test',
    'postcss',
    'connect:test',
    'karma'
  ]);

  grunt.registerTask('build', 'Create app build', function (ENV_NAME) {
    grunt.task.run(
      [
        'clean:dist',
        ENV_NAME ? 'ngconstant:' + ENV_NAME + '' : 'ngconstant:environment',
        'injector:sass',
        'injector',
        'wiredep',
        'useminPrepare',
        'compass:dist',
        'imagemin',
        'svgmin',
        'postcss',
        'ngtemplates',
        'concat',
        'ngAnnotate',
        'copy:dist',
        'cdnify',
        'cssmin',
        'uglify',
        //'filerev',
        'usemin',
        'htmlmin'
      ]
    );
  });

  grunt.registerTask('default', [
    'newer:jshint',
    'newer:jscs',
    'test',
    'build'
  ]);
};
