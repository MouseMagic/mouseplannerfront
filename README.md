# Mouse Planner - Environment Config
## Environment values
Common environment values include:

* 'production' (the live website)
* 'staging' (the test website for client review)
* 'development' (the local development copy of the website)

## Setting the environment

Copy the `environment_example.json` file to `environment.json` and set your API environment values.

For example:
```
{
  "name": "develop",
  "url": "//mouseplanner.dev.bixal.net"
}
```

Execute
```
bower install
npm install
```

Set the AngularJS version to _(or closer to)_ **1.6.7**

### Run locally
Execute `grunt serve`

### Build
Execute `grunt build`
