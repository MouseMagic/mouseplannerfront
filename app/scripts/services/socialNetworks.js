'use strict';

/**
 * @ngdoc service
 * @name mpservicesApp.vacation
 * @description
 * # vacation
 * Service in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .factory('socialNetworks', function (mousePlannerService, $q, $window) {
    var serviceInstance;

    // Init Networks scripts
    function initNetworksScripts(socialConfig) {
      // Include google login library
      if (socialConfig.google) {
        (function() {
          // Add google script
          var po = document.createElement('script');
          po.type = 'text/javascript';
          po.async = true;
          po.defer = true;
          po.src = 'https://apis.google.com/js/platform.js';
          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(po, s);

          // Add google meta tag
          var el = document.createElement('meta');
          el.name = 'google-signin-client_id';
          el.content = socialConfig.google;
          var firstMeta = document.getElementsByTagName('meta')[0];
          firstMeta.parentNode.insertBefore(el, firstMeta);
        })();
      }

      // Include facebook library
      if (socialConfig.facebook) {
        window.fbAsyncInit = function() {
          window.FB.init({
            appId            : socialConfig.facebook,
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v2.11'
          });
        };
        (function(d, s, id){
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) {return;}
          js = d.createElement(s); js.id = id;
          js.src = "https://connect.facebook.net/en_US/sdk.js";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
      }
    }

    // Get social networks keys from backend
    $q.all({google:mousePlannerService.user.getGoogleApiId(), facebook: mousePlannerService.user.getFacebookApiId()}).then(
      function (res) {
        if (res) {
          initNetworksScripts(res);
        }
      },
      function (err) {}
    );

    // Service instance
    serviceInstance = {
      init: function(googleCallback, googleOnFailureCallback) {
        function checkGoogleApi() {
          if ($window.gapi) {
            $window.gapi.signin2.render('my-signin2', {
              'scope': 'profile email',
              'width': 240,
              'height': 50,
              'longtitle': true,
              'theme': 'dark',
              'onsuccess': googleCallback,
              'onfailure': googleOnFailureCallback
            });
            clearInterval(intervalId);
          }
        }
        var intervalId = setInterval(checkGoogleApi, 100);
      },
      googleLogin:function(googleToken){
        return mousePlannerService.user.registrationGoogle({token: googleToken});
      },
      facebookLogin:function(){
        return new Promise(function (resolve, reject) {
          window.FB.login(function(response) {
            if (response.authResponse) {
              var tokenResponse = window.FB.getAuthResponse();
              if (tokenResponse && tokenResponse.accessToken) {
                mousePlannerService.user.registrationFacebook({token: tokenResponse.accessToken})
                  .then(
                    function(res){
                      resolve(res);
                    },
                    function(err){
                      reject(err);
                    }
                  );
              }
            } else{
              reject({popupClose: true});
            }
          }, {scope: 'email'});
        });
      }
    };

    return serviceInstance;
});
