'use strict';

/**
 * @ngdoc service
 * @name dashboardService
 * @description
 * # config
 * Service in the dashboardService.
 */
angular.module('mpservicesApp')
    .service('dashboardService', function (mousePlannerService) {
        var page = {
             getResultsPage:  function (pageNumber, searchPlans, filterSelect) {
                 return mousePlannerService.vacation.searchVacations(pageNumber, searchPlans, filterSelect)
                    .then(function(response) {
                        // get all vacations from page
                        var vacations = [];
                        var allPlans = response.data;

                        for( var i in allPlans ){
                            if (typeof allPlans[i].user !== "undefined") {
                                if (typeof allPlans[i].user.vacations !== "undefined") {
                                    vacations = vacations.concat(allPlans[i].user.vacations);
                                }
                            }
                        }

                        var allData = {
                            currentPage: response.currentPage,
                            plansPerPage: response.perPage,
                            vacations: vacations,
                            totalPlans: response.total,
                            sortVacation: response.data
                        };

                        return allData;
                    });
            }
        };

        return page;
    });