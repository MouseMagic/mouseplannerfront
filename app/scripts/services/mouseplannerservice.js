'use strict';

/**
 * @ngdoc service
 * @name mpservicesApp.mousePlannerService
 * @description
 * # mousePlannerService
 * Service in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .service('mousePlannerService', function ($http, $q, $window, $filter, configPage, Auth) {

    //var urlService = '/wp-json/mouseplanner/v1';

    var user = {
      login: function(data){
        var def = $q.defer();
        $http.post(configPage.urlService + '/oauth/token', angular.extend(configPage.access, data))
        .then(
          function(res){
              if (res.data && res.data.access_token) {
                Auth.setUserToken(res.data.access_token);
              }
            def.resolve(res.data);
          },
          function(err){
            Auth.logoutUser();
            def.reject(err.data);
          }
        );
        return def.promise;
      },
      logout: function(){
        return $http.post(configPage.urlService + '/api/users/logout', {}).then(
          function(res){
            Auth.logoutUser();
            return (res.data);
          },
          function(err){
            Auth.logoutUser();
            return ($q.reject(err.data));
          }
        );
      },
      registration: function(data){
        return $http.post(configPage.urlService + '/api/users/register', data).then(
          function(res){
            return (res.data);
          },
          function(err){
            return ($q.reject(err.data));
          }
        );
      },
      registrationVerify: function(id){
        return $http.get(configPage.urlService + '/api/users/register/verify/' + id).then(handleSuccess, handleError);
      },
      getGoogleApiId: function(){
        return $http.get(configPage.urlService + '/api/auth/google/client-id').then(handleSuccess, handleError);
      },
      getFacebookApiId: function(){
        return $http.get(configPage.urlService + '/api/auth/facebook/client-id').then(handleSuccess, handleError);
      },
      registrationGoogle: function(id_token){
        return $http.post(configPage.urlService + '/api/auth/google', id_token).then(
          function(res){
            $window.sessionStorage.appUserToken = res.data.access_token;
            return (res.data);
          },
          function(err){
            if($window.sessionStorage.appUserToken){
              delete $window.sessionStorage.appUserToken;
            }
            return ($q.reject(err));
          }
        );
      },
      registrationFacebook: function(id_token){
        return $http.post(configPage.urlService + '/api/auth/facebook', id_token).then(
          function(res){
            $window.sessionStorage.appUserToken = res.data.access_token;
            return (res.data);
          },
          function(err){
            if($window.sessionStorage.appUserToken){
              delete $window.sessionStorage.appUserToken;
            }
            return ($q.reject(err));
          }
        );
      },
      passwordResetEmail: function(data){
        return $http.post(configPage.urlService + '/api/password/email', data).then(
          function(res){
            return (res.data);
          },
          function(err){
            return ($q.reject(err));
          }
        );
      },
      passwordReset: function(data){
        return $http.post(configPage.urlService + '/api/password/reset', data).then(
          function(res){
            return (res.data);
          },
          function(err){
            return ($q.reject(err));
          }
        );
      },
      get: function(id){
        if(id){
          return $http.get(configPage.urlService + '/api/users/'+id).then(handleSuccess, handleError);
        }
        else{
          return $http.get(configPage.urlService + '/api/users').then(handleSuccess, handleError);
        }
      },
      me: function(){
        return $http.get(configPage.urlService + '/api/users/me').then(handleSuccess, handleError);
      }
    };

    var subscription = {
      getStripePublicKey: function(){
        return $http.get(configPage.urlService + '/api/stripe/api-key').then(
          function(res){
            return (res.data);
          },
          function(err){
            return ($q.reject(err));
          }
        );
      },
      getSubscriptionPlans: function(){
        return $http.get(configPage.urlService + '/api/stripe/plans').then(
          function(res){
            return (res.data);
          },
          function(err){
            return ($q.reject(err));
          }
        );
      },
      checkCouponCode: function(couponCode){
        return $http.get(configPage.urlService + '/api/stripe/coupon/'+couponCode).then(
          function(res){
            return (res.data);
          },
          function(err){
            return ($q.reject(err));
          }
        );
      },
      subscribe: function(data){
        return $http.post(configPage.urlService + '/api/stripe/subscribe', data).then(
          function(res){
            return (res.data);
          },
          function(err){
            return ($q.reject(err));
          }
        );
      }
    };

    var trip = {
      get: function(id) {
        if(id){
          return $http.get(configPage.urlService + '/api/trips/' + id).then(handleSuccess, handleError);
        }
        else{
          return $http.get(configPage.urlService + '/api/trips').then(handleSuccess, handleError);
        }
      },
      create: function(element) {
        return $http.post(configPage.urlService + '/api/trips', element).then(handleSuccess, handleError);
      },
        update: function (element) {
            return $http.put(configPage.urlService + '/api/trips/' + element.id, element).then(handleSuccess, handleError);
        },
      delete: function(id){
        return $http.delete(configPage.urlService + '/api/trips/' + id).then(handleSuccess, handleError);
      }
    };

  	var vacation = {
  		get: function(id){
        if(id){
          return $http.get(configPage.urlService + '/api/vacations/' + id).then(handleSuccess, handleError);
        }
        else{
          return $http.get(configPage.urlService + '/api/vacations').then(handleSuccess, handleError);
        }
  		},
  		create: function(element){
        return $http.post(configPage.urlService + '/api/vacations', element).then(handleSuccess, handleError);
  		},
  		update: function(element){
        if(!element.highestStepCompleted || element.highestStepCompleted < element.stepNumber){
          element.highestStepCompleted = element.stepNumber;
        }

        return $http.put(configPage.urlService + '/api/vacations/' + element.id, element).then(handleSuccess, handleError);
      },
  		delete: function(element){
        return $http.delete(configPage.urlService + '/api/dashboard/vacations/' + element.id + '/delete').then(handleSuccess, handleError);
      },
      mouseplan: function (id){
        return $http.get(configPage.urlService + '/api/wizard/vacations/' + id + '/mouseplans').then(handleSuccess, handleError);
      },
      mousages: function(vacationId){
          return $http.get(configPage.urlService + '/api/mousages/vacations/' + vacationId).then(handleSuccess, handleError);
      },
      recommendations: function(id){
        return $http.get(configPage.urlService + '/api/wizard/vacations/' + id + '/parks').then(handleSuccess, handleError);
      },
      wishlist: function(){
        return $http.get(configPage.urlService + '/api/wizard/wishlist').then(handleSuccess, handleError);
      },
      park: function(id, parkId){
        return $http.get(configPage.urlService + '/api/parks/' + parkId + '/vacation/' + id + '/schedule').then(handleSuccess, handleError);
      },
      duplicate: function(element){
        return $http.post(configPage.urlService + '/api/dashboard/vacations/' + element.id + '/duplicate', element).then(handleSuccess, handleError);
      },
      experiences: function(id){
        return $http.get(configPage.urlService + '/api/wizard/vacations/' + id + '/experiences').then(handleSuccess, handleError);
      },
      defaultExperiences: function(){
        return $http.get(configPage.urlService + '/api/wizard/default_experiences').then(handleSuccess, handleError);
      },
        searchVacations: function (pageNumber, searchPlans, filterSelect) {
            var _pageNumber = pageNumber ? pageNumber : 1;
            var _searchPlans = searchPlans ? searchPlans : '';
            var _filterSelect = filterSelect ? filterSelect : 'startTravelDate';
            return $http.get(configPage.urlService + '/api/vacations?page=' + _pageNumber + '&search=' + _searchPlans + '&sort=' + _filterSelect).then(handleSuccess, handleError);
        }
  	};

    var mouseplan = {
      get: function (id){
        return $http.get(configPage.urlService + '/api/mouseplans/' + id).then(handleSuccess, handleError);
      },
      create: function(element){
        return $http.post(configPage.urlService + '/api/wizard/vacations/' + element.vacation_id + '/mouseplans', element).then(handleSuccess, handleError);
      },
      update: function(element){
        return $http.put(configPage.urlService + '/api/mouseplans/' + element.id, element).then(handleSuccess, handleError);
      }
    };

    var block = {
      get: function(blockId){
        if(blockId){
          return $http.get(configPage.urlService + '/api/blocks/' + blockId).then(handleSuccess, handleError);
        }
        else{
          return $http.get(configPage.urlService + '/api/blocks').then(handleSuccess, handleError);
        }
      },
      create: function(block){
        return $http.post(configPage.urlService + '/api/blocks', block).then(handleSuccess, handleError);
      },
      update: function(block){
        return $http.put(configPage.urlService + '/api/blocks/' + block.id, block).then(handleSuccess, handleError);
      },
      delete: function(blockId){
        return $http.delete(configPage.urlService + '/api/blocks/' + blockId).then(handleSuccess, handleError);
      }
    };

    var block_type = {
      get: function(blockTypeId){
        if(blockTypeId){
          return $http.get(configPage.urlService + '/api/block_types/' + blockTypeId).then(handleSuccess, handleError);
        }
        else{
          return $http.get(configPage.urlService + '/api/block_types').then(handleSuccess, handleError);
        }
      }
    };

    var mousage = {
      update: function(element){
        return $http.put(configPage.urlService + '/api/mousages/' + element.id, element).then(handleSuccess, handleError);
      }
    };

    var checklist = {
      get: function() {
        return $http.get(configPage.urlService + '/api/checklists').then(handleSuccess, handleError);
      },
      create: function(element){
        return $http.post(configPage.urlService + '/api/mousages/' + element.id + '/checklist', element).then(handleSuccess, handleError);
      },
      update: function(element){
        return $http.put(configPage.urlService + '/api/checklists/' + element.id, element).then(handleSuccess, handleError);
      },
      delete: function(id){
        return $http.delete(configPage.urlService + '/api/checklists/' + id).then(handleSuccess, handleError);
      }
    };

    var note = {
      get: function(id, type){
        type = type || 'blocks'; //valid = blocks, days
        return $http.get(configPage.urlService + '/api/notes/'+type+'/' + id).then(handleSuccess, handleError);
      },
      post: function(element){
        return $http.post(configPage.urlService + '/api/notes', element).then(handleSuccess, handleError);
      },
      put: function(element){
        return $http.put(configPage.urlService + '/api/notes/' + element.id, element).then(handleSuccess, handleError);
      },
      delete: function(id){
        return $http.delete(configPage.urlService + '/api/notes/' + id).then(handleSuccess, handleError);
      }
    };

    var traveler = {
      get: function(vacationId){
        return $http.get(configPage.urlService + '/api/wizard/vacations/' + vacationId + '/travelers').then(handleSuccess, handleError);
      },
      create: function(vacationId, element){
        return $http.post(configPage.urlService + '/api/wizard/vacations/' + vacationId + '/travelers', element).then(handleSuccess, handleError);
      },
      update: function(element){
        return $http.put(configPage.urlService + '/api/travelers/' + element.id, element).then(handleSuccess, handleError);
      },
      delete: function(vacationId, travelerId){
        return $http.delete(configPage.urlService + '/api/travelers/' + travelerId, {vacation_id: vacationId}).then(handleSuccess, handleError);
      }
    };

    var fastpass = {
      get: function(id) {
        return $http.get(configPage.urlService + '/api/fastpass_reservations/days/' + id).then(handleSuccess, handleError);
      },
      post: function(element) {
        return $http.post(configPage.urlService + '/api/fastpass_reservations', element).then(handleSuccess, handleError);
      },
      put: function(element) {
        return $http.put(configPage.urlService + '/api/fastpass_reservations/' + element.id, element).then(handleSuccess, handleError);
      },
      delete: function(id) {
        return $http.delete(configPage.urlService + '/api/fastpass_reservations/' + id).then(handleSuccess, handleError);
      }
    };

    var disneyScraper = {
      attractions: function(){
        return getJsonFileOptions('attractions-all');
      },
      entertainments: function (){
        return getJsonFileOptions('entertainments-all');
      },
      events: function(){
        return getJsonFileOptions('events-all');
      },
      tours: function(){
        return getJsonFileOptions('tours-all');
      },
      refurbishments: function(){
        return getJsonFileOptions('refurbishments');
      },
      schedule: function(){
        return getJsonFileOptions('schedules-all');
      }
    };

    function getJsonFileOptions(jsonFile){
      var request = $http({
        method: 'get',
        url: configPage.scraperService + '/' + jsonFile + '.json'
      });
      return (request.then(handleSuccess, handleError));
    }

    var externalService = {
      trip: {
        get: function (tripId){
          var request;
          if(tripId){
            request = $http({
              method: 'get',
              url: configPage.urlService + '/api/trips/' + tripId
            });
          } else {
            request = $http({
              method: 'get',
              url: configPage.urlService + '/api/trips'
            });
          }
          return (request.then(handleSuccess, handleError));
        },
        create: function (element){
          var request = $http({
            method: 'post',
            url: configPage.urlService + '/api/trips',
            data: element
          });
          return (request.then(handleSuccess, handleError));
        }
      },
      vacation: {
        get: function(vacationId){
          var request;
          if(vacationId){
            request = $http({
              method: 'get',
              url: configPage.urlService + '/api/vacations/' + vacationId
            });
          } else {
            request = $http({
              method: 'get',
              url: configPage.urlService + '/api/vacations'
            });
          }
          return (request.then(handleSuccess, handleError));
        },
        create: function (element){
          var request = $http.post(configPage.urlService + '/api/vacations', element);
          return (request.then(handleSuccess, handleError));
        },
        update: function (vacationId, element){
          var request = $http({
            method: 'put',
            url: configPage.urlService + '/api/vacations/' + vacationId,
            data: element
          });
          return (request.then(handleSuccess, handleError));
        }
      },
      mouseplan: {
        get: function (mousePlanId){
          var request;
          if(mousePlanId){
            request = $http({
              method: 'get',
              url: configPage.urlService + '/api/mouseplans/' + mousePlanId
            });
          } else {
            request = $http({
              method: 'get',
              url: configPage.urlService + '/api/mouseplans'
            });
          }
          return (request.then(handleSuccess, handleError));
        },
        update: function (mousePlanId, element){
          var request = $http({
            method: 'put',
            url: configPage.urlService + '/api/mouseplans/' + mousePlanId,
            data: element
          });
          return (request.then(handleSuccess, handleError));
        }
      },
      wizard: {
        vacation: function (vacationId) {
          var wizardVacation = {
            parks: {
              get: function(){
                var request;
                if(vacationId){
                  request = $http({
                    method: 'get',
                    url: configPage.urlService + '/api/wizard/vacations/' + vacationId + '/parks'
                  });
                }
                return (request.then(handleSuccess, handleError));
              }
            },
            mousePlan: {
              get: function (){
                var request;
                if(vacationId){
                  request = $http({
                    method: 'get',
                    url: configPage.urlService + '/api/wizard/vacations/' + vacationId + '/mouseplans'
                  });
                }
                return (request.then(handleSuccess, handleError));
              },
              create: function(element){
                var request = $http({
                  method: 'post',
                  url: configPage.urlService + '/api/wizard/vacations/' + vacationId + '/mouseplans',
                  data: element
                });
                return (request.then(handleSuccess, handleError));
              }
            },
            traveler: {
              create: function (element){
                var request = $http({
                  method: 'post',
                  url: configPage.urlService + '/api/wizard/vacations/' + vacationId + '/travelers',
                  data: element
                });
                return (request.then(handleSuccess, handleError));
              }
            }
          };

          return wizardVacation;
        }
      },
      dashboard: {
        trip: {
          create: function (){

          },
          get: function(){
            var request = $http({
              method: 'get',
              url: configPage.urlService + '/api/dashboard/trips/user'
            });
            return (request.then(handleSuccess, handleError));
          }
        },
        plan: {
          get: function(tripId){
            var request;
            if(tripId){
              request = $http({
                method: 'get',
                url: configPage.urlService + '/api/dashboard/trips/' + tripId + '/plans'
              });
            }
            return (request.then(handleSuccess, handleError));
          }
        },
        duplicate: {
          create: function(element){
            var request = $http({
              method: 'post',
              url: configPage.urlService + '/api/dashboard/vacations/' + element.vacation_id + '/duplicate',
              data: element
            });
            return (request.then(handleSuccess, handleError));
          }
        },
        remove: {
          delete: function(element){
            var request = $http({
              method: 'delete',
              url: configPage.urlService + '/api/dashboard/vacations/' + element.vacation_id + '/delete'
            });
            return (request.then(handleSuccess, handleError));
          }
        }
      },
      checklist: {
        get: function() {
          var request = $http({
            method: 'get',
            url: configPage.urlService + '/api/checklists'
          });
          return (request.then(handleSuccess, handleError));
        },
        put: function(id, element){
          var request = $http({
            method: 'put',
            url: configPage.urlService + '/api/checklists/' + id,
            data: element
          });
          return (request.then(handleSuccess, handleError));
        },
        delete: function(id){
          var request = $http({
            method: 'delete',
            url: configPage.urlService + '/api/checklists/' + id
          });
          return (request.then(handleSuccess, handleError));
        }
      },
      mousage: {
        post: function(id, element){
          var request;
          if(element){
            request = $http({
              method: 'post',
              url: configPage.urlService + '/api/mousages/' + id + '/checklist',
              data: element
            });
          }
          return (request.then(handleSuccess, handleError));
        },
        put: function(id, element){
          var request;
          if(element.id){
            request = $http({
              method: 'put',
              url: configPage.urlService + '/api/mousages/' + id + '',
              data: element
            });
          }
          return (request.then(handleSuccess, handleError));
        },
        get: {
          plan: function(vacationId){
            var request;
            if(vacationId){
              request = $http({
                method: 'get',
                url: configPage.urlService + '/api/mousages/vacations/' + vacationId + ''
              });
            }
            return (request.then(handleSuccess, handleError));
          },
          mouseplan: function(mousePlanId){
            var request;
            if(mousePlanId){
              request = $http({
                method: 'get',
                url: configPage.urlService + '/api/mousages/mouseplans/' + mousePlanId + ''
              });
            }
            return (request.then(handleSuccess, handleError));
          },
          day: function(dayId){
            var request;
            if(dayId){
              request = $http({
                method: 'get',
                url: configPage.urlService + '/api/mousages/days/' + dayId + ''
              });
            }
            return (request.then(handleSuccess, handleError));
          },
          block: function(blockId){
            var request;
            if(blockId){
              request = $http({
                method: 'get',
                url: configPage.urlService + '/api/mousages/blocks/' + blockId + ''
              });
            }
            return (request.then(handleSuccess, handleError));
          }
        }
      },
      notes: {
        get: {
          block: function(id) {
            var request = $http({
              method: 'get',
              url: configPage.urlService + '/api/notes/blocks/' + id + ''
            });
            return (request.then(handleSuccess, handleError));
          },
          day: function(id) {
            var request = $http({
              method: 'get',
              url: configPage.urlService + '/api/notes/days/' + id + ''
            });
            return (request.then(handleSuccess, handleError));
          }
        },
        post: function(element){
          var request;
          if(element){
            request = $http({
              method: 'post',
              url: configPage.urlService + '/api/notes',
              data: element
            });
          }
          return (request.then(handleSuccess, handleError));
        },
        put: function(id, element){
          var request;
          if(id){
            request = $http({
              method: 'put',
              url: configPage.urlService + '/api/notes/' + id + '',
              data: element
            });
          }
          return (request.then(handleSuccess, handleError));
        },
        delete: function(id){
          var request = $http({
            method: 'delete',
            url: configPage.urlService + '/api/notes/' + id
          });
          return (request.then(handleSuccess, handleError));
        }
      },
      parks: {
        get: function(park, vacation){
          var request = $http({
            method: 'get',
            url: configPage.urlService + '/api/parks/' + park + '/vacation/' + vacation + '/schedule'
          });
          return (request.then(handleSuccess, handleError));
        }
      },
      blocks: {
        get: function(blockId){
          var request;
          if(blockId){
            request = $http({
              method: 'get',
              url: configPage.urlService + '/api/blocks/' + blockId
            });
          } else {
            request = $http({
              method: 'get',
              url: configPage.urlService + '/api/blocks/'
            });
          }

          return (request.then(handleSuccess, handleError));
        }
      },
      fastpass: {
        get: function(dayId) {
          var request = $http({
            method: 'get',
            url: configPage.urlService + '/api/fastpass_reservations/days/' + dayId
          });
          return (request.then(handleSuccess, handleError));
        },
        post: function(element) {
          var request = $http({
            method: 'post',
            url: configPage.urlService + '/api/fastpass_reservations',
            data: element
          });
          return (request.then(handleSuccess, handleError));
        },
        put: function(id, element) {
          var request = $http({
            method: 'put',
            url: configPage.urlService + '/api/fastpass_reservations/' + id,
            data: element
          });
          return (request.then(handleSuccess, handleError));
        },
        delete: function(id) {
          var request = $http({
            method: 'delete',
            url: configPage.urlService + '/api/fastpass_reservations/' + id
          });
          return (request.then(handleSuccess, handleError));
        }
      },
      options: {
        travelerHeights: function(){
          return $http.get(configPage.urlService + '/api/traveler_heights');
        },
        prefixTitleTypes: function(){
          return $http.get(configPage.urlService + '/api/prefix_titles');
        },
        accessibilityTypes: function(){
          return $http.get(configPage.urlService + '/api/accessibilities');
        },
        celebrationTypes: function(){
          return $http.get(configPage.urlService + '/api/celebrations');
        },
        resorts: function(){
          return $http.get(configPage.urlService + '/api/resorts');
        },
        diningPlanTypes: function(){
          return $http.get(configPage.urlService + '/api/dining_plans');
        },
        interestVisitParkTypes: function(){
          return $http.get(configPage.urlService + '/api/parks_visit_interests');
        },
        interestWaterParkTypes: function(){
          return $http.get(configPage.urlService + '/api/water_park_interests');
        },
        bookedPlaceTypes: function(){
          return $http.get(configPage.urlService + '/api/booked_places');
        },
        thrillLevelTypes: function(){
          return $http.get(configPage.urlService + '/api/thrill_levels');
        },
        paceTypes: function(){
          return $http.get(configPage.urlService + '/api/paces');
        },
        budgetTypes: function(){
          return $http.get(configPage.urlService + '/api/budgets');
        },
        timeFlexibilityTypes: function(){
          return $http.get(configPage.urlService + '/api/time_flexibilities');
        },
        napTypes: function(){
          return $http.get(configPage.urlService + '/api/naps');
        },
        orlandoHotels: function(){
          return $http.get(configPage.urlService + '/api/orlando_hotels');
        },
        interestTypes: function(){
          return $http.get(configPage.urlService + '/api/interests');
        },
        favoriteCharactersTypes: function(){
          return $http.get(configPage.urlService + '/api/characters');
        },
        diningPreferenceTypes: function(){
          return $http.get(configPage.urlService + '/api/dining_preferences');
        },
        parks: function(){
          return $http.get(configPage.urlService + '/api/parks');
        },
        mapTravelerHeights: function(){
          return $http.get(configPage.urlService + '/api/traveler_heights/map_values');
        },
        mapAccessibilityTypes: function(){
          return $http.get(configPage.urlService + '/api/accessibilities/map_values');
        },
        mapThrillLevelTypes: function(){
          return $http.get(configPage.urlService + '/api/thrill_levels/map_values');
        },
        mapInterestTypes: function(){
          return $http.get(configPage.urlService + '/api/interests/map_values');
        },
        mapFavoriteCharactersTypes: function(){
          return $http.get(configPage.urlService + '/api/characters/map_values');
        },
        mapParkInterest: function(){
          return $http.get(configPage.urlService + '/api/parks/map_values');
        },
        mapAges: function(){
          return $http.get(configPage.urlService + '/api/ages/map_values');
        },
        smallStuff: function(){
          return $http.get(configPage.urlService + '/api/naps');
        },
        customFacets: function(){
          return $http.get(configPage.urlService + '/api/custom_facets');
        },
        experienceExceptions: function(){
          return $http.get(configPage.urlService + '/api/experience_exceptions');
        },
        parkExceptions: function(){
          return $http.get(configPage.urlService + '/api/parks_exceptions');
        }
      }
    };

    var restaurants = {
      resort: function (resortId) {
        var request = $http({
          method: 'get',
          url: configPage.urlService + '/api/restaurants/resorts/' + resortId
        });
        return (request.then(handleSuccess, handleError));
      },
      park: function(park) {
        var request = $http({
          method: 'get',
          url: configPage.urlService + '/api/restaurants/parks/' + park
        });
        return (request.then(handleSuccess, handleError));
      }
    };

  	// Methods
    function handleError( response ) {
        if (
            !angular.isObject(response.data) ||
            !response.data.message
            ) {
            return ($q.reject('An unknown error occurred.'));
        }
        return ($q.reject(response.data.message));
    }

    function handleSuccess(response) {
      if (response && response.data) {
        return (response.data);
      } else {
        return ({});
      }
    }

    return ({
      user: user,
      trip: trip,
    	vacation: vacation,
      mouseplan: mouseplan,
      block: block,
      block_type: block_type,
      mousage: mousage,
      checklist: checklist,
      note: note,
      traveler: traveler,
      fastpass: fastpass,
      disneyScraper: disneyScraper,
      externalService: externalService,
      restaurants: restaurants,
      subscription: subscription
    });

  });
