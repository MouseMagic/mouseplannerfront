'use strict';

/**
 * @ngdoc service
 * @name mpservicesApp.vacation
 * @description
 * # vacation
 * Service in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .service('vacationSession', function (mousePlannerService, $q, $filter, $location) {

    var optionsLoaded = false;

    function _addTravelersForDefault(firstname, lastname) {

      vacation.travelers.data = [];

      // add User current
      var userCurrent = {
        active: true,
        userCurrent: true,
        data: {
          firstname: firstname,
          lastname: lastname,
        },
        accessibility: [],
        accessibilityTypes: angular.copy(vacation.options.accessibilityTypes)
      };

      vacation.travelers.functions.create(userCurrent.data);

      // other guest
      setTimeout(function () {
        var otherGuest = {
          active: false,
          data: {
            firstname: '',
            lastname: ''
          },
          accessibility: [],
          accessibilityTypes: angular.copy(vacation.options.accessibilityTypes)
        };

        vacation.travelers.functions.create(otherGuest.data);
      }, 1000);
    }

    var vacation = {
      trip: undefined,
      data: {
        stepNumber: 0
      },
      functions: {
        addTravelersDefault: _addTravelersForDefault,
        create: function(element, callbackSaveVacation){

          function saveVacation(item){
            mousePlannerService.vacation.create(item).then(function(response){

              if(response){
                response.startTravelDate = $filter('parsedate')(response.startTravelDate);
                response.endTravelDate = $filter('parsedate')(response.endTravelDate);
                response.departureDate = $filter('parsedate')(response.departureDate);
                response.arrivalDate = $filter('parsedate')(response.arrivalDate);
                response.beginDayId = $filter('parsedate')(response.beginDayId);
                response.endDayId = $filter('parsedate')(response.endDayId);
                
                angular.extend(vacation.data, response);

                item.user_id = 1;

                if(item.celebration_types){

                  item.vacation_id = response.vacation_id;
                  vacation.functions.update(item, callbackSaveVacation);
                } else {
                  callbackSaveVacation();
                }

              }
            });
          }

          if(vacation.trip === undefined){
            var newTrip = {
              name: element.name,
              last_visit: element.lastVisit,
              trip_count: element.tripCountId
            };

            mousePlannerService.trip.create(newTrip).then(function(response){

              if(response){
                angular.extend(vacation.data, response);
                callbackSaveVacation();
              }
            });
          } else {
            element.trip_id = vacation.trip.trip_id;
            saveVacation(element);
          }
        },
        update: function(item, callbackSaveVacation){

          if(item.highestStepCompleted &&
          item.highestStepCompleted > item.stepNumber){
            item.stepNumber = item.highestStepCompleted;
          }
          else if(item.stepNumber > item.highestStepCompleted){
            item.highestStepCompleted = item.stepNumber;
          }

          mousePlannerService.vacation.update(item).then(function(response){
            if(response){
              if(vacation.data.experiences && vacation.data.experiences.length > 0){
                mousePlannerService.experiences.create(vacation.data.id, vacation.data.experiences).then(function(resExperiences){
                });
              }

              callbackSaveVacation();
            }
          });
        }
      },
      travelers: {

        data: [],
        functions: {
          create: function(item){
            if(vacation.data && vacation.data.hasOwnProperty('id') &&
              parseInt(vacation.data.id)){
              var vacationId = parseInt(vacation.data.id);
              mousePlannerService.traveler.create(vacationId, item).then(function(responseTrav){
                if(responseTrav){
                  angular.extend(item, responseTrav);
                  var data = {
                    data: item,
                    accessibility: [],
                    accessibilityTypes: angular.copy(vacation.options.accessibilityTypes)
                  };
                  vacation.travelers.data.push(data);
                }
              });
            }
          },
          update: function(item){
            mousePlannerService.traveler.update(item).then(function(responseTrav){
              if(responseTrav){
                angular.extend(item, responseTrav);
              }
            });
          },
          delete: function(index, item){
            var element = {
              vacation_id: vacation.data.id
            };
            mousePlannerService.traveler.delete(element, item.traveler_id).then(function(responseTrav){
              vacation.travelers.data.splice(index, 1);
            });
          },
        }
      },

      favoriteCharactersType: {
        data: [],
        functions: {
          create: function(item){
            var favoriteCharacterType = {
              favoriteCharactersTypeId: item.id
            };

            if(vacation.data && vacation.data.hasOwnProperty('id') &&
              parseInt(vacation.data.id)) {
              var vacationId = parseInt(vacation.data.id);
              mousePlannerService.vacationFavoriteCharactersTypes.create(vacationId, favoriteCharacterType).then(function(responseFavType){
                if(responseFavType){
                  item.vacationFavoriteCharacterTypeId = responseFavType.id;
                  vacation.favoriteCharactersType.data.push(responseFavType);
                }
              });
            }
          },
          delete: function(item){
            if(vacation.data && vacation.data.hasOwnProperty('id') &&
              parseInt(vacation.data.id)){
              mousePlannerService.vacationFavoriteCharactersTypes.delete(item.vacationFavoriteCharacterTypeId).then(function(responseFavType){
                if(responseFavType){
                  var indexDelete = -1;
                  for(var i = 0; vacation.favoriteCharactersType.data.length > i; i++){
                    if(vacation.favoriteCharactersType.data[i].id === item.vacationFavoriteCharacterTypeId ||
                      vacation.favoriteCharactersType.data[i].id === '' + item.vacationFavoriteCharacterTypeId){
                      indexDelete = i;
                      break;
                    }
                  }
                  vacation.favoriteCharactersType.data.splice(indexDelete, 1);
                  item.vacationFavoriteCharacterTypeId = undefined;
                }
              });
            }
          }
        }
      },
      parkInterest: {
        data: [],
        functions: {
          get: function(){
            var values = $filter('filter')(vacation.options.parkInterest, function(value) {
              if(value.hearts === undefined || isNaN(parseInt(value.hearts)) || parseInt(value.hearts) === 0){
                return false;
              }
              return (value.id !== 4 && value.id !== '4' && value.id !== 6 && value.id !== '6');
            });

            return $filter('orderBy')(values, '"hearts"');
          },
        }
      },
      park: {
        data: [],
        interests: {
          get: function(experience){
            return experience.facets.interests.concat($filter('filter')(vacation.options.customFacets, { disneyId: experience.id, type:'interests' }));
          }
        },
        functions: {
          get: function(parkInterestId, mpConfig){

            var mappedValues = $filter('filter')(vacation.options.mapParkInterest, { parkInterestId: parkInterestId }, true); //get all mappings that match current park

            var parks = $filter('filter')(vacation.options.park, function(experience){
              var locs = (experience.relatedLocations && experience.relatedLocations.primaryLocations) ? experience.relatedLocations.primaryLocations : [experience];
  						return $filter('filter')(mappedValues, function(mappingItem){
  							return $filter('filter')(locs, function(loc){
  								return (loc.links && (loc.links.ancestorThemePark && loc.links.ancestorThemePark.title === mappingItem.disneyParkInterestValue) || (loc.links.ancestorResortArea && loc.links.ancestorResortArea.title === mappingItem.disneyParkInterestValue)); //match any mapped disney values to this experience values
  							}).length;
  						}).length;
            });

            if(mpConfig.filterConfig.excludeParksWater){
              parks = $filter('filter')(parks, { id: '!entityType=water-parks' });
            }
            if(mpConfig.filterConfig.excludeDisneySprings){
              parks = $filter('filter')(parks, { id: '!entityType=entertainment-venue' });
            }
            if(mpConfig.filterConfig.excludeSmallStuff){
              parks = $filter('filter')(parks, vacation.filters.smallStuff);
            }
            if(mpConfig.filterConfig.excludeTours){
              parks = $filter('filter')(parks, vacation.filters.tours);
            }

            var refurbishments = $filter('filter')(parks, vacation.filters.refurbishment);
            var experienceExceptions = $filter('filter')(refurbishments, vacation.filters.excludeExperience);


            var parksFilterAge = $filter('filter')(experienceExceptions, vacation.filters.age);
            var parksFilterHeight = $filter('filter')(parksFilterAge, vacation.filters.height);
            var parksFilterThrillLevel = $filter('filter')(parksFilterHeight, vacation.filters.thrillFactor);


            var orderingFavoriteCharacters = $filter('orderBy')(parksFilterThrillLevel, vacation.ordering.favoriteCharacters, false);
            var orderingClassicCharacters = $filter('orderBy')(orderingFavoriteCharacters, vacation.ordering.interests, false);


            var orderNames = $filter('orderBy')(orderingClassicCharacters, 'name', true);
            var orderingFastPass = $filter('orderBy')(orderNames, vacation.ordering.fastpass, false);
            var orderingWhatsNew = $filter('orderBy')(orderingFastPass, vacation.ordering.whatsNew, true);


            return orderingWhatsNew;
          },
          facets: {
            interests: function(experience){
              var custom = $filter('filter')(vacation.options.customFacets, { disneyId: experience.id, type: 'interests' });
              var concat = experience.facets.interests.concat(custom);
              return concat;
            }
          }
        }
      }
    };

    function _loadVacation(vacationId){
      mousePlannerService.vacation.get(vacationId).then(function(respVacation){

        if (respVacation){

          respVacation.startTravelDate = $filter('parsedate')(respVacation.startTravelDate);
          respVacation.endTravelDate = $filter('parsedate')(respVacation.endTravelDate);
          respVacation.departureDate = $filter('parsedate')(respVacation.departureDate);
          respVacation.arrivalDate = $filter('parsedate')(respVacation.arrivalDate);
          respVacation.beginDayId = $filter('parsedate')(respVacation.beginDayId);
          respVacation.endDayId = $filter('parsedate')(respVacation.endDayId);

          vacation.data = respVacation;

          if(vacation.data.beginDayTime !== undefined &&
          vacation.data.beginDayTime !== null){
            vacation.data.beginDayTime = vacation.data.begin_day_date_time; //moment(vacation.data.begin_day_date_time, 'YYYY-MM-DD HH:mm:ss');
          }

          if(vacation.data.endDayTime !== undefined &&
          vacation.data.endDayTime !== null){
            vacation.data.endDayTime = vacation.data.end_day_date_time; // new Date(); //moment(vacation.data.endDayTime, 'HH:mm:ss');
          }

          if(vacation.data.isFlying){
            vacation.data.isFlying = vacation.data.isFlying === 1 ? 'true': 'false';
          }

          vacation.data.highestStepCompleted = vacation.data.stepNumber;

          _loadVacationCelebrationType();
          _loadInterestTypes();
          _loadFavoriteCharactersTypes();
          _loadDiningPreferenceTypes();
          _loadParkInterest();

          if(parseInt(vacation.data.stepNumber) < 6){
            var steps = ['start', 'guest', 'travel', 'preferences', 'activities', 'wishlist'];
            $location.path('/vacation/' + vacation.data.id + '/' + steps[parseInt(vacation.data.stepNumber)]);
          } else {
            $location.path('/scheduler/' + vacation.data.id);
          }

        }
      });
    }

    function _loadVacationCelebrationType(){
      if(vacation.data.celebration_types){
        vacation.celebrationTypes.data = vacation.data.celebration_types;
        if(vacation.data.celebration_types && vacation.data.celebration_types.length > 0){
          var celebration_types = '';
          for(var i = 0; i < vacation.data.celebration_types.length; i++){
            if(celebration_types === ''){
              celebration_types = '' + vacation.data.celebration_types[i].celebration_type_id;
            } else {
              celebration_types = celebration_types + ',' + vacation.data.celebration_types[i].celebration_type_id;
            }
          }
          vacation.data.celebration_types = celebration_types;
        }
      }
    }

    function _loadTravelers(vacationId){
      mousePlannerService.traveler.get(vacationId).then(function(responseTrav){
        if(responseTrav && responseTrav.length > 0){
          vacation.travelers.data = [];

          for(var i = 0; i < responseTrav.length; i++){
            var data = {
              active: false,
              data: responseTrav[i],
              accessibility: [],
              accessibilityTypes: angular.copy(vacation.options.accessibilityTypes)
            };

            vacation.travelers.data.push(data);

            _loadTravelersAccessibility(data, data.data.traveler_id);
          }

        } else{
          _addTravelersForDefault('', '');
        }
      });
    }

    function _loadInterestTypes(){
      vacation.interestType.data = vacation.data.interest_types;

      if(vacation.interestType.data){
        var interest_type_ids = '';
        for (var i = 0; i < vacation.data.interest_types.length; i++) {
          if(interest_type_ids === ''){
            interest_type_ids = '' + vacation.data.interest_types[i].interest_type_id;
          } else {
            interest_type_ids = interest_type_ids + ',' + vacation.data.interest_types[i].interest_type_id;
          }
        }

        vacation.data.interest_type_ids = interest_type_ids;

        if(vacation.options.interestTypes &&
          vacation.options.interestTypes.length > 0){
          for(i = 0; i < vacation.options.interestTypes.length; i++){
            var interestTypeOpt = vacation.options.interestTypes[i];

            for(var j = 0; j < vacation.interestType.data.length; j++){
              var interestTypeData = vacation.interestType.data[j];

              if(interestTypeOpt.id === interestTypeData.interest_type_id){
                interestTypeOpt.val = parseInt(interestTypeOpt.id);
                interestTypeOpt.vacationInterestTypeId = parseInt(interestTypeData.id);
                break;
              }
            }
          }
        }
      }
    }

    function _loadFavoriteCharactersTypes(){

      vacation.favoriteCharactersType.data = vacation.data.characters;

      if(vacation.favoriteCharactersType.data){
        var character_ids = '';

        for (var i = 0; i < vacation.data.characters.length; i++) {
          if(character_ids === ''){
            character_ids = '' + vacation.data.characters[i].character_id;
          } else {
            character_ids = character_ids + ',' + vacation.data.characters[i].character_id;
          }
        }

        vacation.data.character_ids = character_ids;

        if(vacation.options.favoriteCharactersTypes &&
          vacation.options.favoriteCharactersTypes.length > 0){
          for(i = 0; i < vacation.options.favoriteCharactersTypes.length; i++){
            var favoriteCharactersTypeOpt = vacation.options.favoriteCharactersTypes[i];

            for(var j = 0; j < vacation.favoriteCharactersType.data.length; j++){
              var favoriteCharactersTypeData = vacation.favoriteCharactersType.data[j];

              if(favoriteCharactersTypeOpt.id === favoriteCharactersTypeData.character_id){
                favoriteCharactersTypeOpt.val = parseInt(favoriteCharactersTypeOpt.id);
                favoriteCharactersTypeOpt.vacationFavoriteCharacterTypeId = parseInt(favoriteCharactersTypeData.id);
                break;
              }
            }
          }
        }
      }
    }

    function _loadDiningPreferenceTypes(){

      vacation.diningPreferenceType.data = vacation.data.dining_preferences;

      if(vacation.diningPreferenceType.data){

        var dining_preference_ids = '';

        for (var i = 0; i < vacation.diningPreferenceType.data.length; i++) {

          if(dining_preference_ids === ''){
            dining_preference_ids = '' + vacation.diningPreferenceType.data[i].dining_id;
          } else {
            dining_preference_ids = dining_preference_ids + ',' + vacation.diningPreferenceType.data[i].dining_id;
          }
        }

        vacation.data.dining_preference_ids = dining_preference_ids;

        if(vacation.options.diningPreferenceTypes &&
          vacation.options.diningPreferenceTypes.length > 0){
          for(i = 0; i < vacation.options.diningPreferenceTypes.length; i++){
            var diningPreferenceTypeOpt = vacation.options.diningPreferenceTypes[i];

            for(var j = 0; j < vacation.diningPreferenceType.data.length; j++){
              var diningPreferenceTypeData = vacation.diningPreferenceType.data[j];

              if(diningPreferenceTypeOpt.id === diningPreferenceTypeData.dining_id){
                diningPreferenceTypeOpt.val = parseInt(diningPreferenceTypeOpt.id);
                diningPreferenceTypeOpt.vacationDiningPreferenceTypeId = parseInt(diningPreferenceTypeData.id);
                break;
              }
            }
          }
        }
      }
    }

    function _loadParkInterest(){

      vacation.parkInterest.data = vacation.data.vacation_parks;

      if(vacation.parkInterest.data){

        if(vacation.options.parkInterest &&
          vacation.options.parkInterest.length > 0){
          for(var i = 0; i < vacation.options.parkInterest.length; i++){
            var parkInterestOpt = vacation.options.parkInterest[i];

            for(var j = 0; j < vacation.parkInterest.data.length; j++){
              var parkInterestData = vacation.parkInterest.data[j];

              if(parkInterestOpt.name === parkInterestData.park_name){
                parkInterestOpt.hearts = parseInt(parkInterestData.hearts);
                parkInterestOpt.vacationParkInterestId = parseInt(parkInterestOpt.id);
                break;
              }
            }
          }
        }

        for (var index = 0; index < vacation.parkInterest.data.length; index++) {
          var park = vacation.parkInterest.data[index];
          if(park.park_name === 'HOLLYWOOD STUDIOS'){
            vacation.data.hollywood_studios_hearts = park.hearts;
          } else if(park.park_name === 'MAGIC KINGDOM'){
            vacation.data.magic_kingdom_hearts = park.hearts;
          } else if(park.park_name === 'EPCOT'){
            vacation.data.epcot_hearts = park.hearts;
          } else if(park.park_name === 'ANIMAL KINGDOM'){
            vacation.data.animal_kingdom_hearts = park.hearts;
          }
        }
      }
    }

    function _loadPark(vacationId){

      mousePlannerService.experiences.get(vacationId).then(function(resExperiences){
        if(resExperiences){
          vacation.data.experiences = resExperiences;

          vacation.park.data = resExperiences;
          if(vacation.options.park &&
            vacation.options.park.length > 0){
            for(var i = 0; i < vacation.options.park.length; i++){
              var parkOpt = vacation.options.park[i];

              for(var j = 0; j < vacation.park.data.length; j++){
                var parkData = vacation.park.data[j];

                var idSplit = parkOpt.id.split(';');
                if(idSplit.length > 0){
                  parkOpt.id = idSplit[0];
                }

                if(parkOpt.name.toUpperCase() === parkData.experience_name.toUpperCase()){
                  parkOpt.hearts = parseInt(parkData.hearts);
                  parkOpt.vacationParkId = parseInt(parkOpt.id);
                  break;
                }
              }
            }
          }
        }
      });
    }

    function _loadTravelersAccessibility(traveler, travelerId){
      for(var i = 0; i < traveler.accessibilityTypes.length; i++){
        for(var j = 0; j < traveler.data.accessibility_types.length; j++){
          if(traveler.accessibilityTypes[i].id === traveler.data.accessibility_types[j].accessibility_type_id){
            traveler.accessibilityTypes[i].selected = true;
            break;
          }
        }
      }
    }

    var callbackOptions = function (){
      console.log('callback - original');
    };

    return {
      get: function(){
        return vacation;
      },
      set: function(_vacation){
        vacation = _vacation;
      },
      init: function(vacationId){
        if(parseInt(vacationId)){
          _loadVacation(vacationId);
          _loadTravelers(vacationId);
          _loadPark(vacationId);
        }
      }
    };
  });
