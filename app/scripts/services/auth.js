'use strict';

/**
 * @ngdoc service
 * @name mpservicesApp.Auth
 * @description
 * # Auth
 * Service in the mpservicesApp.
 */

angular.module('mpservicesApp')
.factory('Auth', ['$q', '$window', '$state', function($q, $window, $state){
  var isAuthorized = false;
  return {
    request: function (config) {
        config.headers = config.headers || {};
        if ($window.sessionStorage.appUserToken) {
            config.headers.Authorization = 'Bearer '+$window.sessionStorage.appUserToken;
            isAuthorized = true;
        }
        return config;
    },
    response: function (response) {
        return response || $q.when(response);
    },
    responseError: function(rejection) {
      if (rejection.status === 401) {
        if (!rejection.data.error || (rejection.data.error !== 'invalid_credentials' && rejection.data.error !== 'not_confirmed')) {
          delete $window.sessionStorage.appUserToken;
          isAuthorized = false;
          return $state.go('registration');
        }
      }
      if (rejection.status === 403) {
        return $state.go('subscription');
      }
      return $q.reject(rejection);
    },
    UserIsAuthorized: function() {
      return isAuthorized;
    },
    logoutUser: function() {
      delete $window.sessionStorage.appUserToken;
      isAuthorized = false;
    },
    setUserToken: function(token) {
      $window.sessionStorage.appUserToken = token;
      isAuthorized = true;
    }
  };
}]);
