'use strict';

/**
 * @ngdoc service
 * @name dashboardService
 * @description
 * # config
 * Service in the dashboardService.
 */
angular.module('mpservicesApp')
    .service('saveStepService', function ($q, $state, $filter, $document, configPage, mousePlannerService) {

        var updateTrip = function (trip, settings) {
            var def = $q.defer();
            mousePlannerService.trip.update(trip)
                .then(
                    function(res){
                        settings.config.notification.success();
                        def.resolve();
                    },
                    function(err){
                        def.reject(err);
                    }
                );
            return def.promise;
        };

        var step = {
            currentStep: function (settings) {
                var steps = configPage.navigation;
                var currentStep = $filter('filter')(steps, {state: $state.current.name}, true);

                return currentStep.length ? (currentStep[0].number - 1) : ((settings.vacation.data && settings.vacation.data.stepNumber >= 0) ? settings.vacation.data.stepNumber : 0);
            },
            saveVacation: function (trip, vacation, settings){
                var def = $q.defer();
                if(!trip.id){
                    mousePlannerService.trip.create(angular.extend(trip, {name: vacation.name}))
                        .then(
                            function(res){
                                settings.main.trips.push(res);
                                settings.main.plans.data.push(angular.extend(vacation, {id: res.vacation_id, trip_id: res.id}));
                                step.updateVacation(vacation, settings)
                                    .then(function(){
                                        if (settings.vacation.trip.id) {
                                            updateTrip(settings.vacation.trip, settings);
                                        }
                                        def.resolve();
                                    });
                            },
                            function(err){
                                def.reject(err);
                            }
                        );
                }
                else{
                    mousePlannerService.vacation.create({trip_id: trip.id, name: vacation.name})
                        .then(
                            function(res){
                                settings.main.plans.data.push(angular.extend(vacation, res));
                                step.updateVacation(vacation, settings)
                                    .then(function(){
                                        if (settings.vacation.trip.id) {
                                            updateTrip(settings.vacation.trip, settings);
                                        }
                                        def.resolve();
                                    });
                            },
                            function(err){
                                def.reject(err);
                            }
                        );
                }
                return def.promise;
            },
            updateVacation: function (vacation, settings){
            var def = $q.defer();
            mousePlannerService.vacation.update(vacation)
                .then(
                    function(res){
                        if (settings.vacation.trip.id) {
                            updateTrip(settings.vacation.trip, settings);
                        }
                        settings.config.notification.success();
                        def.resolve();
                    },
                    function(err){
                        if(settings.vacation.data.highestStepCompleted === settings.vacation.data.stepNumber) {
                            settings.vacation.data.highestStepCompleted = settings.vacation.data.highestStepCompleted - 1;
                        }
                        def.reject(err);
                    }
                );
            return def.promise;
        }
        };

        return step;
    });