'use strict';

/**
 * @ngdoc service
 * @name mpservicesApp.userService
 * @description
 * # userService
 * Service in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .factory('userService', function (mousePlannerService, $state) {
    var serviceInstance;

    // Service instance
    serviceInstance = {
      redirectAfterLogin: function() {
        var options = { reload: true };

        return new Promise(function (resolve, reject) {
          mousePlannerService.vacation.get().then(
            function (res) {

              // If user has more then one plan
              if (res.data.length > 1) {
                resolve($state.go('main.dashboard', {}, options));
              }

              // If user does not have any plan
              else if (res.data.length === 0) {
                resolve($state.go('main.newtrip.start', {}, options));
              }

              // If user has one trip
              else if (res.data.length === 1) {
                if (res.data[0] && res.data[0].highestStepCompleted < 6) {
                  resolve($state.go('main.trip.edit', {id: res.data[0].id}, options));
                } else {
                  resolve($state.go('main.dashboard', {}, options));
                }
              }

              else {
                resolve($state.go('main.dashboard', {}, options));
              }
            },
            function (err) {
              reject(err);
            }
          ).catch(function(err) {
            $state.go('main.dashboard', {}, options);
          });
        });
      }
    };

    return serviceInstance;
});
