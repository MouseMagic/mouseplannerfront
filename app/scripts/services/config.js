'use strict';

/**
 * @ngdoc service
 * @name mpservicesApp.config
 * @description
 * # config
 * Service in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .service('configPage', function (ENV) {
    var page = {
      urlService: ENV.apiEndpoint,
      scraperService: ''+ENV.apiEndpoint+'/json-output',
      token: {},
      access: {
        grant_type: 'password',
        client_id: '2',
        client_secret: ENV.client_secret ? ENV.client_secret : 'eGWN8Lb130sAsun2OCqUFcF5WyF47XOpnUvKRhw5',
        scope: '*'
      },
      authorized: false,
      startDayMin: '06:00',
      startDayMax: '10:00',
      startDayDefault: '07:00',
      endDayMin: '20:00',
      endDayMax: '03:00',
      endDayDefault: '22:00',
      maxTravelDate: 730, //in days. today + maxTravelDate = max
      datePickerFormat: 'MMM dd, yyyy',
      datePickerOptions: {showWeeks: false, minDate: new Date()},
      datePickerMargin: 5,
      minuteStep: 1,
      minLastVisit: 1971,
      navigation: [
        {number: 1, name: 'START', state:'main.trip.wizard.start'},
        {number: 2, name: 'GUEST', state:'main.trip.wizard.guest'},
        {number: 3, name: 'TRAVEL', state:'main.trip.wizard.travel'},
        {number: 4, name: 'PREFERENCES', state:'main.trip.wizard.preferences'},
        {number: 5, name: 'ACTIVITIES', state:'main.trip.wizard.activities'},
        {number: 6, name: 'WISH LIST', state:'main.trip.wizard.wishlist'},
        {number: 7, name: 'MousePlan!', state:'main.trip.scheduler'}
      ],
      blockIdMapping: [
        {id: 1, type: 'park', img: 'imagic.png', class: 'object-box--blue'},
        {id: 2, type: 'park', img: 'ianimalkingdom.png', class: 'object-box--green'},
        {id: 3, type: 'park', img: 'iepcot.png', class: 'object-box--purple'},
        {id: 4, type: 'park', img: 'ihollywood.png', class: 'object-box--yellow'},
        {id: 5, type: 'park', img: 'isprings.png', class: 'object-box--v'},
        {id: 6, type: 'park', img: 'iblizzard-beach.png', class: 'object-box--light-blue'},
        {id: 7, type: 'park', img: 'ityphoon.png', class: 'object-box--light-blue'},
        {id: 8, type: 'resort'},
        {id: 9, type: 'dining'},
        {id: 10, type: 'transit'},
        {id: 11, type: 'fixed'},
        {id: 12, type: 'fixed'}
      ],
      blockDefaultDuration: [
        {type: 'park', duration: 500},
        {type: 'resort', duration: 180},
        {type: 'dining', duration: 60},
        {type: 'transit', duration: 60},
        {type: 'fixed', duration: 60},
        {type: 'custom', duration: 60}
      ],
      blockParents: ['park','resort'],
      run: [],
      callback: function(){
        for (var i = 0; i < page.run.length; i++) {
          page.run[i]();
        }
        page.run = [];
      }
    };

    page.datePickerOptions.maxDate = (function(){var d = new Date(); return new Date(d.setDate(d.getDate()+page.maxTravelDate));})();

    return page;
  });
