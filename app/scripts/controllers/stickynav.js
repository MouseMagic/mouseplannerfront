'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:StickynavCtrl
 * @description
 * # StickynavCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
  .controller('StickynavCtrl', function ($scope, vacationSession) {
    var trip = $scope.vacation.trip;
    $scope.vacation = vacationSession.get();
    $scope.vacation.trip = trip;
  });
