'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:RegistrationVerifyCtrl
 * @description
 * # RegistrationVerifyCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
.controller('RegistrationVerifyCtrl', ['mousePlannerService', 'Auth', '$state', '$scope', '$stateParams', '$transitions', function(mousePlannerService, Auth, $state, $scope, $stateParams, $transitions){
  if (Auth.UserIsAuthorized()) {
    return $state.go('error', {id: 404});
  }
  $scope.app.fullHeightPage = true;
  $scope.verifiedSuccess = false;
  $scope.verifiedExparied = false;
  if ($stateParams.id) {
    mousePlannerService.user.registrationVerify($stateParams.id)
    .then(
      function(res){
        $scope.verifiedSuccess = true;
      },
      function(err){
        $scope.verifiedExparied = true;
      }
    );
  }
  $scope.goToLogin = function(){
    $state.go('login');
  };
}]);
