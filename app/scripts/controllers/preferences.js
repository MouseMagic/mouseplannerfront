'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:PreferencesCtrl
 * @description
 * # PreferencesCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
  .controller('PreferencesCtrl', function ($scope, $filter) {
    window.scrollTo(0, 0);

    $scope.mytime = null;
    $scope.changedTime = function() {
      console.log('time', $scope.vacation.data.beginDayId);
    };

    var setDate = function(hoursAndMinutes, date){
      var dt = date ? new Date(date) : new Date();
      dt.setHours(parseInt(hoursAndMinutes.split(':')[0]));
      dt.setMinutes(parseInt(hoursAndMinutes.split(':')[1]));
      return dt;
    };

    if(!$scope.vacation.data.beginDayTime){
      $scope.vacation.data.beginDayTime = setDate($scope.main.config.startDayDefault);
      // if the plan is new - rewrite time by UTC time
      $scope.vacation.data.beginDayTime = moment($scope.vacation.data.beginDayTime).utc($scope.vacation.data.beginDayTime).format();
    }



    $scope.startDayMin = moment(setDate($scope.main.config.startDayMin, $scope.vacation.data.beginDayTime));
    $scope.startDayMax = moment(setDate($scope.main.config.startDayMax, $scope.vacation.data.beginDayTime));
    if(!$scope.vacation.data.endDayTime){
      $scope.vacation.data.endDayTime = setDate($scope.main.config.endDayDefault, $scope.vacation.data.beginDayTime); //use begin day to set end day time
      // if the plan is new - rewrite time by UTC time
      $scope.vacation.data.endDayTime = moment($scope.vacation.data.endDayTime).utc($scope.vacation.data.endDayTime).format();
    }

    $scope.endDayMin = moment(setDate($scope.main.config.endDayMin, $scope.vacation.data.beginDayTime));

    var endDayMax = moment(setDate($scope.main.config.endDayMax, $scope.vacation.data.beginDayTime)).format();

    if(moment(endDayMax).valueOf() < moment($scope.endDayMin).valueOf()){ //if end date max is after midnight
        endDayMax = moment(endDayMax).set('date', moment(endDayMax).get('date')+1).format();
    }
    $scope.endDayMax = endDayMax;

    //this variables need for displaying correct time on view
    $scope.utcTime = {
      startTime: moment( moment($scope.vacation.data.beginDayTime).utc().valueOf() - moment().utcOffset()*60000).toDate(),
      endTime: moment( moment($scope.vacation.data.endDayTime).utc().valueOf() - moment().utcOffset()*60000).toDate(),
    };
    //this variables need for displaying correct time on view

      $scope.$watch('utcTime.startTime', function(val){
        //re-write local variables to the correct value in db
        $scope.vacation.data.beginDayTime = moment( moment(val).utc().valueOf() + moment().utcOffset()*60000).toDate();
        //re-write local variables to the correct value in db

          if ($scope.utcTime.startTime < $scope.startDayMin) {
              $scope.mpForm.beginDay.$setValidity("dayTimeError", false);
          }
          else {
              if ($scope.mpForm.beginDay) {
                  $scope.mpForm.beginDay.$setValidity("dayTimeError", true);
              }
          }
      });

      $scope.$watch('utcTime.endTime', function(val){
        //re-write local variables to the correct value in db
        $scope.vacation.data.endDayTime = moment( moment(val).utc().valueOf() + moment().utcOffset()*60000).toDate();
        //re-write local variables to the correct value in db

          var maxEndHoursDifference = ($scope.utcTime.endTime - $scope.endDayMin) / 1000 / 60 / 60;
          if ( Math.ceil(maxEndHoursDifference) > 7 || ($scope.utcTime.endTime < $scope.endDayMin) ) {
              $scope.mpForm.endDay.$setValidity("dayTimeError", false);
          }
          else {
              if ($scope.mpForm.endDay) {
                  $scope.mpForm.endDay.$setValidity("dayTimeError", true);
              }
          }
      });

    $scope.validateBegin = function(date){
      var inMinutes = $filter('timeToMinutes')(date, true);
      if(inMinutes > $filter('timeToMinutes')($scope.main.config.startDayMax)){
        $scope.vacation.data.beginDayTime = setDate($scope.main.config.startDayMax);
      }
      else if(inMinutes < $filter('timeToMinutes')($scope.main.config.startDayMin)){
        $scope.vacation.data.beginDayTime = setDate($scope.main.config.startDayMin);
      }
    };

    $scope.validateEnd = function(date){
      var inMinutes = $filter('timeToMinutes')(date, true);
      if(inMinutes > $filter('timeToMinutes')($scope.main.config.endDayMax)){
        $scope.vacation.data.endDayTime = setDate($scope.main.config.endDayMax);
      }
      else if(inMinutes !== 0 && inMinutes < $filter('timeToMinutes')($scope.main.config.endDayMin)){
        $scope.vacation.data.endDayTime = setDate($scope.main.config.endDayMin);
      }
    };
  });
