'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
.controller('MainCtrl', function(main, configPage, $scope, $document, mousePlannerService){
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  	$scope.config = {
		mpRoot: '/wp-content/plugins/mpservices',
		user: main[0],
		validation: {
			year: {
				min: 1971,
				max: (function(){
					var d = new Date();
					return d.getFullYear();
				})()
			},
			edgeDate: (function(){
                var d = new Date();
                var currentDate = d.getTime();
                var EDGE_DATE = 190 * 24 * 60 * 60 * 1000; // 190 days in milliseconds
                return currentDate + EDGE_DATE;
            })(),
            maxEdgeDate: function(date){
                var d = new Date(date);
                d.setDate(d.getDate() - 190);
                return months[d.getMonth()] + '/' +  d.getDate() + '/' + d.getFullYear();
            },
			phone: {
				pattern: '.+'
			},
			nameTrip: {
				pattern: /^([a-zA-Z0-9ñáéíóúÁÉÍÓÚ'\ ]{0,40})$/i
			},
			guestName: {
				pattern: /^([a-zA-ZñáéíóúÁÉÍÓÚ'\-\ ]{0,40})$/i
			}
		},
	    options: {
	      ofBirth: [
	        { 'id': 1, name: 'Jan' },
	        { 'id': 2, name: 'Feb' },
	        { 'id': 3, name: 'Mar' },
	        { 'id': 4, name: 'Apr' },
	        { 'id': 5, name: 'May' },
	        { 'id': 6, name: 'Jun' },
	        { 'id': 7, name: 'Jul' },
	        { 'id': 8, name: 'Aug' },
	        { 'id': 9, name: 'Sep' },
	        { 'id': 10, name: 'Oct' },
	        { 'id': 11, name: 'Nov' },
	        { 'id': 12, name: 'Dec' }
	      ],
	      yearsOfBirth: (function(){
	        var years = [];
	        for (var i = 2020; i >= 1900; i--) {
	          var year = {
	            id: i,
	            name: '' + i
	          };
	          years.push(year);
	        }
	        return years;
	      })(),
	      dayVisitDisneyPark: [
	        { 'id': 1, name: '1' },
	        { 'id': 2, name: '2' },
	        { 'id': 3, name: '3' },
	        { 'id': 4, name: '4' },
	        { 'id': 5, name: '5' },
	        { 'id': 6, name: '6' },
	        { 'id': 7, name: '7' },
	        { 'id': 8, name: '8' },
	        { 'id': 9, name: '9' },
	        { 'id': 10, name: '10' },
	        { 'id': 11, name: '11+' }
	      ]
	    },
	    go: {
	      section: function(element){
	        setTimeout(function () {
	          var someElement = angular.element(document.getElementById('' + element));
	          $document.scrollToElementAnimated(someElement);
	        }, 100);
	      },
	      topPage: function(){
	        var top = 0;
	        var duration = 100;
	        $document.scrollTop(top, duration);
	      }
	    },
	    notification: {
	      working: false,
	      cssClass: '',
	      cssIcon1: '',
	      cssIcon2: '',
	      textNotification: '',
	      success: function(){
	        var configNotification = {
	          cssClass: 'notification--success',
	          cssIcon1: 'ilast-step',
	          cssIcon2: 'ilast-step',
	          message: 'Your info has been saved!'
	        };
	        $scope.config.notification.template(configNotification);
	      },
	      error: function(){
	        var configNotification = {
	          cssClass: 'notification--error',
	          cssIcon1: 'icn-notif-1',
	          cssIcon2: 'icn-notif-2',
	          message: 'Please make sure all fields are filled out correctly.'
	        };
	        $scope.config.notification.template(configNotification);
	      },
	      template: function(item){
	        $scope.config.notification.working = true;
	        $scope.config.notification.cssClass = item.cssClass;
	        $scope.config.notification.cssIcon1 = item.cssIcon1;
	        $scope.config.notification.cssIcon2 = item.cssIcon2;
	        $scope.config.notification.textNotification = item.message;
	        setTimeout(function () {
	          $scope.config.notification.working = false;
	          $scope.$apply();
	        }, 4000);
	      }
	    },
		datepicker: {
			format: 'M!/d!/yyyy',
			options: {
				initDate: new Date(),
				minDate: new Date()
			}
		},
	    filterConfig: {
	      excludeParksWater: true,
	      excludeDisneySprings: true,
	      excludeTours: true,
	      excludeRecreation: true
	    }
	};

    var checkedItems = [];
    var unCheckedItems = [];
	$scope.main = {
      trips: main[1],
      plans: main[2],
      checklist: main[3],
	  checkedItems: checkedItems,
      unCheckedItems: unCheckedItems,
      config: configPage,
      mousage: {
      	markRead: function(mousage){
            mousage.read = (mousage.read === 0 ? 1 : 0);
            mousePlannerService.mousage.update(mousage);
            event.stopPropagation(); // ng-if removes or recreates a portion of the DOM tree that closes schedule popup
      	},
      	addToChecklist: function(mousage, mousages){
      		var index = mousages.indexOf(mousage);
            mousePlannerService.checklist.create(angular.copy(mousage))
            .then(
              function (res){
              	res.done = res.done ? 1 : 0;
                $scope.main.checklist.push(res);
              },
              function (err){
                console.log("error");
              }
            );
            if(index !== -1){
            	mousages.splice(index, 1);
                event.stopPropagation(); // ng-if removes or recreates a portion of the DOM tree that closes schedule popup
            }
      	}
      },
      checklistItem: {
      	markChecked: function(item){
            item.done = (item.done === 0 ? 1 : 0);
            if (item.done === 1) {
                mousePlannerService.checklist.update(item);
                $scope.main.checkedItems.push(item);
            }
            else {
                mousePlannerService.checklist.update(item);
                $scope.main.unCheckedItems.push(item);
            }
      	},
      	remove: function(item){
            mousePlannerService.checklist.delete(item.id);
      		var index = $scope.main.checklist.indexOf(item);
            if(index !== -1){
            	$scope.main.checklist.splice(index, 1);
            }
      	}
      },
      switchColors: function (initial, type) {
            switch (initial) {
                case 'HS':
                    type === 'color' ? $scope.parkColor = 'yellow-color' : $scope.parkColor = 'yellow-background';
                    break;
                case 'AK':
                    type === 'color' ? $scope.parkColor = 'green-color' : $scope.parkColor = 'green-background';
                    break;
                case 'MK':
                    type === 'color' ? $scope.parkColor = 'blue-color' : $scope.parkColor = 'blue-background';
                    break;
                case 'EP':
                    type === 'color' ? $scope.parkColor = 'purple-color' : $scope.parkColor = 'purple-background';
                    break;
                case 'BB':
                    type === 'color' ? $scope.parkColor = 'light-blue-color' : $scope.parkColor = 'light-blue-background';
                    break;
                case 'DS':
                    type === 'color' ? $scope.parkColor = 'pink-color' : $scope.parkColor = 'pink-background';
                    break;
                case 'TL':
                    type === 'color' ? $scope.parkColor = 'light-blue-color' : $scope.parkColor = 'light-blue-background';
                    break;
                default:
                    $scope.parkColor = '';
            }
            return $scope.parkColor;
        }
	};

    for (var item in $scope.main.checklist) {
        if ($scope.main.checklist[item].done === 1) {
            checkedItems.push($scope.main.checklist[item]);
        }
        else {
            unCheckedItems.push(main[3][item]);
        }
    }

});
