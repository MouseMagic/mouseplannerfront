'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
.controller('PasswordResetCtrl', ['mousePlannerService', 'Auth', '$state', '$scope', '$stateParams',
  function(mousePlannerService, Auth, $state, $scope, $stateParams){
    // Check if user login
    if (Auth.UserIsAuthorized()) {
      return $state.go('error', {id: 404});
    }

    // Add class to body
    $scope.app.fullHeightPage = true;

    // Page mode
    $scope.pageMode = false;
    if ($stateParams.id) {
      $scope.pageMode = 'enterPassword';
    } else {
      $scope.pageMode = 'enterEmail';
    }

    // Email form
    $scope.passResetEmail = {
      serverError: false,
      submit: function(form){
        if(!form.$valid){
          return;
        }
        $scope.disableEmailButton = true;
        mousePlannerService.user.passwordResetEmail($scope.passResetEmail.data)
          .then(
            function(res){
              $scope.pageMode = 'emailSuccess';
            },
            function(err){
              $scope.passResetEmail.serverError = err.data.messages;
              $scope.disableEmailButton = false;
            }
          );
      }
    };

    // Password form
    $scope.passReset = {
      serverError: false,
      submit: function(form){
        if(!form.$valid){
          return;
        }
        // Disable button
        $scope.disablePassButton = true;

        // Prepare submit data
        var data = $scope.passReset.data;
        data.token = $stateParams.id;

        // Query
        mousePlannerService.user.passwordReset(data)
          .then(
            function(res){
              $state.go('login');
            },
            function(err){
              $scope.passReset.serverError = err.data.messages;
              $scope.disablePassButton = false;
            }
          );
      }
    };
}]);
