'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:TripCtrl
 * @description
 * # TripCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
.controller('TripCtrl', ['init', 'mousePlannerService', '$q', '$scope', '$filter', '$state', function(init, mousePlannerService, $q, $scope, $filter, $state){
	window.scrollTo(0, 0);
	$scope.vacation = {
		trip: init[0],
		data: init[1],
		mousages: init[2],
		filters: {
      ids: function(experience, experiencesIds){
        return experiencesIds.indexOf(experience.id) !== -1;
      },
      park: function(experience, mappedParks){
        var locs = (experience.relatedLocations && experience.relatedLocations.primaryLocations) ? experience.relatedLocations.primaryLocations : [experience];
        return $filter('filter')(mappedParks, function(mappingItem){
          return $filter('filter')(locs, function(loc){
            return (loc.links && (loc.links.ancestorThemePark && loc.links.ancestorThemePark.title === mappingItem.disneyParkInterestValue) || (loc.links.ancestorResortArea && loc.links.ancestorResortArea.title === mappingItem.disneyParkInterestValue)); //match any mapped disney values to this experience values
          }).length;
        }).length;
      },
      experiencePark: function(park){
        var mappedParks = $scope.vacation.options.mapParkInterest.filter(function(item){
          return park.id === item.park_id;
        });
        return function(experience){
          return $scope.vacation.filters.park(experience, mappedParks);
        };
      },
      age: function(experience) {
        if(!experience.facets || !experience.facets.age) { return true; } //show all items that do not have an age facet
        var now = new Date();
        var year = now.getFullYear();
        var mappedValues = $filter('filter')($scope.vacation.options.mapAges, function(mappingItem){
          return $filter('filter')(experience.facets.age, function(item){return parseInt(item.id) === mappingItem.disneyAgeId;}).length; //if mapped value is in experience values arra
        });
        for(var i = 0; i < $scope.vacation.data.travelers.length; i++){
          var age = year - parseInt($scope.vacation.data.travelers[i].yearOfBirth);
          for(var j = 0; j < mappedValues.length; j++){
            if(parseInt(mappedValues[j].min) <= age && parseInt(mappedValues[j].max) >= age){
              return true;
            }
          }
        }
        return false;
      },
      thrillFactor: function(experience){
          if(!experience.facets || !experience.facets.thrillFactor){
              return true;
          }
          var mappedValues = $filter('filter')($scope.vacation.options.mapThrillLevelTypes, function(mappingItem){
              return $filter('filter')(experience.facets.thrillFactor, function(item){return parseInt(item.id) === mappingItem.disneyThrillLevelTypeId;}).length; //if mapped value is in experience values arra
          });

          $filter('filter')(mappedValues, function (value) {
              if (value.thrill_level_type_id > $scope.vacation.data.thrill_level_type_id) { // check cards with bigger thrill level than choosed at step 4
                  $scope.sortedByThrill.push(experience);
			  }
          });
          $scope.sortedByThrill = _.uniqBy($scope.sortedByThrill, 'id'); // take only unique after each filter call

          return $scope.sortedByThrill;
      },
      waterPark: function(experience){
        return (experience.id.indexOf('entityType=water-parks') === -1 && !experience.links.ancestorWaterPark);
      },
      tours: function(experience){
        if(experience.id.indexOf('entityType=tour') === -1){ //if not a tour, return true
          return true;
        }
        var pastTrips = 5;
        var budget = 3;
        if(($scope.vacation.trip && $scope.vacation.trip.tripCount && parseInt($scope.vacation.trip.tripCount) > pastTrips) || ($scope.vacation.data && $scope.vacation.data.budget_type_id && $scope.vacation.data.budget_type_id === budget)){
          return true; //return true if userPastTrips > 5 or budget is high
        }
        if(!experience.facets || !experience.facets.interests){
          return false; //pre-condition check for next filter
        }
        return $filter('filter')(experience.facets.interests, { value: 'Behind the Scenes' }, true).length; //return true if Behind the scenes
      },
      excludeExperience: function(experience){
        if(!experience.id || !$scope.vacation.data.startTravelDate || !$scope.vacation.data.endTravelDate) { return false; }//if trip dates have not been selected, return false
        var mappedItems = $filter('filter')($scope.vacation.options.experienceExceptions, { disneyId: experience.id }, true);
        if(mappedItems.length){
          var startYear = new Date($scope.vacation.data.startTravelDate).getFullYear();
          for (var i = 0; i < mappedItems.length; i++){
            var endYear = (mappedItems[i].endMonth < mappedItems[i].startMonth || (mappedItems[i].endMonth === mappedItems[i].startMonth && mappedItems[i].endDay < mappedItems[i].startDay)) ? startYear + 1 : startYear;
            if(new Date(startYear, mappedItems[i].startMonth, mappedItems[i].startDay, 0, 0, 0, 0) <= new Date($scope.vacation.data.startTravelDate) && new Date(endYear, mappedItems[i].endMonth, mappedItems[i].endDay, 23, 59, 59, 999) >= new Date($scope.vacation.data.endTravelDate)) { return false; }
          }
        }
        return true;
      }
    },
	      ordering: {
	        park: function(park){
				return park.hearts || 0;
			},
	        whatsNew: function(experience){
	          	if(!experience.facets || !experience.facets.interests){
	          		return false;
	          	}
	          	return $filter('filter')(experience.facets.interests, { value: "What's New" }, true).length;
	        },
	        score: function(experience){
	        	var score = 0;
	        	score += ($scope.vacation.ordering.whatsNew(experience) ? 1000 : 0); //1000 point baseline to new items
	        	score += ($scope.vacation.ordering.fastpass(experience) ? 100 : 0); //100 point baseline to fastpass items
				score += $scope.vacation.ordering.favoriteCharacters(experience);
	            score  += $scope.vacation.ordering.interests(experience);
	            return score;
	        },
	        fastpass: function(experience){
	          if(!experience.facets || !experience.facets.interests) { return false; }
	          return $filter('filter')(experience.facets.interests, { value: "FastPass+" }, true).length;
	        },
	        interests: function(experience){
	          	var score = 0;
	          	if(!experience.facets || !experience.facets.interests){
	           		return score;
	           	}
	          	var facets = experience.facets.interests.concat($filter('filter')($scope.vacation.options.customFacets, function(item){return (item.disneyId === experience.id && item.type === 'interests');}));
	          	var mappedValues = $filter('filter')($scope.vacation.options.mapInterestTypes, function(mapped){
		            return $filter('filter')(facets, function(item){return item.value.toLowerCase() === mapped.disneyInterestTypeValue.toLowerCase();}).length;
		        });
	          	for(var i = 0; i < mappedValues.length; i++){
	            	var interestsActives = $filter('filter')($scope.vacation.data.interest_types, {id: mappedValues[i].interest_type_id});
	            	if(interestsActives.length && mappedValues[i].weight){
	              		score += mappedValues[i].weight;
	            	}
	          	}
	          	return score;
	        },
	        favoriteCharacters: function(experience){
	          	var score = 0;
	          	if(!experience.facets || !experience.facets.interests){
	          		return score;
	          	}
	          	var facets = experience.facets.interests.concat($filter('filter')($scope.vacation.options.customFacets, function(item){return (item.disneyId === experience.id && item.type === 'interests');}));
	          	var mappedValues = $filter('filter')($scope.vacation.options.mapFavoriteCharactersTypes, function(mapped){
	            	return $filter('filter')(facets, function(item){return item.value.toLowerCase() === mapped.disneyFavoriteCharactersTypeValue.toLowerCase();}).length;
	          	});
	          	for(var i = 0; i < mappedValues.length; i++){
	            	var interestsActives = $filter('filter')($scope.vacation.data.characters, {id: mappedValues[i].character_id}, true);
	            	if(interestsActives.length && mappedValues[i].weight){
	              		score += mappedValues[i].weight;
	            	}
	          	}
	          	return score;
	        },
	      },
	      checked: {
					age: function(facet){
						var mappedValues = $filter('filter')($scope.vacation.options.mapAges, { disneyAgeValue: facet.value }, true);
						if(!mappedValues.length) { return false; }
						for(var i = 0; i < $scope.vacation.data.travelers.length; i++){
	            var now = new Date();
	            var year = now.getFullYear();
	            var ageTraveler = year - parseInt($scope.vacation.data.travelers[i].yearOfBirth);
	            if(ageTraveler >= mappedValues[0].min && ageTraveler <= mappedValues[0].max) { return true; }
						}
						return false;
					},
					height: function(facet){
						var mappedValues = $filter('filter')($scope.vacation.options.mapTravelerHeights, { disneyHeightValue: facet.value }, true);
						if(!mappedValues.length) { return false; }

	          for(var i = 0; i < $scope.vacation.data.travelers.length; i++){
							if($filter('filter')(mappedValues, { travelerHeightId: $scope.vacation.data.travelers[i].heightId }, true).length) { return true; }
						}
						return false;
					},
					thrillFactor: function(facet){
						var fv = $scope.vacation.data.thrillLevelTypeId|| null;
						var mappedValues = $filter('filter')($scope.vacation.options.mapThrillLevelTypes, { disneyThrillLevelTypeValue: facet.value }, true);
						return (mappedValues.length && fv === mappedValues[0].thrillLevelTypeId);
					},
					accessibility: function(facet){
	          var mappedValues = $filter('filter')($scope.vacation.options.mapAccessibilityTypes, { disneyAccessibilityTypeValue: facet.value }, true);
	          var guests = $filter('filter')($scope.vacation.data.travelers, function(guest){
	            return $filter('filter')(guest.accessibility_types, function(accessibility){
	              return $filter('filter')(mappedValues, function(item){
	                return accessibility.accessibilityTypeId === item.accessibilityTypeId;
	              }).length;
	            }).length;
	          });
	          return guests.length > 0;
					},
					mobilityDisability: function(facet){
	          var mappedValues = $filter('filter')($scope.vacation.options.mapAccessibilityTypes, { disneyAccessibilityTypeValue: facet.value }, true);
	          var guests = $filter('filter')($scope.vacation.data.travelers, function(guest){
	            return $filter('filter')(guest.accessibility_types, function(accessibility){
	              return $filter('filter')(mappedValues, function(item){
	                return accessibility.accessibilityTypeId === item.accessibilityTypeId;
	              }).length;
	            }).length;
	          });
	          return guests.length > 0;
					},
					interests: function(facet){
	          var pvInterest = false;
	          var pvFavoriteCharacters = false;

	          var mappedValuesInterest = $filter('filter')($scope.vacation.options.mapInterestTypes, { disneyInterestTypeValue: facet.value }, true);
	          if(mappedValuesInterest.length){
	            pvInterest = $filter('filter')($scope.vacation.data.interest_types, function(interestType){
	              return $filter('filter')(mappedValuesInterest, function(mapped){
	                return interestType.interestTypeId === mapped.interestTypeId;
	              }).length;
	            }).length;
	          }

	          var mappedValuesFavorite = $filter('filter')($scope.vacation.options.mapFavoriteCharactersTypes, { disneyFavoriteCharactersTypeValue: facet.value }, true);
	          if(mappedValuesFavorite.length){
	            pvFavoriteCharacters = $filter('filter')($scope.vacation.data.characters, function(favoriteCharacterType){
	              return $filter('filter')(mappedValuesFavorite, function(mapped){
	                return favoriteCharacterType.favoriteCharactersTypeId === mapped.favoriteCharactersTypeId;
	              }).length;
	            }).length;
	          }


	          return pvInterest || pvFavoriteCharacters;
	        },
	        checkDate: function(item,start,end,config){
	          if(!item.schedules || !item.schedules.length) { return true; }
	          item.schedules.sort(function(a,b){
	              return new Date(b.date) - new Date(a.date);
	          });
	          if(new Date(item.schedules[0].date) < end && new Date(item.schedules[0].date) < start || new Date(item.schedules[item.schedules.length-1].date) > start && new Date(item.schedules[item.schedules.length-1].date) > end) {
	            return false;
	          } //if schedule dates fall outside of trip dates
	          var test = false;
	          item.schedules.forEach(function(s){
	              var valid = $scope.vacation.checked.getScheduleType(s, config);
	              if(valid){ //if this is an unknown schedule type or a type with Value == true
	                  var dtStart = new Date(s.date+'T'+s.startTime); //add schedule time
	                  var dtEnd = new Date(s.date+'T'+s.endTime); //add schedule time
	                  if(dtEnd >= start && dtStart <= end){
	                      test = true;
	                      return;
	                  }
	              }
	          });
	          return test;
	        },
	        getScheduleType: function(schedule,config){
	          if(!schedule.type || !config.length) { return false; }
	          for(var i = 0; i < config.length; i++) {
	              if(config[i].DisneyValue === schedule.type) { return config[i].Value; }
	          }
	          return false;
	        }
				},
	      types: {
	        schedule: {
	  				get: function(){
	  					var data = [
	  						{Value: false, DisneyValue: "Block-Out"},
	  						{Value: false, DisneyValue: "Closed"},
	  						{Value: true, DisneyValue: "Extra Magic Hours"},
	  						{Value: true, DisneyValue: "Operating"},
	  						{Value: true, DisneyValue: "Performance Time"},
	  						{Value: false, DisneyValue: "Refurbishment"},
	  						{Value: true, DisneyValue: "Special Ticketed Event"}
	  					];
	  					return data;
	  				}
	  			}
	      },
	      flags: {
	        mobilityDisability: function(experience){
				if(!experience.facets || !experience.facets.mobilityDisabilities){
					return false;
				}
	          	var guests = $filter('filter')($scope.vacation.data.travelers, function(guest){
	            	return $filter('filter')(guest.accessibility_types, function(accessibility){
	              		var mappedValues = $filter('filter')($scope.vacation.options.mapAccessibilityTypes, { accessibility_type_id: accessibility.id }, true);
	              		return $filter('filter')(mappedValues, function(item){
	                		return $filter('filter')(experience.facets.mobilityDisabilities,{value: item.disneyAccessibilityTypeValue}, true).length;
	              		}).length;
	            	}).length;
	          	});
	          	return guests.length > 0;
			},

					serviceAnimal: function(experience){
	          if(!experience.facets || !experience.facets.serviceAnimals) { return false; }
	          var guests = $filter('filter')($scope.vacation.data.travelers, function(guest){
	            return $filter('filter')(guest.accessibility_types, function(accessibility){
	              var mappedValues = $filter('filter')($scope.vacation.options.mapAccessibilityTypes, { accessibility_type_id: accessibility.id }, true);
	              return $filter('filter')(mappedValues, function(item){
	                return $filter('filter')(experience.facets.serviceAnimals,{value: item.disneyAccessibilityTypeValue}, true).length;
	              }).length;
	            }).length;
	          });
	          return guests.length > 0;
					},

					visualDisability: function(experience){

	          var guests = $filter('filter')($scope.vacation.data.travelers, function(guest){
	            var visualSelected = $filter('filter')(guest.accessibility_types, {id: 3}, true);
							if(visualSelected.length > 0){
								if(!experience.facets || !experience.facets.hearingandVisualDisability) { return true; }
								var visualDisabilityTypes = $filter('filter')($scope.vacation.options.mapAccessibilityTypes, { accessibility_type_id: 3}, true);
								var matches = $filter('filter')(visualDisabilityTypes, function(item){
									return $filter('filter')(experience.facets.hearingandVisualDisability, {value: item.disneyAccessibilityTypeValue}, true).length;
								});
								return matches.length === 0;
							}
	          });

	          return guests.length > 0;
					},
					hearingDisability: function(experience){

						var guests = $filter('filter')($scope.vacation.data.travelers, function(guest){
							var hearingSelected = $filter('filter')(guest.accessibility_types, {id: 4}, true);
							if(hearingSelected.length > 0) {
								if(!experience.facets || !experience.facets.hearingandVisualDisability) { return true; }
								var hearingDisabilityTypes = $filter('filter')($scope.vacation.options.mapAccessibilityTypes, { accessibility_type_id: 4}, true);
								var matches =  $filter('filter')(hearingDisabilityTypes, function(item){
									return $filter('filter')(experience.facets.hearingandVisualDisability, {value: item.disneyAccessibilityTypeValue}, true).length;
								});
								return matches.length === 0;
							}
	          });

						return guests.length > 0;
					},
	        fastpass: function(experience){
	          if(!experience.facets || !experience.facets.interests) { return false; }
	          return $filter('filter')(experience.facets.interests, { value: "FastPass+" }, true).length;
	        },
	        newExperience: function(experience){
	          if(!experience.facets || !experience.facets.interests) { return false; }
	          return $filter('filter')(experience.facets.interests, { value: "What's New" }, true).length;
	        }
      	},
        functions: {
          experiences: function(parks, availableExperiences, defaultExperiences){
            var experiences = $scope.vacation.options.experiences || [];
            var parkIds = parks.map(function(item){
              return item.id;
            });
            var mappedParks = $scope.vacation.options.mapParkInterest.filter(function(item){
              return parkIds.indexOf(item.park_id) !== -1;
            });

            var parkExperiences = $filter('filter')(experiences, function(experience){
              return $scope.vacation.filters.ids(experience, availableExperiences);
            });
            parkExperiences = $filter('filter')(parkExperiences, function(experience){
              return $scope.vacation.filters.park(experience, mappedParks);
            });
            if($scope.config.filterConfig.excludeParksWater){
              parkExperiences = $filter('filter')(parkExperiences, $scope.vacation.filters.waterPark);
            }
            if($scope.config.filterConfig.excludeDisneySprings){
              parkExperiences = $filter('filter')(parkExperiences, { id: '!entityType=entertainment-venue' });
            }
            if($scope.config.filterConfig.excludeRecreation){
              parkExperiences = $filter('filter')(parkExperiences, { id: '!entityType=recreation-activity' });
            }
            if($scope.config.filterConfig.excludeTours){
              parkExperiences = $filter('filter')(parkExperiences, $scope.vacation.filters.tours);
            }
            parkExperiences = $filter('filter')(parkExperiences, $scope.vacation.filters.excludeExperience);
            parkExperiences = $filter('filter')(parkExperiences, $scope.vacation.filters.age);

            $scope.sortedByThrill = []; // refresh excluded cards array after each step moving
            parkExperiences = $filter('filter')(parkExperiences, $scope.vacation.filters.thrillFactor);
            parkExperiences = _.differenceWith(parkExperiences, $scope.sortedByThrill); // exclude cards from default array

              var defaultParkExperiences = $filter('filter')(experiences, function (experience) {
                  return $scope.vacation.filters.ids(experience, defaultExperiences);
              });
              defaultParkExperiences.forEach(function (defaultExperience) {
                  var index = parkExperiences.findIndex(function (parkExperience) {
                      return parkExperience.id === defaultExperience.id;
                  });
                  if (index === -1) {
                      parkExperiences.push(defaultExperience);
                  }
              });

            parkExperiences = $filter('orderBy')(parkExperiences, 'name', true);
            parkExperiences = $filter('orderBy')(parkExperiences, $scope.vacation.ordering.score, true);
            return parkExperiences;
          }
        }
	};

	$scope.isWizardStep = function(){
		return $state.current.name !== 'main.trip.scheduler';
	};

	$q.all([
        mousePlannerService.externalService.options.travelerHeights(),
        mousePlannerService.externalService.options.prefixTitleTypes(),
        mousePlannerService.externalService.options.accessibilityTypes(),
        mousePlannerService.externalService.options.celebrationTypes(),
        mousePlannerService.externalService.options.resorts(),
        mousePlannerService.externalService.options.diningPlanTypes(),
        mousePlannerService.externalService.options.interestVisitParkTypes(),
        mousePlannerService.externalService.options.interestWaterParkTypes(),
        mousePlannerService.externalService.options.bookedPlaceTypes(),
        mousePlannerService.externalService.options.thrillLevelTypes(),
        mousePlannerService.externalService.options.paceTypes(),
        mousePlannerService.externalService.options.budgetTypes(),
        // mousePlannerService.externalService.options.beginDays(), // maybe not necessary?
        mousePlannerService.externalService.options.napTypes(), //remove me later
        mousePlannerService.externalService.options.napTypes(), //remove me later
        // mousePlannerService.externalService.options.endDays(), //maybe not necessary?
        mousePlannerService.externalService.options.timeFlexibilityTypes(),
        mousePlannerService.externalService.options.napTypes(),
        mousePlannerService.externalService.options.orlandoHotels(),
        mousePlannerService.externalService.options.interestTypes(),
        mousePlannerService.externalService.options.favoriteCharactersTypes(),
        mousePlannerService.externalService.options.diningPreferenceTypes(),
        mousePlannerService.externalService.options.parks(),
        mousePlannerService.externalService.options.mapTravelerHeights(),
        mousePlannerService.externalService.options.mapAccessibilityTypes(),
        mousePlannerService.externalService.options.mapThrillLevelTypes(),
        mousePlannerService.externalService.options.mapInterestTypes(),
        mousePlannerService.externalService.options.mapFavoriteCharactersTypes(),
        mousePlannerService.externalService.options.mapParkInterest(),
        mousePlannerService.externalService.options.mapAges(),
        mousePlannerService.externalService.options.customFacets(),
        mousePlannerService.externalService.options.experienceExceptions(),
        mousePlannerService.externalService.options.parkExceptions(),
        mousePlannerService.disneyScraper.attractions(),
        mousePlannerService.disneyScraper.entertainments(),
        mousePlannerService.disneyScraper.events(),
        mousePlannerService.disneyScraper.tours(),
        mousePlannerService.disneyScraper.refurbishments()

	])
	.then(
		function(res){
			$scope.vacation.options = {
				travelerHeights: res[0].data,
			  	prefixTitleTypes: res[1].data,
			  	accessibilityTypes: res[2].data,
			  	celebrationTypes: res[3].data,
			  	resorts: res[4].data,
			  	diningPlanTypes: res[5].data,
			  	interestVisitParkTypes: res[6].data,
			  	interestWaterParkTypes: res[7].data,
			  	bookedPlaceTypes: res[8].data,
			  	thrillLevelTypes: res[9].data,
			  	paceTypes: res[10].data,
			  	budgetTypes: res[11].data,
			  	beginDays: res[12].data,
			  	endDays: res[13].data,
			  	timeFlexibilityTypes: res[14].data,
			  	napTypes: res[15].data,
			  	orlandoHotels: res[16].data,
			  	interestTypes: $filter('orderBy')(res[17].data, function(interestType){
			    	return parseInt(interestType.value);
			  	}),
			  	favoriteCharactersTypes: $filter('orderBy')(res[18].data, function(favoriteCharactersType){
			    	return parseInt(favoriteCharactersType.value);
			  	}),
			  	diningPreferenceTypes: res[19].data,
			  	parks: res[20].data,
			  	mapTravelerHeights: res[21].data,
			  	mapAccessibilityTypes: res[22].data,
			  	mapThrillLevelTypes: res[23].data,
			  	mapInterestTypes: res[24].data,
			  	mapFavoriteCharactersTypes: res[25].data,
			  	mapParkInterest: res[26].data,
			  	mapAges: res[27].data,
			  	customFacets: res[28].data,
			  	experienceExceptions: res[29].data,
			  	parkExceptions: res[30].data,
			  	refurbishments: res[35].entries
			};
			var experiences =  res[31].concat(res[32]);
			experiences = experiences.concat(res[33]);
			experiences = experiences.concat(res[34]);
			$scope.vacation.options.experiences = $filter('filter')(experiences, function(experiences) {
				return (experiences.name);
			},true); // remove any experiences that do not have name in the object
		},
		function(err){
			console.log('failed to get wizzard options.');
		}
	);

}]);
