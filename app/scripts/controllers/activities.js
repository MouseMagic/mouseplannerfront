'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:ActivitiesCtrl
 * @description
 * # ActivitiesCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
  .controller('ActivitiesCtrl', function ($scope, $location, $filter, mousePlannerService) {
    window.scrollTo(0, 0);
    $scope.accordion = {
      itemActive: 1
    };
    $scope.vacation.data.interest_types = $scope.vacation.data.interest_types || [];
    $scope.vacation.data.characters = $scope.vacation.data.characters || [];
    $scope.vacation.data.dining_preferences = $scope.vacation.data.dining_preferences || [];
    $scope.vacation.data.parks = $scope.vacation.data.parks || [];

    $scope.isSelected = function(item, arr){
      return $filter('filter')(arr, {id: item.id}, true).length;
    };

    $scope.toggleSelection = function (item, arr){
      var index = $filter('idIndex')(arr, item.id);
      if(index !== -1){
        arr.splice(index, 1);
      }
      else{
        arr.push(item);
      }
    };

    $scope.skip = function(){
        $scope.vacation.data.stepNumber = parseInt($scope.vacation.data.stepNumber) + 1;
        mousePlannerService.vacation.update($scope.vacation.data)
        .then(
          function(){
            $scope.config.notification.success();
            $location.path('/vacation/' + $scope.vacation.data.id + '/wishlist');
          },
          function(err){
            console.log(err);
          }
        );
    };

    $scope.heartValue = function(hearts, parkInterest){
      var matches = $filter('filter')($scope.vacation.data.parks, {id: parkInterest.id}, true);
      if(matches.length){
        return matches[0].hearts === parseInt(hearts);
      }
      else{
        return false;
      }
    };

    $scope.selectedHeart = function(hearts, parkInterest){
      var index = $filter('idIndex')($scope.vacation.data.parks, parkInterest.id);
      if(index !== -1){
        if($scope.vacation.data.parks[index].hearts === parseInt(hearts)){
          $scope.vacation.data.parks.splice(index, 1);
        }
        else{
          $scope.vacation.data.parks[index].hearts = parseInt(hearts);
        }
      }
      else{
        $scope.vacation.data.parks.push({id: parkInterest.id, hearts: parseInt(hearts)});
      }
    };
  });
