'use strict';

/**
 - * @ngdoc function
 - * @name mpservicesApp.controller:DashboardCtrl
 - * @description
 - * # DashboardCtrl
 - * Controller of the mpservicesApp
 - */

angular.module('mpservicesApp')
  .controller('DashboardCtrl', function ($scope, ngDialog, mousePlannerService, dashboardService, $state, $stateParams, $filter, main) {

    window.scrollTo(0, 0);
    $scope.dashboard = {};

    var trip = $filter('orderBy')($scope.main.trips, '"updated_at"', true);
    if(trip.length){
      if($stateParams.tripId && $filter('filter')(trip, {id: parseInt($stateParams.tripId)}, true).length) {
        $scope.dashboard.trip = $filter('filter')(trip, {id: parseInt($stateParams.tripId)}, true)[0];
      }
      else {
        $scope.dashboard.trip = trip[0];
      }
    }

    // get current logged user
    mousePlannerService.user.me().then(function (currentUser) {
          $scope.currentUserInfo = currentUser;
      });

      // call function after page loaded
      dashboardService.getResultsPage(1, '', '').then(function (data) {
          $scope.usersPerPage = data.plansPerPage;
          $scope.sortVacation = data.sortVacation;
          $scope.totalUsers = data.totalPlans;
          $scope.vacations = data.vacations;
          // change plans array on each page for editing and deleting
          if ($scope.currentUserInfo.role_id === 1) {
              main[2].data = $scope.vacations;
          }
          else {
              main[2].data = $scope.sortVacation;
          }
      });

      // init input
      $scope.searchPlans = '';

    // search, pagination or filter action
      $scope.pageChanged = function (pageNumber, searchPlans, filterSelect) {
          dashboardService.getResultsPage(pageNumber, searchPlans, filterSelect).then(function (data) {
              $scope.usersPerPage = data.plansPerPage;
              $scope.sortVacation = data.sortVacation;
              $scope.totalUsers = data.totalPlans;
              $scope.vacations = data.vacations;
              $scope.currentPage = data.currentPage;
              // change plans array on each page for editing and deleting
              if ($scope.currentUserInfo.role_id === 1) {
                  main[2].data = $scope.vacations;
              }
              else {
                  main[2].data = $scope.sortVacation;
              }
          });
      };

      $scope.filterSelect = 'startTravelDate';


    $scope.selectTrip = function(trip){
      $state.go($state.current, {tripId: trip.id}, {reload: false});
    };

    $scope.showMousages = function(mousages){

      $scope.mousages = mousages;
      ngDialog.open({
        template: 'views/templates/mousages-modal.html',
        className: 'ngdialog-theme-default modal--checklist',
        scope: $scope,
        showClose: false
      });
    };

    $scope.newTrip = function(){
      $state.go('main.newtrip.start');
    };

    $scope.learnMore = function() {
          $scope.form = {
              modal: ngDialog.open({
                  template: 'views/templates/learn-more.html',
                  scope: $scope
              }),
              submit: function(form){
                  if(!form.$valid){
                      return;
                  }
                  this.modal.close();
              }
          };
      };

    // USER GUIDE
    $scope.showGuide = function() {
        var currentTrip = $scope.main.trips[0];
        currentTrip.firstTrip = 0;
          $scope.form = {
              modal: ngDialog.open({
                  template: 'views/templates/show-guide.html',
                  className: 'ngdialog-show-guide',
                  scope: $scope
              }),
              submit: function(form){
                  if(!form.$valid){
                      return;
                  }
                  this.modal.close();
                  mousePlannerService.trip.update(currentTrip);
              }
          };
      };

    $scope.removeTrip = function(trip){
      $scope.form = {
        description: "Would you still like to delete this trip?",
        modal: ngDialog.open({
          template: 'views/templates/confirm-modal.html',
          scope: $scope
        }),
        submit: function(form){
          if(!form.$valid){
            return;
          }
          mousePlannerService.trip.delete(trip.id);
          var index = $scope.main.trips.indexOf(trip);
          if(index !== -1){
            $scope.main.trips.splice(index, 1);
          }
          this.modal.close();
          $scope.dashboard.trip = null;
        }
      };
    };

    $scope.newPlan = function(trip){
      if(!trip){
        return $scope.newTrip();
      }
      $state.go('main.newplan.start', {id: trip.id});
    };

    $scope.editPlan = function(plan){
      $state.go('main.trip.edit', {id: plan.id});
    };

    $scope.duplicatePlan = function(plan){
      mousePlannerService.vacation.duplicate(plan)
      .then(
        function(response){
            // get info from another condition for admin
            if ($scope.currentUserInfo.role_id === 1) {
                for (var users in $scope.sortVacation) {
                    for (var vacation in $scope.sortVacation[users].user.vacations) {
                        if ($scope.sortVacation[users].user.vacations[vacation].id === plan.id) {
                            $scope.sortVacation[users].user.vacations.push(response);
                            main[2].data = $scope.sortVacation[users].user.vacations;
                        }
                    }
                }
            }
            else {
                $scope.sortVacation.push(response);
            }

        },
        function(err){
          console.log(err);
        }
      );
    };

    $scope.removePlan = function(plan){
      $scope.form = {
        description: "Would you still like to delete this plan?",
        modal: ngDialog.open({
          template: 'views/templates/confirm-modal.html',
          scope: $scope
        }),
        submit: function(form){
          if(!form.$valid){
            return;
          }
          mousePlannerService.vacation.delete(plan);
            // get info from another condition for admin
            if ($scope.currentUserInfo.role_id === 1) {
                for (var users in $scope.sortVacation) {
                    for (var vacation in $scope.sortVacation[users].user.vacations) {
                        var index = $scope.sortVacation[users].user.vacations.indexOf(plan);
                        if ($scope.sortVacation[users].user.vacations[vacation].id === plan.id) {
                            if(index !== -1){
                                $scope.sortVacation[users].user.vacations.splice(index, 1);
                            }
                        }
                    }
                }
            }
            else {
                var index = $scope.sortVacation.indexOf(plan);
                if(index !== -1){
                    $scope.sortVacation.splice(index, 1);
                }
            }
          this.modal.close();
        }
      };
    };

      var indexedPlans = [];
      $scope.plansToFilter = function () {
          indexedPlans = [];
          return $scope.sortVacation;
      };

      $scope.filterPlans = function (plan) {
          var planIsNew = indexedPlans.indexOf(plan.firstname) === -1;
          if (planIsNew) {
              indexedPlans.push(plan.firstname);
          }
          return planIsNew;
      };

      // Subscription popup
    $scope.userCanSubscribe = $scope.config.user.is_on_trial;
    $scope.showSubscriptionPopup = false;
    $scope.toggleSubscriptionPopup = function() {
    $scope.showSubscriptionPopup = true;
    };
  });