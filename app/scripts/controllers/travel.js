'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:TravelCtrl
 * @description
 * # TravelCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
  .controller('TravelCtrl', function ($scope, $stateParams, mousePlannerService, configPage, $location, $filter) {
    window.scrollTo(0, 0);
    //var paramVacationId = $stateParams.id;

    //$scope.vacation = vacationSession.get();


    $scope.visitDays = [
      { 'id': 1, name: '1' },
      { 'id': 2, name: '2' },
      { 'id': 3, name: '3' },
      { 'id': 4, name: '4' },
      { 'id': 5, name: '5' },
      { 'id': 6, name: '6' },
      { 'id': 7, name: '7' },
      { 'id': 8, name: '8' },
      { 'id': 9, name: '9' },
      { 'id': 10, name: '10' }
    ];

    $scope.hotels = [];

    var dt = new Date();
    dt = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate()+1, 0, 0, 0, 0);
    $scope.minDateFrom = new Date(dt);

    //this variables need for displaying correct time on view (timepicker always adds local time zone, this variables prevent this behavior)
    $scope.localDate = {
      startTravelDate: $scope.vacation.data.startTravelDate ? moment( moment($scope.vacation.data.startTravelDate).utc().valueOf() - moment($scope.vacation.data.startTravelDate).utcOffset()*60000).toDate() :  new Date(dt),
      endTravelDate: $scope.vacation.data.endTravelDate ? moment( moment($scope.vacation.data.endTravelDate).utc().valueOf() - moment($scope.vacation.data.endTravelDate).utcOffset()*60000).toDate() :  new Date(dt),
      arrivalDate: $scope.vacation.data.arrivalDate ? moment( moment($scope.vacation.data.arrivalDate).utc().valueOf() - moment().utcOffset()*60000).toDate() : null,
      departureDate: $scope.vacation.data.departureDate ? moment( moment($scope.vacation.data.departureDate).valueOf() - moment().utcOffset()*60000).toDate() : null
    };
    //this variables need for displaying correct time on view
    $scope.localDate.endTravelDate.maxArrangeDate = false;

    //re-write local variables to the correct value in db
    $scope.$watch('localDate.arrivalDate', function(val){
      $scope.vacation.data.arrivalDate =  moment( moment(val).utc().valueOf() + moment().utcOffset()*60000).toDate();
    });

    $scope.$watch('localDate.departureDate', function(val){
      $scope.vacation.data.departureDate =  moment( moment(val).utc().valueOf() + moment().utcOffset()*60000).toDate();
    });
    //re-write local variables to the correct value in db

    $scope.$watch('localDate.startTravelDate', function(val){
      $scope.vacation.data.startTravelDate =  moment( moment(val).utc().valueOf() + moment(val).utcOffset()*60000).toDate();
        $scope.maxEdgeDate = $scope.config.validation.maxEdgeDate($scope.vacation.data.startTravelDate);

        if(new Date(val).getTime() > $scope.main.config.datePickerOptions.maxDate){
        $scope.vacation.data.startTravelDate = $scope.main.config.datePickerOptions.maxDate;
        return;
      }
      if($scope.localDate.endTravelDate && new Date(val) >= new Date($scope.localDate.endTravelDate)){
        var dt = new Date(val);
        $scope.localDate.endTravelDate = new Date(dt.setDate(dt.getDate()+$scope.main.config.datePickerMargin));
      }

      $scope.vacation.data.edgeDate = new Date(val).getTime() > $scope.config.validation.edgeDate;

      if (Math.ceil(Math.abs($scope.localDate.endTravelDate.getTime() - $scope.vacation.data.startTravelDate.getTime()) / (1000 * 3600 * 24)) > 15) {
          $scope.localDate.endTravelDate.maxArrangeDate = true;
          $scope.mpForm.endTravelDate.$setValidity("arrangeError", false);
      }
      else {
          $scope.localDate.endTravelDate.maxArrangeDate = false;
          $scope.mpForm.endTravelDate.$setValidity("arrangeError", true);
      }
    });

    $scope.$watch('localDate.endTravelDate', function(val){
      $scope.vacation.data.endTravelDate =  moment( moment(val).utc().valueOf() + moment(val).utcOffset()*60000).toDate();

      if(new Date(val).getTime() > $scope.main.config.datePickerOptions.maxDate){
        $scope.vacation.data.endTravelDate = $scope.main.config.datePickerOptions.maxDate;
        return;
      }
      if($scope.vacation.data.startTravelDate && new Date(val) <= new Date($scope.vacation.data.startTravelDate)){
        var dt = new Date($scope.vacation.data.startTravelDate);
        $scope.vacation.data.endTravelDate = new Date(dt.setDate(dt.getDate()+$scope.main.config.datePickerMargin));
      }

        if (Math.ceil(Math.abs($scope.vacation.data.endTravelDate.getTime() - $scope.vacation.data.startTravelDate.getTime()) / (1000 * 3600 * 24)) > 15) {
            $scope.localDate.endTravelDate.maxArrangeDate = true;
            $scope.mpForm.endTravelDate.$setValidity("arrangeError", false);
        }
        else {
            $scope.localDate.endTravelDate.maxArrangeDate = false;
            $scope.mpForm.endTravelDate.$setValidity("arrangeError", true);
            $scope.mpForm.startTravelDate.$invalid = true;
        }
    });

    /*
    $scope.now = moment().add(1, 'day').format('MMM Do YYYY');
    $scope.minDateFrom = moment().add(1, 'day').hour(12).startOf('h');
    $scope.minDateTo = moment().add(2, 'day');

    var mirrorDate = function(val, prop){
      if(val){
        $scope.vacation.data[prop] = new Date(val._d);
      }
      else if($scope.vacation.data[prop]){
        $scope[prop] = moment($scope.vacation.data[prop]);
        if(prop === 'startTravelDate'){
          $scope.minDateFrom = angular.copy($scope[prop]);
        }
        else if(prop === 'endTravelDate'){
          $scope.minDateTo = angular.copy($scope[prop]);
        }
      }
    };

    */

    /*

    if($scope.vacation.data &&
      $scope.vacation.data.startTravelDate != undefined &&
      $scope.vacation.data.startTravelDate != null){

      $scope.minDateFrom = angular.copy($scope.vacation.data.startTravelDate);
      $scope.minDateTo = angular.copy($scope.vacation.data.endTravelDate);
    }

    */

    $scope.changeDateMinTo = function (modelName, newValue) {
      
    };

    /*

    function addDays(days, date){
      date = new Date(date);
      date.setDate(date.getDate() + days);
      return date;
    }

    $scope.changeEndTravelDate = function(modelName, newValue){
      mirrorDate(newValue,'endTravelDate');
    };

    mirrorDate(null,'startTravelDate'); //init
    mirrorDate(null,'endTravelDate'); //init

    */

    /*
    // Verify that there is no other charged holidays.
  	if(paramVacationId !== undefined && parseInt(paramVacationId) && (!$scope.vacation.data || !$scope.vacation.data.id)){
      vacationSession.init(parseInt(paramVacationId));
    }
    */

    function loadOrlandoHotels(){
      for(var i = 0; i < $scope.vacation.options.orlandoHotels.length; i++){
        var hotel = angular.copy($scope.vacation.options.orlandoHotels[i]);
        if($scope.hotels.indexOf(hotel.name) === -1){
          $scope.hotels.push(hotel.name);
        }

      }
    }

    $scope.completAddressZip = function(hotelName){
      var hotelData = findOrlandoHotelsbyName(hotelName);
      $scope.vacation.data.streetAddress = hotelData.address;
      $scope.vacation.data.zipCode = hotelData.zip;
    };

    $scope.updateHotels = function(hotelName){
      var hotels = $filter('filter')($scope.vacation.options.orlandoHotels, { name: hotelName });
      var newHotels = [];
      var numMaxItems = 8;
      for(var i = 0; i < hotels.length && i < numMaxItems; i++){
        var hotel = hotels[i];
        if(newHotels.indexOf(hotel.name) === -1){
          newHotels.push(hotel.name);
        }
      }
      $scope.hotels = newHotels;
    };

    function findOrlandoHotelsbyName(name){

      if ($scope.vacation.options.orlandoHotels.length > 1){
        for(var i = 0; i < $scope.vacation.options.orlandoHotels.length; i++){
          var hotel = $scope.vacation.options.orlandoHotels[i];
          if(hotel.name === name){
            return hotel;
          }
        }
      }
    }

    $scope.$watch('vacation.options.orlandoHotels', function() {
      if($scope.vacation.options && $scope.vacation.options.orlandoHotels){
        loadOrlandoHotels();
      }
    });

  });
