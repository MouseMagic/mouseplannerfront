'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:RegistrationCtrl
 * @description
 * # RegistrationCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
.controller('ErrorCtrl', ['mousePlannerService', 'Auth', '$state', '$scope', '$stateParams',
  function(mousePlannerService, Auth, $state, $scope, $stateParams){
    // Add class to body
    $scope.app.fullHeightPage = true;
    $scope.availableErrors = ['404', '500'];

    $scope.errorCode = $scope.availableErrors.indexOf($stateParams.id) !== -1 ? $stateParams.id : '404';
}]);
