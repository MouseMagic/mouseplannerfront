'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:WishlistCtrl
 * @description
 * # WishlistCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
  .controller('WishlistCtrl', function ($scope, $stateParams, $filter, $timeout, $location, ngDialog, mousePlannerService, experience_lists) {
    window.scrollTo(0, 0);

    $scope.accordion = {
      itemActive: 0
    };

    $scope.vacation.data.experiences = $scope.vacation.data.experiences || [];

    var init = function(){
      if(!$scope.vacation.options){
        return;
      }
      var parks = $filter('park')($scope.vacation.options.parks, $scope.vacation.options.parkExceptions);
      $scope.parks = $filter('parkInterest')(parks, $scope.vacation.data.parks);
      $scope.experiences = $scope.vacation.functions.experiences($scope.parks, experience_lists[1], experience_lists[2]);
    };

    $scope.$watch('vacation.options', init);

    var scoreToHearts = function(parkScores){
      if(!parkScores.length){
        return [];
      }
      parkScores = $filter('orderBy')(parkScores, 'score', true);
      var hearts = 3;
      var totalUnique = 3;
      var arr = [{id: parkScores[0].id, hearts: hearts}];
      for(var i = 1; i < parkScores.length; i++){
        if(hearts > 0 && (parkScores[i].score > 0 || arr.length < totalUnique)){
          if(parkScores[i].score < parkScores[i-1].score){
            hearts--;
          }
          arr.push({id: parkScores[i].id, hearts: hearts});
        }
      }
      return arr;
    };

    if(!$scope.vacation.data.parks || !$scope.vacation.data.parks.length){ //if user did not select a park, recommend some
        mousePlannerService.vacation.recommendations($scope.vacation.data.id)
        .then(
          function(res){
            $scope.vacation.data.parks = scoreToHearts(res.parks);
            init();
          },
          function(err){
            console.log(err);
          }
        );
    }

    $scope.selectedHeart = function(hearts, experience, park){
      var index = $filter('idIndex')($scope.vacation.data.experiences, experience.id, 'external_id');
      if(index !== -1){
        if($scope.vacation.data.experiences[index].hearts === parseInt(hearts)){
          $scope.vacation.data.experiences.splice(index, 1);
        }
        else{
          $scope.vacation.data.experiences[index].hearts = parseInt(hearts);
        }
      }
      else{
        $scope.vacation.data.experiences.push({external_id: experience.id, park_id: park.id, hearts: parseInt(hearts)});
      }
    };

    $scope.heartValue = function(hearts, experience){
      var matches = $filter('filter')($scope.vacation.data.experiences, {external_id: experience.id}, true);
      if(matches.length && matches[0].hearts === parseInt(hearts)){
         return true;
      }
      return false;
    };

    $scope.skip = function(accodionIndex, park){
      $timeout(function(){
        $scope.accordion.itemActive = accodionIndex;
      });
      var available = $filter('filter')($scope.experiences, $scope.vacation.filters.experiencePark(park));
      var availableIds = available.map(function(experience){return experience.id;});
      var wishlist = experience_lists[0];
      for(var i=0; i < wishlist.length; i++){
        var index = availableIds.indexOf(wishlist[i].external_id);
        if(index !== -1){
          if(!$filter('filter')($scope.vacation.data.experiences, {external_id: wishlist[i].external_id}, true).length){
            $scope.selectedHeart(wishlist[i].hearts, available[index], park);
          }
        }
      }
    };

    $scope.external = function(url1, url2){
      $scope.external = {
        url: url1 + url2
      };
      ngDialog.open({
        template: 'externallink',
        width: '90%',
        height: 600,
        scope: $scope
      });
    };

    $scope.showPopoverDetails = function(park, vacation){

      $scope.parkTemp = park;
      $scope.vacationTemp = vacation;

      ngDialog.open({
        template: 'views/templates/wishlist-modal.html',
        className: 'ngdialog-theme-default modal--checklist',
        scope: $scope,
        showClose: false,
        controller: ['$scope', function($scope){
        }]
      });
    };
  });
