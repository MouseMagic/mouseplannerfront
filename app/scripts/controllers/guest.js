'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:GuestCtrl
 * @description
 * # GuestCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
  .controller('GuestCtrl', function ($scope, $stateParams, $q, $location, $timeout, $filter, mousePlannerService) {
    if(!$scope.vacation.data.travelers || !$scope.vacation.data.travelers.length){
      $scope.vacation.data.travelers = [];
      var userCurrent = {
        firstname: $scope.config.user.firstname,
        lastname: $scope.config.user.lastname,
        accessibility_types: [],
        active: true
      };
      $scope.vacation.data.travelers.push(userCurrent);
    }

    $scope.travelerAgeTypes = [
      {id: 1, value: 1, label:"Yes"},
      {id: 0, value: 0, label:"No"}
    ];

    $scope.getTravelerType = function(traveler){
      if(!traveler || !traveler.prefix_title_type_id){
        return null;
      }
      var type = $filter('filter')($scope.vacation.options.prefixTitleTypes, {id: traveler.prefix_title_type_id}, true);
      if(type.length){
        return type[0].is_adult;
      }
      return null;
    };

    $scope.isAdult = function(is_adult){
      return function(item){
        return item.is_adult === is_adult;
      };
    };

    $scope.accessibilitySelected = function(traveler, accessibility){
      if(!traveler.accessibility_types){
        return false;
      }
      return $filter('filter')(traveler.accessibility_types, {id: accessibility.id}, true).length;
    };

    $scope.changeAccessibility = function (traveler, item){
      var index = $filter('idIndex')(traveler.accessibility_types, item.id);
      if(index !== -1){
        traveler.accessibility_types.splice(index, 1);
      }
      else{
        if(!traveler.accessibility_types){
          traveler.accessibility_types = [];
        }
        traveler.accessibility_types.push(item);
      }
    };

    $scope.removeTraveler = function(item){
    	var index = $scope.vacation.data.travelers.indexOf(item);
      if(index !== -1){
        if(item.id){
          mousePlannerService.traveler.delete($scope.vacation.data.id, item.id);
        }
        $scope.vacation.data.travelers.splice(index, 1);
      }
    };

    $scope.addTraveler = function(){
    	$scope.vacation.data.travelers.push({});
    };

    $scope.updateTraveler = function(form, traveler){
      if(!form.$valid){
        $scope.config.notification.error();
        return;
      }
      if(!traveler.monthOfBirth || !traveler.yearOfBirth){
        var dt = new Date();
        dt.setFullYear(dt.getFullYear()-20);
        traveler.monthOfBirth = dt.getMonth()+1;
        traveler.yearOfBirth = dt.getFullYear();
      }
      if(!traveler.traveler_height_id){
        traveler.traveler_height_id = 1;
      }
      //t.accessibility_types = $filter('objArrToIdString')(t.accessibility_types); //convert array to string for posting
      if(traveler.id){ //update
        mousePlannerService.traveler.update(angular.copy(traveler))
        .then(
          function(res){
            //do nothing
          },
          function(err){
            console.log(err);
          }
        );
      }
      else { //create
        mousePlannerService.traveler.create($scope.vacation.data.id, angular.copy(traveler))
        .then(
          function(res){
            angular.extend(traveler, res);
          },
          function(err){
            console.log(err);
          }
        );
      }
    };

    $scope.editNextTraveler = function(item){
      var index = $scope.vacation.data.travelers.indexOf(item);
      $scope.vacation.data.travelers[index].active = false;
      if($scope.vacation.data.travelers.length > (index + 1)){
        $scope.vacation.data.travelers[index + 1].active = true;
      }
    };
  });
