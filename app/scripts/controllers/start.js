'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:StartCtrl
 * @description
 * # StartCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
  .controller('StartCtrl', function ($scope, $location, $q, $filter, mousePlannerService, vacationSession, $stateParams) {
		window.scrollTo(0, 0);
    if($scope.config){
      $scope.config.go.topPage();
    }

      // short check array or obj
      if (Array.isArray($scope.vacation.trip)) {
          $scope.vacation.trip = $scope.vacation.trip[0];
      }

		//var paramVacationId = $stateParams.id;

		//$scope.vacation = vacationSession.get();
		//$scope.celebrationTypes = [];
		var dt = new Date();
		$scope.maxLastVisit = parseInt(dt.getFullYear());

		$scope.changeItemCelebration = function(item){
			var index = $filter('idIndex')($scope.vacation.data.celebration_types, item.id);
			if(index !== -1){
				$scope.vacation.data.celebration_types.splice(index, 1);
			}
			else{
              	$scope.vacation.data.celebration_types.push(item);
			}

			/*

			if(item.selected){
				$scope.vacation.celebrationTypes.functions.create(item);
			}else{
				for(var i = 0;  i < $scope.vacation.celebrationTypes.data.length; i++){
					if($scope.vacation.celebrationTypes.data[i].celebrationTypeId === item.id){
						$scope.vacation.celebrationTypes.functions.delete(i, $scope.vacation.celebrationTypes.data[i]);
						return;
					}
				}
			}
			*/
		};

		$scope.celebrationSelected = function(celebration){
			return $filter('filter')($scope.vacation.data.celebration_types, {id: celebration.id}, true).length;
		};

		/*

		$scope.someCelebrationSelected = function(){
			var selected = ($scope.vacation.celebrationTypes &&
			$scope.vacation.celebrationTypes.data &&
			$scope.vacation.celebrationTypes.data.length > 0);
			return selected;
		};

		*/

		// Load Options

		/*  THESE ARE ALREADY BEING LOADED ON THE TRIP RESOLVE - JOE

		$q.all([mousePlannerService.options.celebrationTypes()])
		.then(
			function(options){
				if(options && options[0]){
					$scope.celebrationTypes = options[0];
					celebrationSelected();
				}
			},
			function(err){
				console.log(JSON.stringify(err));
			}
		);

		*/

		/*

		function celebrationSelected(){

			if($scope.vacation.celebrationTypes &&
				$scope.vacation.celebrationTypes.data){

				for(var i = 0; i < $scope.celebrationTypes.length; i++){
					var celebration = $scope.celebrationTypes[i];
					celebration.selected = isCelebrationSelected(celebration);
				}
			}
		}

		*/

		function isCelebrationSelected(celebration){
			for(var x = 0; x < $scope.vacation.celebrationTypes.data.length; x++){
				if($scope.vacation.celebrationTypes.data[x].celebrationTypeId === celebration.id){
					return true;
				}
			}
		}

		/* REMOVED BECAUSE -- WHAT??? - Joe

		// Verify that there is no other charged holidays.
		if(paramVacationId !== undefined && parseInt(paramVacationId) && (!$scope.vacation.data || !$scope.vacation.data.id)){

			vacationSession.init(parseInt(paramVacationId));
			$scope.$watch('vacation.celebrationTypes.data', function() {

        if($scope.vacation.celebrationTypes && $scope.vacation.celebrationTypes.data){
        	celebrationSelected();
        }
	    });
		}

		*/

    $scope.increment = function(val) {
            var number;

            if ($scope.vacation.trip.tripCount) {
              number = parseInt($scope.vacation.trip.tripCount);
              if (number > 0 && number <= 99) {
                number += val;
              }
            } else {
              number = 1;
            }
            if (number <= 0) {
              number = 1;
            } else if (number >= 100) {
              number = 99;
            }
            $scope.vacation.trip.tripCount = number;
        };

        $scope.incrementLV = function(val) {
            if ($scope.vacation.trip.lastVisit) {
              var number = parseInt($scope.vacation.trip.lastVisit)+val;
              if (number >= $scope.main.config.minLastVisit){
                $scope.vacation.trip.lastVisit = number;
                return $scope.vacation.trip.lastVisit;
              }
            }
            $scope.vacation.trip.lastVisit = $scope.main.config.minLastVisit;
            return $scope.vacation.trip.lastVisit;
        };
      });
