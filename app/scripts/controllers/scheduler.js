/* jshint ignore:start */
'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:SchedulerCtrl
 * @description
 * # SchedulerCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
  .controller('SchedulerCtrl', function (scheduler, $scope, $document, $filter, $stateParams, $q, mousePlannerService, ngDialog, configPage, $timeout, main, $window, $uibPosition) {
    window.scrollTo(0, 0);

    $scope.resorts = [];

    $scope.restaurants = [];
    $scope.restaurantsNames = [];
    $scope.showparks = true;

    $scope.configAutocomplete = {};
    $scope.mousagesPopover = {open: true};
    $scope.$watch('mousagesPopover.open', function(val){
      if(val){
        $scope.app.menuOpen = true;
        $scope.genOpen = true;
        $scope.open = true;
      }
    });

    $scope.readMousages = function(showAll){
      return function(item){ // item can be day or mousage

        if(showAll){
          return true;
        }

        if(item.day_number) {
          if(!(item.mousages)) {
            return false;
          }
          else {
            var unreadMousages = $filter('filter')(item.mousages, {read: 0}, true);
            if(unreadMousages.length > 0) {
              return true;
            }
            else {
              return false;
            }
          }
        }

        else if(item.mousageable_id) {
          return item.read === 0;
        }

        else {
          return false;
        }
      };
    };

    $scope.reoptimize = function(){
      var newMousePlan = {
        vacation_id: $scope.vacation.data.id,
        dashboard_img: 'img'
      };
      $scope.schedulerConfig.saves.push(newMousePlan);
      mousePlannerService.mouseplan.create(newMousePlan)
      .then(
        function(res){
          $scope.schedulerConfig.saveComplete(newMousePlan);
          mousePlannerService.vacation.mousages(newMousePlan.vacation_id)
          .then(
            function(res){
              $scope.vacation.mousages = res;
            }
          );
          $scope.vacation.data.mouse_plan = res;
        }
      );
    };


    var date = new Date($scope.vacation.data.startTravelDate);
    var startTravelTime = date.getTime();
    $scope.vacation.data.edgeDate = startTravelTime > $scope.config.validation.edgeDate;
    $scope.maxEdgeDate = $scope.config.validation.maxEdgeDate($scope.vacation.data.startTravelDate);

    $scope.header = {
      min: new Date(),
      title: '',
      tabCurrent: -1,
      datePicker: {
        startDate: moment($scope.vacation.data.startTravelDate),
        endDate: moment($scope.vacation.data.endTravelDate)
      },
      datePickerOptions: {
        locale: {
          format: "MMM D, YYYY"
        }
      },
      currentDay: 0
    };

    $scope.$watch('vacation.data.name', function(val, oldVal){
      if(val && val !== "" && val !== oldVal){
        mousePlannerService.vacation.update($scope.vacation.data);
      }
    });
    var getDateDiff = function(d1, d2){
      var diff = Math.max(((((new Date(d2) - new Date(d1))/1000)/60)/60)/24, 0);
      return parseInt(diff);
    };

    // USER GUIDE
      if ($scope.main.trips[0].firstTrip !== 0  && $scope.main.trips.length  <= 1) {
          var currentTrip = $scope.main.trips[0];
          showGuide();
          currentTrip.firstTrip = 0;
          mousePlannerService.trip.update(currentTrip);
      }

      $scope.showGuide = function() {
          showGuide();
      };

    function showGuide() {
        $scope.form = {
            modal: ngDialog.open({
                template: 'views/templates/show-guide.html',
                className: 'ngdialog-show-guide',
                scope: $scope
            }),
            submit: function(form){
                if(!form.$valid){
                    return;
                }
                this.modal.close();
            }
        };
    }
    $scope.$watch('header.datePicker', function(val) {
      if(!moment($scope.vacation.data.startTravelDate).isSame(val.startDate, 'day') || !moment($scope.vacation.data.endTravelDate).isSame(val.endDate, 'day')){
        var emptyDays = Math.max(getDateDiff($scope.vacation.data.startTravelDate, $scope.vacation.data.endTravelDate) - parseInt($scope.vacation.data.visitDays), 0);
        $scope.vacation.data.startTravelDate = moment(val.startDate).toDate();
        $scope.vacation.data.endTravelDate = moment(val.endDate).toDate();
        var newTotalDays = getDateDiff($scope.vacation.data.startTravelDate, $scope.vacation.data.endTravelDate);
        $scope.vacation.data.visitDays = Math.min(Math.max((newTotalDays - emptyDays),0),11);
        var saveData = angular.copy($scope.vacation.data);
        $scope.schedulerConfig.saves.push(saveData);
        mousePlannerService.vacation.update(saveData)
        .then(function(res){
          $scope.schedulerConfig.saveComplete(saveData);
          $scope.reoptimize();
        });
      }
    });

    $scope.blockTypes = [
      {name: 'Park', cssClass: 'object-box--default', block:{block_type_id: 1}},
      {name: 'Resort', cssClass: 'object-box--warning', block:{block_type_id: 8}},
      {name: 'Dining', cssClass: 'object-box--primary', block:{block_type_id: 9}},
      {name: 'Transit', cssClass: 'object-box--inverse', block:{block_type_id: 10}},
      {name: 'Custom', cssClass: 'object-box--custom', block:{block_type_id: 13}}
    ];

    $scope.mobileVariables = {
      activeContentMobile: false
    };

    $scope.prevDay = function(){
        var index = $scope.header.currentDay;
        if(index > 0) {
          $scope.header.currentDay --;
        }
      if($scope.schedulerConfig.viewActive === 'week') {
        var daysContainer = angular.element(document.querySelector('#container-days'));
        var containerOffset = daysContainer.prop('offsetLeft');
        var activeDay = angular.element(document.querySelector('.scheduler__calendar--day.active'));
        var dayOffset = activeDay.prop('offsetLeft') - containerOffset;
        daysContainer.prop('scrollLeft', dayOffset - activeDay.width());
      }
    };

    $scope.nextDay = function(){
      var index = $scope.header.currentDay;
      if(index+1 < $scope.vacation.data.mouse_plan.days.length) {
        $scope.header.currentDay ++;
      }
      if($scope.schedulerConfig.viewActive === 'week') {
        var daysContainer = angular.element(document.querySelector('#container-days'));
        var containerOffset = daysContainer.prop('offsetLeft');
        var activeDay = angular.element(document.querySelector('.scheduler__calendar--day.active'));
        var dayOffset = activeDay.prop('offsetLeft') - containerOffset;
        daysContainer.prop('scrollLeft', dayOffset + activeDay.width());
      }
    };

    $scope.viewDay = function(i){
      var minDate = $scope.vacation.data.mouse_plan.days[0].date;
      if(i > -1){
        $scope.header.currentDay = i;
      }
    };

    var minHeightBlockLogo = 82;
    var minHeightBlock = 55;
    var minHeightBlockFastPass = 100;
    var marginBottomBlock = 8;
    var minutes = 60;
    var hours = 24;
    var oneHourInMinutes = 60;
    var startDayInHour = 7;
    var startDayInMinutes = startDayInHour * minutes;
    var oneMinute = 1;
    var endDayInMinutes = hours * minutes - startDayInMinutes - oneMinute;
    var totalMinutesAvailablePerDay = hours * minutes - startDayInMinutes - oneMinute;
    var milliseconds = 1000;
    var showDay;
    var marginLeftBlocks = 1;
    var maxMinutesPark = 840;
    // don't re-write this variables in this way var [endDayMaxHours, endDayMaxMinutes] = configPage.endDayMax.split(":");
    // it's crash build on the server side
    var endDayMax = configPage.endDayMax.split(":"),
        endDayMaxHours= endDayMax[0],
        endDayMaxMinutes = endDayMax[1];

    $scope.block_types = scheduler[0]; //lets try... calling block_types block_types (not... parks)!
    $scope.vacation.mousages = scheduler[1];
    $scope.main.checklist = scheduler[2];

    $scope.toUTCDate = function(date){
      return moment(moment(date).utc().valueOf() - moment().utcOffset()*60000).toDate();
    };

    // NOTE: FastPass+ will be hidden for the beta release - March, 2017
    // $scope.$watch('vacation.options.experiences', function(experiences){
    //     if(!experiences){
    //       return;
    //     }
    //     $scope.fastpass = $filter('experienceFastpassPlus')(experiences);
    // });

    $scope.addSubBlock = function(event, ui, day, block){
      var newBlocks = $filter('filter')(day.blocks, function(block) {
        return (block.created);
      },true);
      var scopeBlock = $filter('orderBy')(newBlocks, 'created', true)[0];

      var type = $filter('blockType')(scopeBlock.block_type_id, $scope.block_types).name;
      var parentType = $filter('blockType')(block.block_type_id, $scope.block_types).name;
      if(configPage.blockParents.indexOf(type) === -1 && configPage.blockParents.indexOf(parentType) !== -1){ //if dropped in parent
        scopeBlock.parent_id = block.id;
        if(new Date(scopeBlock.startTime) < new Date(block.startTime)){
          scopeBlock.startTime = new Date(block.startTime);
          var duration = $filter('filter')(configPage.blockDefaultDuration, {type: type})[0].duration;
          var endTime = new Date(new Date(scopeBlock.startTime).setMinutes(new Date(scopeBlock.startTime).getMinutes()+duration));
        }
        if(new Date(scopeBlock.endTime) > new Date(block.endTime)){
          scopeBlock.endTime = new Date(block.endTime);
        }
      }
    };

    // start resize directive
    $scope.$on('angular-resizable.resizeStart', function(event, block){

      //  merge the the original block with the resizable properties
      getScopedBlock(block.id, block);
    });

    // while resizing show time based on height
    $scope.$on('angular-resizable.resizing', function(event, block) {

      // set the new duration based on our tracked height
      var duration = block.height;

      // create a newEndTime by adding the duration in minutes
      var currentEndTime = new Date(new Date(block.startTime).setMinutes(new Date(block.startTime).getMinutes(block.startTime)+duration));
    });

    // when resizing stops, save time based on height
    $scope.$on('angular-resizable.resizeEnd', function(event, block) {

      // make a copy of the original block
      var newBlock = angular.copy(block);

      // set the new duration based on our tracked height
      var duration = newBlock.height;

      // create a newEndTime by adding the duration in minutes
      var newEndTime = new Date(new Date(newBlock.startTime).setMinutes(new Date(newBlock.startTime).getMinutes(newBlock.startTime)+duration));

      // convert our newEndTime
      newBlock.endTime = moment(newEndTime).utc();

      // set max end time according configPage.endDayMax
      var maxEndTime = $filter('parseTime')(moment(newBlock.startTime).set({'hours' : endDayMaxHours, 'minutes' : endDayMaxMinutes}), moment(newBlock.startTime).add(1,'days')),
        //this variable needs for correct end point block is being changed(resize,drag and drop, timepicker change)
        maxEndPoint = moment(moment(maxEndTime).utc().valueOf() + moment().utcOffset()*60000).toDate();

      //cut blocks when endTime more than available
      if ( newEndTime > maxEndPoint ) {
        newBlock.endTime = maxEndPoint;
      }

      // need to trim the 'block_' string on the id
      var blockIDNumber = newBlock.id.replace('block_','');
      newBlock.id = parseInt(blockIDNumber);
      block.id = parseInt(blockIDNumber);
      
      updateBlock(newBlock, block);// update back end
      updateScopedBlock(block); // update front end
    });
    //end resize

    var getDayBlocks = function(block){
      if(block.day_id){
        var previousDay = $filter('filter')($scope.vacation.data.mouse_plan.days, {id: block.day_id}, true);
        if(previousDay.length){
          return previousDay[0].blocks;
        }
      }
      return [];
    };

    var spliceBlock = function(block, blocks){
      if(!blocks){
        blocks = getDayBlocks(block);
      }
      var previousBlockIndex = blocks.indexOf(block);
      if(previousBlockIndex !== -1){
        blocks.splice(previousBlockIndex, 1); //remove previous block
        return previousBlockIndex;
      }
      return -1;
    };

    $scope.addDayBlock = function(event, ui, day){
      var scopeBlock = (ui.draggable.scope().blockType && ui.draggable.scope().blockType.block) ? ui.draggable.scope().blockType.block : ui.draggable.scope().block;
      var dayBlocks = getDayBlocks(scopeBlock);
      var config = angular.copy(scopeBlock);
      var blockType = $filter('blockType')(config.block_type_id, $scope.block_types);
      var blockDefaults = {
        id: 0,
        name: 'New '+blockType.name+' block',
        location: null
      };
      if(blockType && blockType.association_id){
        blockDefaults.association_id = blockType.association_id;
      }
      config = angular.extend(blockDefaults, config); //merge existing block with some defaults
      var top = ui.offset.top - angular.element(event.target).offset().top;
      var duration = $filter('filter')(configPage.blockDefaultDuration, {type: blockType.name})[0].duration;
      if(config.startTime && config.endTime && !isNaN(new Date(config.startTime).getTime()) && !isNaN(new Date(config.endTime).getTime())){
        var s = new Date(config.startTime);
        var s1 = moment($filter('parseUtcTime')($scope.vacation.data.beginDayTime, config.startTime).valueOf() + moment().utcOffset() * 60000).toDate();
        if(s < s1){
            s = s1;
        }
        duration = parseInt(Math.abs(new Date(config.endTime) - s)/60000);
      }
      var hours = parseInt(top/60) + new Date($scope.vacation.data.beginDayTime).getHours();
      var minutes = $filter('minuteStep')(parseInt(top) + new Date($scope.vacation.data.beginDayTime).getMinutes());
      var selectedDate = $scope.toUTCDate(day.date);
      var startTime = new Date(new Date(selectedDate).getFullYear(), new Date(selectedDate).getMonth(), new Date(selectedDate).getDate(), hours, minutes, 0, 0);
      if($filter('timeToMinutes')($scope.vacation.data.beginDayTime, true) > $filter('timeToMinutes')(startTime, true)){
        startTime = $filter('parseTime')($scope.vacation.data.beginDayTime, startTime);
      }
      var endTime = new Date(new Date(startTime).setMinutes(startTime.getMinutes()+duration));
      var newBlock = angular.extend(config, {day_id: day.id, parent_id: null, startTime: startTime, endTime: endTime, created: new Date()});
      var previousIndex = spliceBlock(scopeBlock, dayBlocks);
      if(previousIndex !== -1){
        day.blocks.splice(previousIndex, 0, newBlock);
      }
      else{
        day.blocks.push(newBlock);
      }
      if(!scopeBlock.parent_id){ //if block is a parent, find child blocks to move them as well
        var children = $filter('filter')(dayBlocks, {parent_id: scopeBlock.id}, true);
        for(var i = 0; i < children.length; i++){
          var child = angular.copy(children[i]);
          angular.extend(child, {day_id: day.id});
          var childStart = new Date(child.startTime);
          var childDuration = parseInt(Math.abs(new Date(child.endTime) - childStart)/60000);
          child.startTime = new Date(new Date(selectedDate).getFullYear(), new Date(selectedDate).getMonth(), new Date(selectedDate).getDate(), childStart.getHours(), childStart.getMinutes(), 0, 0);
          child.endTime = new Date(new Date(child.startTime).setMinutes(new Date(child.startTime).getMinutes()+childDuration));
          spliceBlock(children[i], dayBlocks);
          if($filter('timeToMinutes')(child.startTime, true) < $filter('timeToMinutes')(newBlock.startTime, true) || $filter('timeToMinutes')(child.endTime, true) > $filter('timeToMinutes')(newBlock.endTime, true)){
            child.parent_id = null; //unlink the child from the parent when child starts earlier than parent or ends later than parent
          }
          else{
            child.parent_id = newBlock.id;
          }
          updateBlock(child, children[i]);
          day.blocks.push(child);
        }
      }
      $timeout(function(){ //need to set a timeout because the parent_id gets set by the drag and drop function that gets called immediately after this one.
        if(!newBlock.id > 0){ //if block is new, post
          createBlock(newBlock)
          .then(function(res){
            if(blockType.name === 'park' || blockType.name === 'resort'){ //this is a new park block so we want to open the edit menu
              $scope.editBlock(newBlock);
            }
          });
        }
        else{ //if block is not new, put
          updateBlock(newBlock, scopeBlock);
        }
      },300);
    };

    var createBlock = function(block){
      var original = angular.copy(block);
      if(original.id === 0){
        delete original.id;
      }
      $scope.schedulerConfig.saves.push(original);
      return mousePlannerService.block.create(original)
      .then(
          function(res){
            angular.extend(block, res.block);
            $scope.history.push({type: 1, list: 'blocks', data: angular.copy(block)});
            $scope.vacation.data.mouse_plan.tripScore = res.tripScore;
            $scope.schedulerConfig.saveComplete(original);
            return block;
          },
          function(err){
            $scope.config.notification.error();
            return err;
          });
    };

    var updateBlock = function(block, original){
      $scope.history.push({type: 2, list: 'blocks', data: angular.copy(original)}); //log original to history list
      angular.extend(original, block); //extend original in view
      var saveData = angular.copy(block);
      $scope.schedulerConfig.saves.push(saveData);
      return mousePlannerService.block.update(saveData)
      .then(
        function(res){
          $scope.vacation.data.mouse_plan.tripScore = res.tripScore;
          $scope.schedulerConfig.saveComplete(saveData);
          return angular.extend(block, res.block);
        },
        function(err){
          $scope.config.notification.error();
          return err;
        });
    };

    // update a block with what is in the scope
    var getScopedBlock = function(id, block){

      var originalBlock = block;
      var blockID = id.replace('block_','');
      var blockIDNumber = parseInt(blockID);

      $scope.vacation.data.mouse_plan.days.forEach(function(day) {
        day.blocks.forEach(function(block){
          if(block.id === blockIDNumber) {
            angular.extend(originalBlock, block);
            return block;
          }
        });
      });
    };

    // update the scope with what is in the block
    var updateScopedBlock = function(updatedBlock){
      var originalBlock = angular.copy(updatedBlock);

      $scope.vacation.data.mouse_plan.days.forEach(function(day) {
          if(day.id === updatedBlock.day_id) {

            var dayBlocks = angular.copy(day.blocks);

            day.blocks.forEach(function(block){
              if(block.id === updatedBlock.id) {
                var spliceBlock = function(b){
                  var blockIndex = day.blocks.indexOf(b);
                  if(blockIndex !== -1){
                    day.blocks.splice(blockIndex, 1, originalBlock); // take the block out but put the original back into the index at the correct position to trigger the calculations of the add more placeholder
                  }
                };
                spliceBlock(block);
              }
            });
          }
      });
    };

    var deleteBlock = function(block){
      $scope.history.push({type: 3, list: 'blocks', data: angular.copy(block)}); //log original to history list
      mousePlannerService.block.delete(block.id)
      .then(
        function(res){
          $scope.vacation.data.mouse_plan.tripScore = res.tripScore;
          return null;
        },
        function(err){
          return err;
        });
    };

    $scope.deleteBlock = function(block){
        $scope.form = {
            description: "Are you sure you want to delete this?",
            modal: ngDialog.open({
                template: 'views/templates/confirm-modal.html',
                scope: $scope
            }),
            submit: function(form){
                if(!form.$valid){
                    return;
                }
                var day = $filter('filter')($scope.vacation.data.mouse_plan.days, {id: block.day_id}, true);
                var spliceBlock = function(b){
                    deleteBlock(b);
                    var blockIndex = day.blocks.indexOf(b);
                    if(blockIndex !== -1){
                        day.blocks.splice(blockIndex, 1);
                    }
                };
                if(day.length){
                    day = day[0];
                    spliceBlock(block);
                    if(!block.parent_id){ //if is parent block
                        $filter('filter')(day.blocks, {parent_id: block.id}, true).map(spliceBlock); //remove any child blocks as well
                    }
                }
                this.modal.close();
            }
        };
    };

    $scope.changeChecklistRead = function(mousage, index){
      mousePlannerService.externalService.mousage.put(mousage.id, mousage).then(function (response){
      });
    };


    $scope.addMessageToCheklist = function (mousage, index){

      $scope.isConfirmMoveMousageToCheklist = true;
      $scope.mousageExistsInCheklist = false;

      mousePlannerService.externalService.checklist.get().then(function(response){

        if(response && response.length > 0){

          for (var item in response) {
            if( mousage.type === response[item].type ){
              $scope.mousageExistsInCheklist = true;
              break;
            }
          }

          if($scope.mousageExistsInCheklist)
          {
            $scope.isConfirmMoveMousageToCheklist = false;
            ngDialog.open({
              template: 'views/templates/confirm-modal-delete-mousages.html',
              scope: $scope,
              controller: ['$scope', function($scope){

              }]
            });
          }

          if($scope.isConfirmMoveMousageToCheklist){
            mousePlannerService.externalService.mousage.post(mousage.id, mousage).then(function (response){
              _loadMousages($scope.mouseplan.mouse_plan_id);
            });
          }
        }
      });

      $scope.ConfirmMoveMousageToCheklist = function(){
        $scope.isConfirmMoveMousageToCheklist = true;
        mousePlannerService.externalService.mousage.post(mousage.id, mousage).then(function (response){
          _loadMousages($scope.mouseplan.mouse_plan_id);
        });
      };
    };

    $scope.history = [angular.copy($scope.vacation.data.mouse_plan)]; //initialize history list with initial mouseplan
    $scope.days = [];
    var days = [];
    var daysOrigin = [];


    function formatDD(number) {
      if (number  < 10) {number = '0' + number;}
      return number;
    }

    $scope.savePlan = function(){
      var saveData = angular.copy($scope.vacation.data.mouse_plan);
      mousePlannerService.mouseplan.update(saveData)
      .then(
        function(){
          console.log('mouseplan saved');
          $scope.schedulerConfig.saveComplete(saveData);
        },
        function(){
          console.log('error saving mouseplan');
        }
      );
      $scope.schedulerConfig.saves.push(saveData);
    };

    $scope.undo = function(){
      if($scope.history && $scope.history.length > 1){
        $scope.vacation.data.mouse_plan = angular.copy($scope.history[$scope.history.length - 2]);
        $scope.history.splice($scope.history.length-1, 1); //remove last item
      }
    };

    function _processMousePlan(_days){

      _days = $filter('orderBy')(_days, 'day_number');

      var processDays = [];
      for(var d = 0; d < _days.length; d++){
        var day = {
          day_id: _days[d].day_id,
          day_number: _days[d].day_number,
          blocks: []
        };

        day.date = new Date(_days[d].date.replace( /(\d{4})-(\d{2})-(\d{2})/, "$2/$3/$1"));
        for(var b = 0; b < _days[d].blocks.length; b++){
          var startTime = _days[d].blocks[b].startTime.split(':');
          var endTime = _days[d].blocks[b].endTime.split(':');

          var startTimeHour = parseInt(startTime[0]);
          var startTimeMinute = parseInt(startTime[1]);

          var endTimeHour = parseInt(endTime[0]);
          var endTimeMinute = parseInt(endTime[1]);

          var startTimeInMinutes = startTimeHour * 60 + startTimeMinute - 7 * 60;
          var endTimeInMinutes = endTimeHour * 60 + endTimeMinute - 7 * 60;

          var type = getTypeBlockById(_days[d].blocks[b].block_type_id);

          var block = {
            id: _days[d].blocks[b].block_id,
            block_id: _days[d].blocks[b].block_id,
            split: _days[d].blocks[b].split,
            block_type_id: _days[d].blocks[b].block_type_id,
            type: type,
            name: _days[d].blocks[b].name,
            startTime: new Date(day.date.getFullYear(), day.date.getMonth(), day.date.getDate(), startTimeHour, startTimeMinute, 0, 0),
            endTime: new Date(day.date.getFullYear(), day.date.getMonth(), day.date.getDate(), endTimeHour, endTimeMinute, 0, 0),
            startTimeInMinutes: startTimeInMinutes,
            endTimeInMinutes: endTimeInMinutes,
            diffTimeInMinutes: endTimeInMinutes - startTimeInMinutes,
            anchored: _days[d].blocks[b].anchored === 1 ? true: false,
            parent_id: _days[d].blocks[b].parent_id,
            location: _days[d].blocks[b].location
          };

          if(type === 'park'){
            _completeBlockPark(block);
          }

          day.blocks.push(block);
        }

        processDays.push(day);
      }

      var processTemp = [];

      if(_days && _days.length > 0){

        $scope.header.datePicker.startDate = moment(new Date(_days[0].date.replace( /(\d{4})-(\d{2})-(\d{2})/, "$2/$3/$1")));
        $scope.header.datePicker.endDate = moment(new Date(_days[_days.length - 1].date.replace( /(\d{4})-(\d{2})-(\d{2})/, "$2/$3/$1")));

        var minDate = new Date($scope.header.datePicker.startDate.format('YYYY'), parseInt($scope.header.datePicker.startDate.format('M')) - 1, $scope.header.datePicker.startDate.format('D'), 0, 0, 0, 0);
        var maxDate = new Date($scope.header.datePicker.endDate.format('YYYY'), parseInt($scope.header.datePicker.endDate.format('M')) - 1, $scope.header.datePicker.endDate.format('D'), 23, 59, 59, 0);

        for(d = angular.copy(minDate); d <= maxDate; d.setDate(d.getDate() + 1)){
          var found = false;
          for(b = 0; processDays && b < processDays.length; b++){
            var minDateBlock = angular.copy(processDays[b].date);
            minDateBlock.setDate(minDateBlock.getDate() - 1);

            if(processDays[b].date.getFullYear() === d.getFullYear() && processDays[b].date.getMonth() === d.getMonth() && processDays[b].date.getDate() === d.getDate()){
              processTemp.push(processDays[b]);
              found = true;
              break;
            }
          }

          if(!found){
            processTemp.push({
              date: angular.copy(d),
              blocks: []
            });
          }
        }
      }
      daysOrigin = angular.copy(processTemp);

      _updateTextDate();
    }

    function getTypeBlockById(id){
      var type = '';
      switch(id) {
        case 1:
          type = 'park';
          break;
        case 2:
          type = 'park';
          break;
        case 3:
          type = 'park';
          break;
        case 4:
          type = 'park';
          break;
        case 5:
          type = 'park';
          break;
        case 6:
          type = 'park';
          break;
        case 7:
          type = 'park';
          break;
        case 8:
          type = 'resort';
          break;
        case 9:
          type = 'dining';
          break;
        case 10:
          type = 'transit';
          break;
        case 11:
          type = 'fixed';
          break;
        case 12:
          type = 'fixed';
          break;
        default:
          type = '';
      }
      return type;
    }

    function _completeBlockPark(_block){
      if(_block.block_type_id === 1){
        _block.class = 'object-box--blue';
        _block.img = 'imagic.png';
        _block.inPlan = true;
      } else if(_block.block_type_id === 2){
        _block.class = 'object-box--green';
        _block.img = 'ianimalkingdom.png';
        _block.inPlan = true;
      } else if(_block.block_type_id === 3){
        _block.class = 'object-box--purple';
        _block.img = 'iepcot.png';
        _block.inPlan = true;
      } else if(_block.block_type_id === 4){
        _block.class = 'object-box--yellow';
        _block.img = 'ihollywood.png';
        _block.inPlan = true;
      } else if(_block.block_type_id === 5){
        _block.class = 'object-box--v';
        _block.img = 'isprings.png';
        _block.inPlan = false;
      } else if(_block.block_type_id === 6){
        _block.class = 'object-box--light-blue';
        _block.img = 'iblizzard-each.png';
        _block.inPlan = false;
      } else if(_block.block_type_id === 7){
        _block.class = 'object-box--light-blue';
        _block.img = 'ityphoon.png';
        _block.inPlan = false;
      }

      _block.fastpass = angular.copy($scope.fastpass);
      for(var fp = 0; fp < _block.fastpass.length; fp++){
        _block.fastpass.startTime = angular.copy(_block.startTime);
      }
    }

    $scope.daysForViewDays = [];

    function renderScheduler(){
      //dummy to avoid errors (since i got rid of the renderScheduler function below)
      console.log('renderScheduler was called but no longer exists');
    }

    $('.js-addblock-mobile').click(function(e){


      var body = $('body');
      var layer = $('#layer-popup');
      var actionPopup = $('#action-popup');

      actionPopup.show();
      body.addClass('no-scroll');
      layer.show();

    });

    $scope.onlyMobile = {

    };
    $scope.minDateMobile = $scope.header.datePicker.startDate;
    $scope.maxDateMobile = $scope.header.datePicker.endDate;

    $scope.addBlockMobile = function(config){
      //this variables need for displaying correct time on view
      $scope.localVariable = {
        startTime: $filter('parseTime')(moment(moment(config.startTime).utc().valueOf() - moment().utcOffset()*60000).toDate(), config.startTime),
        endTime: $filter('parseTime')(moment(moment(config.endTime).utc().valueOf() - moment().utcOffset()*60000).toDate(), config.startTime),
      };
      //this variables need for displaying correct time on view
      config = config || {};
      var getModal = function(){
        if(!config.startTime){
          return ngDialog.open({
            template: "views/templates/new-block.html",
            scope: $scope,
            disableAnimation: true
          });
        }
      };
      var addMinutes = function(date, minutes){
        var dt = new Date(date);
        return new Date(dt.setMinutes(dt.getMinutes()+minutes));
      };
      var updateEndTime = function(){
        if($scope.form.data.startTime){
          var duration = parseInt($scope.main.config.minuteStep);
          if($scope.form.type){
            duration = $filter('filter')(configPage.blockDefaultDuration, {type: $scope.form.type.name})[0].duration;
          }
          $scope.form.data.endTime = addMinutes($scope.form.data.startTime, duration);
          //re-write local variables to the correct value in db
          $scope.form.data.endTime = moment(moment($scope.localVariable.endTime).utc().valueOf() + moment().utcOffset()*60000).toDate();
          //re-write local variables to the correct value in db
          //don't remove, its need for setting max time value for new added block
          // $scope.form.data.maxEndUtc = $filter('parseUtcTime')($scope.vacation.data.endDayTime, $scope.form.data.endTime);
          $scope.form.data.maxEndUtc = $filter('parseTime')(moment($scope.form.data.startTime).set({'hours' : endDayMaxHours, 'minutes' : endDayMaxMinutes}), moment($scope.form.data.startTime).add(1,'days'));
          //don't remove, its need for setting max time value for new added block
        }
      };
      var updateStart = function(date){
        if(config.startTime && !$scope.form.data.startTime){
          //re-write local variables to the correct value in db
          $scope.form.data.startTime = moment(moment($scope.localVariable.startTime).utc().valueOf() + moment().utcOffset()*60000).toDate();
          //re-write local variables to the correct value in db
        }
        else{
          $scope.form.data.startTime = $filter('parseUtcTime')($scope.vacation.data.beginDayTime, new Date(date));
         $scope.form.data.startTime = moment($scope.form.data.startTime).utc();
        }
        updateEndTime();
        //don't remove, its need for setting min time value for new added block
        $scope.form.data.beginUtc = $filter('parseUtcTime')($scope.vacation.data.beginDayTime, $scope.form.data.startTime);
        //don't remove, its need for setting min time value for new added block
      };

      $scope.editBlockProps = null;

      $scope.form = {
        page: 1,
        data: {
          id: 0,
          name: "",
          location: null
        },
        config: {
          startChanged: function(){
            var check = addMinutes($scope.form.data.startTime, parseInt($scope.main.config.minuteStep));
            if(!$scope.form.data.endTime || new Date($scope.form.data.endTime) < check){
              $scope.form.data.endTime = check;
            }
          },
          endChanged: function(){
            var check = addMinutes($scope.form.data.endTime, (-1*parseInt($scope.main.config.minuteStep)));
            if(!$scope.form.data.startTime || new Date($scope.form.data.startTime) > check){
              $scope.form.data.startTime = check;
            }
          },
          typeChanged: function(){
            if($scope.form.data.block_type_id){
              $scope.form.type = $filter('blockType')($scope.form.data.block_type_id, $scope.block_types);
              if($scope.form.type){
                updateEndTime();
                if($scope.form.type.association_id){
                  $scope.form.data.association_id = $scope.form.type.association_id;
                }
              }
            }
          },
          dateChanged: function(){
            $scope.form.data.day_id = $scope.form.day.id;
            updateStart($scope.form.day.date);
          },

        },
        modal: getModal(),
        save: function(form){
          if(!form.$valid){
            return;
          }
          var day = $scope.form.day;
          var saveData = angular.copy(this.data);
          $scope.schedulerConfig.saves.push(saveData);
          createBlock(saveData)
          .then(
            function(res){
              day.blocks.push(res);
              $scope.schedulerConfig.saveComplete(saveData);
            }
          );
          this.close();
        },
        back: function(){
          if(this.page === 1){
            $scope.scroll();
            return this.close();
          }
          return this.page--;
        },
        next: function(form){
          if(form.$valid){
            this.page++;
          }
        },
        close: function(){
          if(this.modal){
            this.modal.close();
          }
          else if(config){
            config.formOpen = false;
          }
        },
        autocomplete: {
          restaurants: {
            select: function(restaurant){
              $scope.form.data.location = restaurant.location;
            },
            get: function(nameRestaurant){
              if(!parent || !parent.association_id){ //if is parent block
                return [];
              }
              var type = $filter('blockType')(parent.block_type_id, $scope.block_types);
              if(type && type.name === 'park'){
                var def = $q.defer();
                mousePlannerService.restaurants.park(parent.association_id).then(function(res){
                  res = $filter('filter')(res, {name: nameRestaurant});
                  res = $filter('limitTo')(res, 5);
                  def.resolve(res);
                });
                return def.promise;
              }
              else if(type && type.name === 'resort'){
                var def = $q.defer();
                mousePlannerService.restaurants.resort(parent.association_id).then(function(res){
                  res = $filter('filter')(res, {name: nameRestaurant});
                  res = $filter('limitTo')(res, 5);
                  def.resolve(res);
                });
                return def.promise;
              }
            }
          }
        }
      };

      if(config.dayId){
        $scope.form.day = $filter('filter')($scope.vacation.data.mouse_plan.days, {id: config.dayId}, true)[0];
        $scope.form.config.dateChanged();
      }
    };

    $scope.changeDateMobile = function(modelName, newValue){
      if(modelName === 'onlyMobile.date'){
        if(newValue){
          for (var i = 0; i < daysOrigin.length; i++) {
            if(moment(newValue).format('YYYY') === daysOrigin[i].date.getFullYear() &&
              parseInt(newValue.format('M')) - 1 === daysOrigin[i].date.getMonth() &&
              newValue.format('D') === daysOrigin[i].date.getDate()){
              $scope.onlyMobile.dailyIndex = i;
              $scope.onlyMobile.nextStep = true;
              break;
            }
          }

          position.block.top = '0px';
          configPopupAction(undefined, '');
        }
      }
    };

    // Drag and drop
    var dragSrcElement = null;
    var cols = angular.element('.object-box');
    var column = angular.element('.object-box--empty');

    [].forEach.call(cols, function(col) {
      col.addEventListener('dragstart', handleDragStart, false);
      col.addEventListener('dragend', handleDragEnd, false);
    });

    function handleDragStart(e) {
      var parent = $document[0].createElement('div');
      var clone = this.cloneNode(true);
      dragSrcElement = this;

      this.classList.add('object-box--moving');
      $('.object-box--transparent').show();
      parent.appendChild(clone);

      e.dataTransfer.effectAllowed = 'move';
      e.dataTransfer.setData('element', parent.innerHTML);
    }

    function handleDragEnd(e) {
      this.classList.remove('object-box--moving');
      setTimeout(function () {
        $('.object-box--transparent').hide();
      }, 1000);
    }

    function handleDragOver(e) {
      this.classList.add('object-box--dragover');
      if (e.preventDefault) {
        e.preventDefault();
      }
      e.dataTransfer.dropEffect = 'move';
      return false;
    }

    function handleDragEnter(e) {
      this.classList.remove('object-box--dragover');
    }
    function handleDragLeave(e) {
      this.classList.remove('object-box--dragover');
    }

    var position = {
      mouse: {
        top: 0,
        left: 0
      },
      block: {
        top: 0,
        left: 0
      }
    };

    function handleDrop(e) {

      this.classList.remove('object-box--dragover');
      // this / e.target is current target element.
      if (e.stopPropagation) {
        e.stopPropagation(); // stops the browser from redirecting.
      }

      if (dragSrcElement !== this) {

        var parent = $document[0].createElement('div');
        parent.innerHTML = e.dataTransfer.getData('element');

        var child = parent.firstChild;
        var type = child.getAttribute('type');

        position.block.top = (e.clientY - this.getBoundingClientRect().top) + 'px';
        position.mouse.top = e.clientY;
        position.parent = {
          top: e.clientY - this.getBoundingClientRect().top,
          left: e.clientX - this.getBoundingClientRect().left,
          width: this.getBoundingClientRect().width,
          height: this.getBoundingClientRect().height
        };
        configPopupAction(this, type);
      }
      return false;
    }

    function configPopupAction(blockElement, type){

      var top = position.block.top;
      var heightBlock = 0;
      var daily = 0;
      var dailyIndex = $scope.onlyMobile.dailyIndex;

      if(blockElement !== undefined){
        heightBlock = blockElement.offsetHeight + marginBottomBlock;
        daily = blockElement.parentNode.parentNode;
        dailyIndex = parseInt(daily.getAttribute("data-index"));
      }

      if(blockElement && position.block.bounding){
        top = '' + position.block.bounding.top;
        heightBlock = position.block.bounding.height;
      }

      var startTimeInMinutes = parseInt(top.replace('px', ''));

      var startTime = new Date(daysOrigin[dailyIndex].date.getTime() + ((startTimeInMinutes + startDayInMinutes) * minutes * milliseconds));
      var endTime = new Date(daysOrigin[dailyIndex].date.getTime() + ((startTimeInMinutes + heightBlock + startDayInMinutes) * minutes * milliseconds));

      var startTimeView = new Date(daysOrigin[dailyIndex].date.getFullYear(), daysOrigin[dailyIndex].date.getMonth(), daysOrigin[dailyIndex].date.getDate(), 7, 0, 0, 0);
      var endTimeView = new Date(daysOrigin[dailyIndex].date.getFullYear(), daysOrigin[dailyIndex].date.getMonth(), daysOrigin[dailyIndex].date.getDate(), 23, 59, 59, 0);

      var parksBellow = $filter('filter')(daysOrigin[dailyIndex].blocks, function(item){
        return startTimeInMinutes <= item.startTimeInMinutes && (item.type === 'park' || item.type === 'resort');
      });

      var blocksBellow = $filter('filter')(daysOrigin[dailyIndex].blocks, function(item){
        return startTimeInMinutes <= item.startTimeInMinutes;
      });

      blocksBellow = $filter('filter')(blocksBellow, function(item){
        for(var i = 0; i < parksBellow.length; i++){
          if(parksBellow[i].startTimeInMinutes < item.startTimeInMinutes &&
            parksBellow[i].endTimeInMinutes > item.startTimeInMinutes && item.type !== 'park' && item.type !== 'resort'){
            return false;
          }
        }
        return true;
      });

      var minutesAvailableParkResort = totalMinutesAvailablePerDay - startTimeInMinutes;
      for(var i = 0; i < blocksBellow.length; i++){
        minutesAvailableParkResort = minutesAvailableParkResort - blocksBellow[i].diffTimeInMinutes;
      }

      if($scope.block.new.action !== 'edit'){

        if(minutesAvailableParkResort <= 0){
          return;
        }

        endTime = new Date(daysOrigin[dailyIndex].date.getTime() + ((startTimeInMinutes + minutesAvailableParkResort + startDayInMinutes) * minutes * milliseconds));
      } else {
        startTime = new Date($scope.block.new.startTime);
        endTime = new Date($scope.block.new.endTime);
      }

      if($scope.onlyMobile &&
      $scope.onlyMobile.dailyIndex){
        endTime = new Date(daysOrigin[dailyIndex].date.getTime() + ((startDayInMinutes + endDayInMinutes) * minutes * milliseconds));
      }

      $scope.block.new.startTimeInMinutes = startTimeInMinutes;
      $scope.block.new.endTimeInMinutes = startTimeInMinutes + heightBlock;
      $scope.block.new.type = type;
      $scope.block.new.dailyIndex = dailyIndex;
      $scope.block.new.startTime = startTime;
      $scope.block.new.startTimeView = startTimeView;
      $scope.block.new.endTime = endTime;
      $scope.block.new.endTimeView = endTimeView;

      if(type === 'park'){
        $scope.block.new.startTimeView = new Date(daysOrigin[dailyIndex].date.getFullYear(), daysOrigin[dailyIndex].date.getMonth(), daysOrigin[dailyIndex].date.getDate(), 8, 0, 0, 0);
        $scope.block.new.endTimeView = new Date(daysOrigin[dailyIndex].date.getFullYear(), daysOrigin[dailyIndex].date.getMonth(), daysOrigin[dailyIndex].date.getDate(), 22, 0, 0, 0);
      }

      var blocks = $filter('filter')(daysOrigin[$scope.block.new.dailyIndex].blocks, function(item){
        return item.startTimeInMinutes > startTimeInMinutes && (item.type === 'custom' || item.type === 'dining' || item.type === 'transit' || item.type === 'fixed');
      });

      var minutesAvailable = totalMinutesAvailablePerDay - startTimeInMinutes;
      for(i = 0; i < blocks.length; i++){
        minutesAvailable = minutesAvailable - blocks[i].diffTimeInMinutes;
      }

      if(minutesAvailable <= 0){
        return;
      }

      if($scope.block.new.steps === undefined){
        $scope.block.new.steps = [];
      }

      if(type === 'park'){
        $scope.block.new.steps.push('parks');
      } else if(type === 'fastpass'){
        $scope.block.new.steps.push('fastpass');
      } else if(type !== ''){
        $scope.block.new.steps.push('config');
      } else {
        $scope.block.new.steps.push('types');
      }

      if(position.parent){
        var xfind = (position.parent.left / position.parent.width) * 100;
        var blockParent = findBlockParentByXY(xfind, position.parent.top, $scope.days[dailyIndex].blocks);
        if(blockParent && blockParent.length > 0){
          $scope.block.new.parent_id = blockParent[0].id;
          if($scope.block.new.type === 'dining'){
            if(blockParent[0].type === 'park'){
              $scope.configAutocomplete.type = 'park';
              if(blockParent[0].name === 'Hollywood Studios' || blockParent[0].block_type_id === 4){
                $scope.configAutocomplete.parameter = 'HS';
              } else if (blockParent[0].name === 'Magic Kingdom' || blockParent[0].block_type_id === 1){
                $scope.configAutocomplete.parameter = 'MK';
              } else if (blockParent[0].name === 'Animal Kingdom' || blockParent[0].block_type_id === 2){
                $scope.configAutocomplete.parameter = 'AK';
              } else if (blockParent[0].name === 'Epcot' || blockParent[0].block_type_id === 3){
                $scope.configAutocomplete.parameter = 'EP';
              } else {
                $scope.configAutocomplete.parameter = '';
              }
            } else if(blockParent[0].type === 'resort'){
              $scope.configAutocomplete.type = 'resort';
              $scope.configAutocomplete.parameter = blockParent[0].resort_id;
            } else {
              $scope.configAutocomplete = {};
            }
          } else {
            $scope.configAutocomplete = {};
          }
        } else {
          $scope.configAutocomplete = {};
        }
      }

      position.block.bounding = undefined;
      position.parent = undefined;

      if(!$scope.$$phase) {
        $scope.$apply();
      }

      setTimeout(function () {
        showPopupAction(blockElement);
      }, 10);
    }

    function showPopupAction(block){
      var body = $('body');
      var layer = $('#layer-popup');
      var actionPopup = $('#action-popup');


      if(actionPopup !== undefined) {

        var position = getPositionPopupAction(actionPopup, block);
        setPositionTopIndicatorPopupAction(position.indicator.top);

        actionPopup.css({ 'top': '' + position.top + 'px' });
        actionPopup.css({ 'left': '' + position.left + 'px' });

        actionPopup.show();
        body.addClass('no-scroll');
        layer.show();
      }
    }

    function getPositionPopupAction(actionPopup, block){
      var _window = $(window);
      var rect = block.getBoundingClientRect();
      var _block = $(block);
      var marginBottom = 8;

      var top = position.mouse.top - actionPopup.outerHeight() / 2; // + _block.outerHeight() / 2;
      var left = rect.left + rect.width - 2;
      var topIndicator = actionPopup.outerHeight() / 2  - marginBottom;

      if(_window.height() < (top + actionPopup.outerHeight())) {
        var minus = top + actionPopup.outerHeight() - _window.height();
        top = top - minus;
        topIndicator = topIndicator + minus;
      }

      return {
        top: top,
        left: left,
        indicator: {
          top: topIndicator
        }
      };
    }

    function setPositionTopIndicatorPopupAction(top){
      var _head = $('head');
      _head.append('<style>#action-popup:before{ top: ' + top + 'px; }</style>');
    }

    $scope.saveFastPass = function(){
      var find = findBlockOriginalList($scope.block.new.parentId);
      if(find &&
        find.blocks &&
        find.block &&
        find.blocks.length > 0){

        find.blocks[find.index].fastpass = $scope.block.new.fastpass;

        $scope.history.push(angular.copy(daysOrigin));
      }

      $('#action-popup').hide();
      $('body').removeClass('no-scroll');
      $('#layer-popup').hide();
      renderScheduler();
    };

    $scope.scroll =  function(){
      if($('body').hasClass("no-scroll")){
        $('body').removeClass('no-scroll');
      }

      if($('html').hasClass("ngdialog-open")){
        $('html').removeClass("ngdialog-open");
      }
    };

    $scope.toggleSelected = function(block, isFastpass){
      if($scope.editBlockProps && $scope.editBlockProps.id === block.id){
        delete $scope.editBlockProps;
      }
      else{
          $scope.editBlockProps = {id: block.id, templateUrl: 'views/templates/edit-block.html', isFastpass: isFastpass};
      }
    };

    $scope.editBlock = function(block, isFastpass, selectedFastpass){
      var parent = $filter('parentBlock')(block, $scope.vacation.data.mouse_plan.days);
      var addMinutes = function(date, minutes){
        var dt = new Date(date);
        return new Date(dt.setMinutes(dt.getMinutes()+minutes));
      };
      $scope.$watch('form.data.block_type_id', function(val){ //set association_id when a new park is selected
        if(val){
          var newType = $filter('blockType')(val, $scope.block_types);
          if(newType && newType.association_id){
            $scope.form.data.association_id = newType.association_id;
          }
        }
        var $body = $('body'),
            $html = $('html');

        var windowWidth = $(window).innerWidth();

        if (windowWidth <= 1000){
          $body.addClass('no-scroll');
          $html.addClass("ngdialog-open");
        }

      });
      // $scope.$watch('form.data.name', function(val){ //change note name after editing !!! this repeats value at each block, need to refactor
      //     if (val) {
      //         block.name = val;
      //     }
      // });

      $scope.form = {
        type: (isFastpass ? {name: 'fastpass'} : $filter('blockType')(block.block_type_id, $scope.block_types)),
        block: angular.copy(block),
        data: (isFastpass ? angular.copy(selectedFastpass) : angular.copy(block)),
        dataStart: moment( moment(block.startTime).utc().valueOf() - moment().utcOffset()*60000).toDate(),
        dataEnd:  moment( moment(block.endTime).utc().valueOf() - moment().utcOffset()*60000).toDate(),
        config: {
          startChanged: function(){
            var check = addMinutes($scope.form.data.startTime, parseInt($scope.main.config.minuteStep));
            if(!$scope.form.data.endTime || new Date($scope.form.data.endTime) < check){
              $scope.form.data.endTime = check;
            }
          },
          endChanged: function(){
            var check = addMinutes($scope.form.data.endTime, (-1*parseInt($scope.main.config.minuteStep)));
            if(!$scope.form.data.startTime || new Date($scope.form.data.startTime) > check){
              $scope.form.data.startTime = check;
            }
          },
          beginUtc: $filter('parseUtcTime')($scope.vacation.data.beginDayTime, block.startTime),
          // set max end time according configPage.endDayMax
          maxEndUtc:  $filter('parseTime')(moment(block.startTime).set({'hours' : endDayMaxHours, 'minutes' : endDayMaxMinutes}), moment(block.startTime).add(1,'days')),
        },
        close: function(){
          $scope.scroll();
          $scope.toggleSelected(block);
        },
        save: function(form){
          if(!form.$valid){
            return;
          }
          if(isFastpass){
            for(var i = 0; i < selectedFastpass.length; i++){
              if(!$filter('filter')($scope.form.data, selectedFastpass[i].external_id, 'external_id').length){
                var index = $filter('idIndex')(selectedFastpass, selectedFastpass[i].external_id, 'external_id');
                mousePlannerService.fastpass.delete(selectedFastpass[i].id);
                selectedFastpass.splice(index, 1);
              }
            }
            angular.forEach($scope.form.data, function(item){
              // TODO: def is already defined
              var def;
              if(!item.id){
                def = mousePlannerService.fastpass.post(item);
                selectedFastpass.push(item);
              }
              else{
                def = mousePlannerService.fastpass.put(item);
              }
              $scope.schedulerConfig.saves.push(item);
              def.then(
                function(res){
                  angular.extend(item, res);
                  $scope.schedulerConfig.saveComplete(item);
                }
              );
            });
          }
          else{
            // rewrite block object dates by utc time ( $scope.form.data.startTime and $scope.form.data.endTime)
            this.data.startTime = moment(this.dataStart).utc(this.dataStart).format();
            this.data.endTime = moment(this.dataEnd).utc(this.dataEnd).format();
            updateBlock(this.data, block);
          }

          $scope.toggleSelected(block);
        },
        fastpassSelected: function(fastpass){
          return $filter('filter')(this.data, {external_id: fastpass.id}, true).length;
        },
        toggleFastpass: function(fastpass){
          if(this.fastpassSelected(fastpass)){
            var index = $filter('idIndex')(this.data, fastpass.id, 'external_id');
            this.data.splice(index, 1);
          }
          else{
            this.data.push({block_id: block.id, day_id: block.day_id, external_id: fastpass.id});
          }
        },
        autocomplete: {
          restaurants: {
            select: function(restaurant){
              $scope.form.data.location = restaurant.location;
            },
            get: function(nameRestaurant){
              if(!parent || !parent.association_id){ //if is parent block
                return [];
              }
              var type = $filter('blockType')(parent.block_type_id, $scope.block_types);
              if(type && type.name === 'park'){
                var def = $q.defer();
                mousePlannerService.restaurants.park(parent.association_id).then(function(res){
                  res = $filter('filter')(res, {name: nameRestaurant});
                  res = $filter('limitTo')(res, 5);
                  def.resolve(res);
                });
                return def.promise;
              }
              else if(type && type.name === 'resort'){
                var def = $q.defer();
                mousePlannerService.restaurants.resort(parent.association_id).then(function(res){
                  res = $filter('filter')(res, {name: nameRestaurant});
                  res = $filter('limitTo')(res, 5);
                  def.resolve(res);
                });
                return def.promise;
              }
            }
          }
        }
      };
      
      $scope.toggleSelected(block, isFastpass);

    };

    function loadNotes(block, type){
      $scope.block_note = block;
      $scope.block_note._type = type;
      if(type === 'day') {
        mousePlannerService.externalService.notes.get.day($scope.block_note.day_id).then(function(responseNotes){
          if(responseNotes && responseNotes.length > 0){
            $scope.block_note.notes = responseNotes;
          }
        });
      } else if(type === 'block') {
        var blockId = $scope.block_note.id;
        if($scope.block_note.type === 'logo'){
          blockId = $scope.block_note.parentId;
        }

        mousePlannerService.externalService.notes.get.block(blockId).then(function(responseNotes){
          if(responseNotes && responseNotes.length > 0){
            $scope.block_note.notes = responseNotes;
          }
        });
      }
    }

    $scope.notesBlock = loadNotes;

    function loadNotesInit(block, type){
      if(type === 'day' && block.day_id && block.day_id > 0) {
        mousePlannerService.externalService.notes.get.day(block.day_id).then(function(responseNotes){
          if(responseNotes && responseNotes.length > 0){
            block.notes = responseNotes;
          }
        });
      } else if(type === 'block' && block.id && block.id > 0) {
        var blockId = block.id;
        mousePlannerService.externalService.notes.get.block(blockId).then(function(responseNotes){
          if(responseNotes && responseNotes.length > 0){
            block.notes = responseNotes;
          }
        });
      }
    }

    $scope.editNoteViewDay = function (block, type, _note, index){

      $scope.block_note = block;
      $scope.block_note._type = type;
      $scope.block_note._note = _note;
      $scope.block_note._index = index;
      $scope.block_note._action = 'edit';

      _showModalNote();
    };

    $scope.getNotes = function(noteParent) {
      $scope.noteParent = noteParent;
      ngDialog.open({
        template: 'views/templates/notes-modal.html',
        className: 'ngdialog-theme-default modal--popover',
        scope: $scope,
      });
    };

    $scope.addNote = function(item){
      $scope.form = {
        modal: ngDialog.open({
          template: 'views/templates/note-modal.html',
          scope: $scope
        }),
        data: {
          noteable_id: item.id,
          noteable_type: (item.block_type_id ? 'blocks' : 'days')
        },
        submit: function(form){
          if(!form.$valid){
            $scope.config.notification.error();
            return;
          }
          var note = angular.copy(this.data);
          $scope.schedulerConfig.saves.push(note);
          mousePlannerService.note.post(note)
          .then(
            function(res){
              angular.extend(note, res);
              $scope.schedulerConfig.saveComplete(note);

            }
          );
          $scope.schedulerConfig.notes.push(note);
          this.modal.close();
        },
        cancel: function(){
          this.modal.close();
        }
      };
    };

     $scope.editNote = function(item){
       var index = $scope.schedulerConfig.notes.indexOf(item);
       $scope.form = {
        modal: ngDialog.open({
          template: 'views/templates/note-modal.html',
          scope: $scope
        }),
        data: {
          id: item.id,
          noteable_id: item.noteable_id,
          noteable_type: item.noteable_type,
          text: item.text
        },
        submit: function(form){
          if(!form.$valid){
            return;
          }
          var note = angular.copy(this.data);
          mousePlannerService.note.put(note);
          if(index !== -1){
            $scope.schedulerConfig.notes[index] = note;
          }
          this.modal.close();
        },
        cancel: function(){
          this.modal.close();
        }
      };
    };


    $scope.removeNote = function (note){
      var index = $scope.schedulerConfig.notes.indexOf(note);
      mousePlannerService.note.delete(note.id);
      if(index !== -1){
        $scope.schedulerConfig.notes.splice(index, 1);
      }
    };

    $scope.removeNoteViewDay = function (block, note, index){
      if(block.notes && block.notes.length > 0){
        block.notes.splice(index, 1);
        mousePlannerService.externalService.notes.delete(note.note_id).then(function(responseNotes){
        });
      }
    };

    function removeBlockOriginalList(blockId){

      var findBlock = findBlockOriginalList(blockId);

      if(findBlock.block && findBlock.blocks){
        findBlock.blocks.splice(findBlock.index, 1);

        $scope.history.push(angular.copy(daysOrigin));
      }
    }

    function findBlockOriginalList(blockId){
      var _blocks;
      var _block;
      var _index;
      var blockfound = false;
      if(daysOrigin) {
        for(var i = 0; i < daysOrigin.length && !blockfound; i++) {
          for(var j = 0; j < daysOrigin[i].blocks.length && !blockfound; j++) {
            if(daysOrigin[i].blocks[j].id === blockId) {
              _blocks = daysOrigin[i].blocks;
              _block = daysOrigin[i].blocks[j];
              _index = j;
              blockfound = true;
              break;
            }
          }
        }
      }

      return {
        blocks: _blocks,
        block: _block,
        index: _index
      };
    }

    function findBlockFromList(blockId, list){
      var _blocks;
      var _block;
      var _index;
      var blockfound = false;
      if(list) {
        for(var i = 0; i < list.length && !blockfound; i++) {
          if(list[i].id === blockId) {
            _block = list[i];
            _index = i;
            blockfound = true;
            break;
          }
        }
      }

      return {
        block: _block,
        index: _index
      };
    }

    function findBlockParentByXY(x, y, list){
      var blocks = $filter('filter')(list, function(item){
        return (item.type === 'park' || item.type === 'resort') &&
        item.startTimeInMinutes <= y &&
        item.endTimeInMinutes >= y && (
          item.properties === undefined ||
          item.properties === null ||
          (
            item.properties.leftNum - 1 <= x &&
            item.properties.leftNum + item.properties.numWidth - 1 >= x
          )
        );
      });
      return blocks;
    }

    $scope.configTypes = function(type){

      if($scope.block.new.steps === undefined){
        $scope.block.new.steps = ['types'];
      }

      $scope.block.new.type = type;
      if(type === 'park'){
        $scope.block.new.startTimeView = new Date($scope.block.new.startTimeView.getFullYear(), $scope.block.new.startTimeView.getMonth(), $scope.block.new.startTimeView.getDate(), 8, 0, 0, 0);
        $scope.block.new.endTimeView = new Date($scope.block.new.startTimeView.getFullYear(), $scope.block.new.startTimeView.getMonth(), $scope.block.new.startTimeView.getDate(), 22, 0, 0, 0);
        if($scope.block.new.startTimeInMinutes < 60){
          $scope.block.new.startTimeInMinutes = 60;
          $scope.block.new.startTime = new Date($scope.block.new.startTimeView.getFullYear(), $scope.block.new.startTimeView.getMonth(), $scope.block.new.startTimeView.getDate(), 8, 0, 0, 0);
        }

        if(totalMinutesAvailablePerDay - 2 * 60 < $scope.block.new.endTimeInMinutes || $scope.block.new.endTimeInMinutes === 0){
          $scope.block.new.endTime = new Date($scope.block.new.startTimeView.getFullYear(), $scope.block.new.startTimeView.getMonth(), $scope.block.new.startTimeView.getDate(), 22, 0, 0, 0);
          $scope.block.new.endTimeInMinutes = totalMinutesAvailablePerDay - 2 * 60;
        }

        $scope.block.new.diffTimeInMinutes = $scope.block.new.endTimeInMinutes - $scope.block.new.startTimeInMinutes;

        $scope.block.new.steps.push('parks');
      } else {
        $scope.block.new.startTimeView = new Date($scope.block.new.startTimeView.getFullYear(), $scope.block.new.startTimeView.getMonth(), $scope.block.new.startTimeView.getDate(), 7, 0, 0, 0);
        $scope.block.new.endTimeView = new Date($scope.block.new.startTimeView.getFullYear(), $scope.block.new.startTimeView.getMonth(), $scope.block.new.startTimeView.getDate(), 23, 59, 59, 0);

        $scope.block.new.steps.push('config');
      }
    };

    $scope.configParks = function(park){
      $scope.block.new.park = park;
      $scope.block.new.steps.push('config');
    };

    $scope.schedulerConfig = {
      viewActive: 'week',
      saves: [],
      saveComplete: function(item){
        var index = this.saves.indexOf(item);
        if(index !== -1){
          this.saves.splice(index, 1);
          $scope.config.notification.success();
        }
      },
      notes: [],
      updateNotes: function(notes) {
        for(var i=0; i<notes.length; i++) {
          var note = $filter('filter')($scope.schedulerConfig.notes, {id:notes[i].id}, true);
          if(note.length) {
            var index = $scope.schedulerConfig.notes.indexOf(note[0]);
            $scope.schedulerConfig.notes.splice(index, 1, notes[i]);
          }
          else {
            $scope.schedulerConfig.notes.push(notes[i]);
          }
        }

        $scope.schedulerConfig.notes = $filter('orderBy')($scope.schedulerConfig.notes, 'updated_at', true);
      },
      showReadMousages: false,
      dateStringFilter: function(day) {
        return new Date(day.date);
      }
    };

    $scope.changeViewScheduler = function(view){
      if(view === 'day'){
        $scope.schedulerConfig.viewActive = 'day';
      } else if(view === 'week'){

        $scope.schedulerConfig.viewActive = 'week';
      }
    };

    $scope.$watch('vacation.options.resorts', function() {
      if($scope.vacation.options && $scope.vacation.options.resorts){
        loadResortsNames();
      }
    });

    function loadResortsNames(){
      for(var i = 0; i < $scope.vacation.options.resorts.length; i++){
        var resorts = angular.copy($scope.vacation.options.resorts[i]);
        if($scope.resorts.indexOf(resorts.name) === -1){
          $scope.resorts.push(resorts.name);
        }
      }
    }

    $scope.updateResorts = function(nameResort){
      var resorts = $filter('filter')($scope.vacation.options.resorts, { name: nameResort });
      var newResorts = [];
      var numMaxItems = 8;
      for(var i = 0; i < resorts.length && i < numMaxItems; i++){
        var resort = resorts[i];
        if(newResorts.indexOf(resort.name) === -1){
          newResorts.push(resort.name);
        }
      }
      $scope.resorts = newResorts;
    };

    $scope.updateRestaurants = function(nameRestaurant, block){
      $scope.restaurantsNames = [];
      if(!block.parent_id){ //if is parent block
        return [];
      }
      var type = $filter('blockType')(block.parent_id, $scope.block_types);
      if(type && type.name === 'park'){
        mousePlannerService.restaurants.park(type.id)
        .then(function(response){
          if(response){
            $scope.restaurants = response;
            for(var i = 0; i < response.length; i++){
              $scope.restaurantsNames.push(response[i].name);
            }
          }
        });
      }
      else if(type && type.name === 'resort'){
        mousePlannerService.restaurants.resort(type.id)
        .then(function(response){
          if(response){
            $scope.restaurants = response;
            for(var i = 0; i < response.length; i++){
              $scope.restaurantsNames.push(response[i].name);
            }
          }
        });
      }
    };

    function findOrlandoResortByName(name){

      if ($scope.vacation.options.resorts.length > 1){
        for(var i = 0; i < $scope.vacation.options.resorts.length; i++){
          var resort = $scope.vacation.options.resorts[i];
          if(resort.name === name){
            return resort;
          }
        }
      }

      return undefined;
    }

    window.addEventListener('scroll', function() {
      var sticky = document.querySelector('.js-sticky');
      var stickyWrap = document.querySelector('.js-sticky-wrap');
      if(stickyWrap){
        var stickyPosition = stickyWrap.getBoundingClientRect().top;
        if (stickyPosition <= 0) {
          sticky.style.position = 'fixed';
          sticky.style.top = '0px';
        } else {
          sticky.style.position = 'static';
          sticky.style.top = '';
          sticky.style.left = '';
          sticky.style.borderLeft = '';
          sticky.style.borderRight = '';
        }
      }
    });
    $('.js-sticky-week').click(function(){
        var sticky = document.querySelector('.js-sticky');
        sticky.style.position = 'static';
        sticky.style.top = '';
        sticky.style.left = '';
        sticky.style.borderLeft = '';
        sticky.style.borderRight = '';
    });

    $scope.formatBlockName = function(block){
      var maxCharacters = 35;
      var textname = '' + block.name;
      if(block.location){
        textname = textname + ', ' + block.location;
      }

      if(textname.length > maxCharacters){
        textname = textname.substr(0, maxCharacters) + '...';
      }

      return textname;
    };

      // Subscription popup
      $scope.userCanSubscribe = $scope.config.user.is_on_trial;
      $scope.showSubscriptionPopup = false;
      $scope.toggleSubscriptionPopup = function() {
          $scope.showSubscriptionPopup = true;
      };

  });
/* jshint ignore:end */
