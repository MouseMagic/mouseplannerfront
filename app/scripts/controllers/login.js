'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
.controller('LoginCtrl', ['mousePlannerService', 'Auth', '$state', '$scope', '$transitions', 'socialNetworks', '$window', 'userService',
  function(mousePlannerService, Auth, $state, $scope, $transitions, socialNetworks, $window, userService){
    // Check if user login
    if (Auth.UserIsAuthorized()) {
      $scope.userIsAuthorized = true;
      return $state.go('error', {id: 404});
    } else {
      $scope.userIsAuthorized = false;
    }

    // Add class to body
    $scope.app.fullHeightPage = true;

    // LOGIN functionality
    $scope.googleFlag = 0;
    $scope.facebookRegError = false;
    $scope.disableButton = false;
    $scope.registrationError = false;
    $scope.login = {
      googleButtonClick: function(){
        $scope.disableButton = true;
        $scope.googleFlag++;
      },
      googleSignInCallback: function(googleUser) {
        if ($scope.googleFlag > 0) {
          $scope.disableButton = true;
          var id_token = googleUser.getAuthResponse().id_token;
          if (id_token) {
            socialNetworks.googleLogin(id_token).then(
              function(res){
                $scope.googleFlag = 0;
                var auth2 = $window.gapi.auth2.getAuthInstance();
                auth2.signOut();
                userService.redirectAfterLogin();
              },
              function(err){
                $scope.facebookRegError = err.data ? err.data : {};
                $scope.googleFlag = 0;
                $scope.disableButton = false;
              }
            );
          }
        }
      },
      googleOnFailureCallback: function(response) {
        if (response.error && response.error === 'popup_closed_by_user') {
          $scope.$apply(function () {
            $scope.disableButton = false;
          });
        }
      },
      facebookButtonClick: function(){
        if (!$scope.disableButton) {
          $scope.disableButton = true;
          socialNetworks.facebookLogin().then(
            function(res){
              userService.redirectAfterLogin();
            },
            function(err){
              if (!err.popupClose) {
                $scope.$apply(function () {
                  $scope.facebookRegError = err.data ? err.data : {};
                });
              }
              $scope.$apply(function () {
                $scope.disableButton = false;
              });
            }
          );
        }
      },
      submit: function(form){
        if($scope.login.error){
          delete $scope.login.error;
        }
        if(!form.$valid){
          return;
        }
        $scope.disableButton = true;
        mousePlannerService.user.login($scope.login.data)
          .then(
            function(res){
              userService.redirectAfterLogin();
            },
            function(err){
              $scope.login.error = err;
            }
          );
        $scope.disableButton = false;
      }
    };

    // Render google button and set callback onGoogleSignIn
    socialNetworks.init($scope.login.googleSignInCallback, $scope.login.googleOnFailureCallback);

    // Redirect to login page
    $scope.goToRegistration = function(){
      $state.go('registration');
    };
}]);
