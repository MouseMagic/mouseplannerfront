'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:RegistrationCtrl
 * @description
 * # RegistrationCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
.controller('RegistrationCtrl', ['mousePlannerService', 'Auth', '$state', '$scope', '$transitions', 'socialNetworks', 'userService',
  function(mousePlannerService, Auth, $state, $scope, $transitions, socialNetworks, userService){
    // Check if user login
    if (Auth.UserIsAuthorized()) {
      $scope.userIsAuthorized = true;
      return $state.go('error', {id: 404});
    } else {
      $scope.userIsAuthorized = false;
    }

    // Add class to body
    $scope.app.fullHeightPage = true;

    // Registration actions
    $scope.googleFlag = 0;
    $scope.registrationSuccess = false;
    $scope.regServerUnavailable = false;
    $scope.disableButton = false;
    $scope.facebookRegError = false;
    $scope.registration = {
      onEmailChange: function(form){
        if (form.email.$error.emailExist) {
          delete form.email.$error.emailExist;
          form.email.$valid = true;
        }
      },
      emailErrorClass: function(registrationForm) {
        return registrationForm.email.$error && registrationForm.$submitted && !registrationForm.email.$valid;
      },

      // Registration by email and password
      submit: function(form){
        if(!form.$valid){
          return;
        }
        $scope.regServerUnavailable = false;
        $scope.disableButton = true;
        mousePlannerService.user.registration($scope.registration.data)
          .then(
            function(res){
              $scope.registrationSuccess = true;
            },
            function(err){
              if (err && err.email) {
                form.email.$error.emailExist = true;
                form.email.$valid = false;
              }
              else if (err && err.password) {
                form.password.$error.pattern = true;
              }
              else {
                $scope.regServerUnavailable = true;
              }
            }
          );
        $scope.disableButton = false;
      },
      googleButtonClick: function(){
        $scope.disableButton = true;
        $scope.googleFlag++;
      },
      googleSignInCallback: function(googleUser) {
        if ($scope.googleFlag > 0) {
          $scope.disableButton = true;
          var id_token = googleUser.getAuthResponse().id_token;
          if (id_token) {
            socialNetworks.googleLogin(id_token).then(
              function(res){
                $scope.googleFlag = 0;
                userService.redirectAfterLogin();
              },
              function(err){
                $scope.facebookRegError = err.data ? err.data : {};
                $scope.googleFlag = 0;
                $scope.disableButton = false;
              }
            );
          }
        }
      },
      googleOnFailureCallback: function(response) {
        if (response.error && response.error === 'popup_closed_by_user') {
          $scope.$apply(function () {
            $scope.disableButton = false;
          });
        }
      },

      // FACEBOOK registration
      facebookButtonClick: function(){
        if (!$scope.disableButton) {
          $scope.disableButton = true;
          socialNetworks.facebookLogin().then(
            function(res){
              userService.redirectAfterLogin();
            },
            function(err){
              if (!err.popupClose) {
                $scope.$apply(function () {
                  $scope.facebookRegError = err.data ? err.data : {};
                });
              }
              $scope.$apply(function () {
                $scope.disableButton = false;
              });
            }
          );
        }
      }
    };

    // Render google button and set callback onGoogleSignIn
    socialNetworks.init($scope.registration.googleSignInCallback, $scope.registration.googleOnFailureCallback);

    // Redirect to login page
    $scope.goToLogin = function(){
      $state.go('login');
    };
}]);
