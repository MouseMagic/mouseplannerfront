'use strict';

/**
 * @ngdoc function
 * @name mpservicesApp.controller:RegistrationCtrl
 * @description
 * # RegistrationCtrl
 * Controller of the mpservicesApp
 */
angular.module('mpservicesApp')
.controller('SubscriptionCtrl', ['$scope', 'data',
  function($scope, data){
    // Set plans array
    $scope.vacation = [];
    if (data.vacation && data.vacation.length) {
      $scope.vacation = data.vacation;
    }

    // Set user to scope
    $scope.user = data.user;
}]);
