'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:mpDay
 * @description
 * # mpDay
 */
angular.module('mpservicesApp')
.directive('mpDay', function($filter, mousePlannerService){
	return {
		restrict: 'A',
		scope:true,
		link: function(scope) {

			if(!scope.day || !scope.day.id){

				if(scope.vacation.data.mouse_plan.days[scope.header.currentDay] && scope.vacation.data.mouse_plan.days[scope.header.currentDay].id) {
					scope.day = scope.vacation.data.mouse_plan.days[scope.header.currentDay];

					scope.$watch('header.currentDay', function() {
						scope.day = scope.vacation.data.mouse_plan.days[scope.header.currentDay];
					}, true);
				}
				else{
					return;
				}
			}


			mousePlannerService.note.get(scope.day.id, 'days')
			.then(
				scope.schedulerConfig.updateNotes
			);

			var getNotes = function(id) {
					mousePlannerService.fastpass.get(id)
				.then(
					function(res){
						scope.day.selectedFastpass = res;
					}
				);
			};

			scope.$watch('schedulerConfig.viewActive', function() {
				if(scope.schedulerConfig.viewActive === 'week') {
					getNotes(scope.day.id);
				}
			});

			scope.$watch('day.blocks', function(){
				scope.$broadcast('day-blocks-updated');
			}, true);
		}
	};
});
