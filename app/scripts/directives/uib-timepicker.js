'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:uibTimepicker
 * @description
 * # uibTimepicker
 */
angular.module('mpservicesApp')
  .directive('uibTimepicker', function($compile) {
    return {
			restrict: 'C',
			link: function(scope, elem, attrs){
				var row1 = angular.element(elem.find('tr')[0]);
				var cell1 = angular.element(row1.find('td')[5]);
				cell1.html($compile('<a ng-click="toggleMeridian()" ng-class="{disabled: noToggleMeridian()}" ng-disabled="noToggleMeridian()" class="btn btn-link" tabindex="0"><span class="glyphicon glyphicon-chevron-up"></span></a>')(scope));
				var row3 = angular.element(elem.find('tr')[2]);
				var cell3 = angular.element(row3.find('td')[5]);
				cell3.html($compile('<a ng-click="toggleMeridian()" ng-class="{disabled: noToggleMeridian()}" ng-disabled="noToggleMeridian()" class="btn btn-link" tabindex="0"><span class="glyphicon glyphicon-chevron-down"></span></a>')(scope));
			}
		};
  });
