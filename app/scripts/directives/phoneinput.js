'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:phoneInput
 * @description
 * # phoneInput
 */
angular.module('mpservicesApp')
  .directive('phoneInput', function ($filter) {
    return {
			restrict: 'A',
			require: 'ngModel',
			scope: {
				model: '=ngModel',
			},
			link: function(scope) {
				scope.$watch('model', function(value) {			
					value = String(value);
					var number = value.replace(/[^0-9]+/g, '');
					scope.model = number;
					scope.model = $filter('phonenumber')(number);
				});
			}
		};
  });
