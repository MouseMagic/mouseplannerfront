'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:mpParkBlock
 * @description
 * # mpParkBlock
 */
angular.module('mpservicesApp')
.directive('mpParkBlock', ['$filter', 'mousePlannerService', function($filter, mousePlannerService){
	return {
		restrict: 'A',
		scope:true,
		link: function(scope) {
			if(!scope.block || !scope.block.id){
				return;
			}
			scope.$watchCollection('vacation.options.parks', function(parks){
				if(!parks || !parks.length){
					return;
				}
				parks = $filter('filter')(parks, {id: scope.block.id}, true);
				if(parks.length){
					scope.park = parks[0];
					mousePlannerService.vacation.park(scope.vacation.data.id, parks[0].id)
					.then(
						function(res){
							scope.details = res.details;
						},
						function(err){
							console.log(JSON.stringify(err));
						}
					);
				}
			});
		}
	};
}]);
