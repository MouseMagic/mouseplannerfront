'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:mpClickAway
 * @description
 * # mpClickAway
 */
angular.module('mpservicesApp')
.directive('mpClickAway', function($document) {
	return {
  		link: function postLink(scope, element, attr){

	    	var elsewhere = true;

		    element.on('click', function(e) {
		      	elsewhere = false;
		      	$document.off('click', clickElsewhere);
		      	$document.on('click', clickElsewhere);
		    });

		    var clickElsewhere = function() {
		      	if (elsewhere) {
		        	scope.$apply(attr.mpClickAway);
		        	$document.off('click', clickElsewhere);
		      	}
		      	elsewhere = true;
		    };

  		}
	};
});
