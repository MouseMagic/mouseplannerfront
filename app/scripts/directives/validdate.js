'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:validdate
 * @description
 * # validdate
 */
angular.module('mpservicesApp')
  .directive('validDate', function () {
    return {
			restrict: 'A',
			require: 'ngModel',
      link: function (scope, element, attr, ngModel) {
        ngModel.$parsers.push(function(value) {
        	if(value === 'Invalid date'){
						return '';
					}else{
						return value;
					}
				});
				ngModel.$formatters.push(function(value) {
					if(value === 'Invalid date'){
						return '';
					}else{
						return value;
					}
				});
      }
    };
  });
