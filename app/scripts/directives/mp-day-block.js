'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:mpDayBlock
 * @description
 * # mpDayBlock
 */
angular.module('mpservicesApp')
.directive('mpDayBlock', function($filter, configPage, mousePlannerService){
	return {
		restrict: 'A',
		scope: true,
		link: function(scope){
			if(!scope.block){
				return;
			}

			var update = function(){
				scope.blockProperties = $filter('blockProperties')(scope.block, scope.block_types, scope.day, scope.vacation);

				// don't re-write this variables in this way var [endDayMaxHours, endDayMaxMinutes] = configPage.endDayMax.split(":");
        // it's crash build on the server side
        var endDayMax = configPage.endDayMax.split(":"),
          endDayMaxHours= endDayMax[0],
          endDayMaxMinutes = endDayMax[1];

 				if(scope.block.id > 0){
          scope.$watch('scope.block.endTime', function(event, block) {
            // set max end time according configPage.endDayMax
            var maxEndTime = $filter('parseTime')(moment(scope.block.startTime).set({'hours' : endDayMaxHours, 'minutes' : endDayMaxMinutes}), moment(scope.block.startTime).add(1,'days')),
              //this variable needs for correct end point block is being changed(resize,drag and drop, timepicker change)
              maxEndPoint = moment(moment(maxEndTime).utc().valueOf() + moment().utcOffset()*60000).toDate();

            //cut blocks when endTime more than available
            if ( scope.block.endTime > maxEndPoint ) {
              scope.block.endTime = maxEndPoint;
            }
          });

				// 	mousePlannerService.note.get(scope.block.id, 'blocks')
				// 	.then(scope.schedulerConfig.updateNotes);
				}
			};
			update();
			scope.$on('day-blocks-updated', function(){
				update();
			});
		}
	};
});
