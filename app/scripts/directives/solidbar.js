'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:solidbar
 * @description
 * # solidbar
 */
angular.module('mpservicesApp')
    .directive('solidbar', function (configPage, $document, $filter, $state, ngDialog, saveStepService, mousePlannerService) {
        return {
            restrict: 'E',
            scope: true,
            link: function(scope) {
                var settings = {
                    vacation: scope.vacation,
                    main: scope.main,
                    config: scope.config
                };
                scope.currentStep = saveStepService.currentStep(settings);
                scope.steps = configPage.navigation;
                var save = function(number) {
                    scope.isSchedule = false;
                    if ($state.current.name === "main.trip.scheduler") {
                        scope.isSchedule = true;
                    }

                    var redirect = function () {
                        $state.go(scope.steps[number].state);
                    };

                    var moveNext = function () {
                        if( scope.vacation.data.highestStepCompleted >= number || (scope.vacation.data.highestStepCompleted === 5 && number === 6) ){
                            var def;
                            if(scope.vacation.data.highestStepCompleted === 5 && number === 6){
                                var newMousePlan = {
                                    vacation_id: scope.vacation.data.id,
                                    dashboard_img: 'img'
                                };
                                mousePlannerService.mouseplan.create(newMousePlan)
                                    .then(
                                        function(res){
                                            scope.vacation.data.mouse_plan = res;
                                            redirect();
                                            $document[0].body.scrollTop = $document[0].documentElement.scrollTop = 0;
                                        },
                                        function(err){
                                            scope.config.notification.error();
                                        }
                                    );
                            }
                            else {
                                if(scope.vacation.data.id){
                                    def = saveStepService.updateVacation(scope.vacation.data, settings);
                                }
                                else {
                                    def = saveStepService.saveVacation(scope.vacation.trip, scope.vacation.data, settings);
                                }
                                def.then(
                                    function(){
                                        redirect();
                                    },
                                    function(err){
                                        scope.config.notification.error();
                                    }
                                );
                            }

                        }
                    };

                    if(number >= 6 && scope.vacation.data.mouse_plan && scope.currentStep !== 6){
                        // call new generate mouse_plan data
                        var saveData = angular.copy(scope.vacation.data.mouse_plan);
                        var def;
                        if(scope.vacation.data.id){
                            def = saveStepService.updateVacation(scope.vacation.data, settings);
                        }
                        else {
                            def = saveStepService.saveVacation(scope.vacation.trip, scope.vacation.data, settings);
                        }
                        def.then(
                            function(){
                                var newMousePlan = {
                                    vacation_id: scope.vacation.data.id,
                                    dashboard_img: 'img'
                                };
                                mousePlannerService.mouseplan.create(newMousePlan).then(function (res) {
                                    var plan = scope.vacation.data;
                                    var newMousePlan = res;
                                    var oldMousePlan = scope.vacation.data.mouse_plan;

                                    var _isWizardChanged = $filter('mousePlanChanges')(oldMousePlan, newMousePlan);

                                    if (_isWizardChanged) {
                                        var completedEdit = false;
                                        scope.form = {
                                            description: "This change may modify your current MousePlan, including any notes or customizations you've added. These changes cannot be undone. Would you like to save and proceed? Or would you like to create a new version?",
                                            modal: ngDialog.open({
                                                template: 'views/templates/confirm-modal-move-to-step.html',
                                                scope: scope
                                            }),
                                            submit: function (form) {
                                                if (!form.$valid) {
                                                    completedEdit = false;
                                                    return;
                                                }
                                                mousePlannerService.vacation.duplicate(plan).then(function (res) {
                                                    var newCopyPlan = res;
                                                    newCopyPlan.mouse_plan = newMousePlan;
                                                    scope.main.plans.data.push(newCopyPlan);
                                                    mousePlannerService.mouseplan.update(saveData);
                                                    $state.go('main.trip.scheduler', {id: newCopyPlan.id});

                                                });
                                                this.modal.close();
                                            },
                                            cancel: function () {
                                                this.modal.close();
                                                scope.vacation.data.mouse_plan = newMousePlan;
                                                $state.go('main.trip.scheduler', {id: scope.vacation.data.id});
                                            },
                                            move: function () {
                                                this.modal.close();
                                                mousePlannerService.mouseplan.update(saveData);
                                                $state.go('main.trip.scheduler', {id: scope.vacation.data.id});
                                            }
                                        };
                                    }
                                    else {
                                        moveNext();
                                    }
                                });

                            },
                            function(err){
                                scope.config.notification.error();
                            }
                        );

                    }
                    else {
                        moveNext();
                    }
                };

                scope.stepNav = function (form, number) {
                    if (scope.currentStep > 5) {
                        save(number);
                    }
                    else {
                        if (form && form.$valid) {
                            save(number);
                        }
                        else {
                            scope.config.notification.error();
                        }
                    }
                };
            },
            templateUrl: 'views/templates/solidbar.html'
        };
    });
