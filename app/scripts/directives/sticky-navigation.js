'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:backAndNext
 * @description
 * # stickynavigation
 */
angular.module('mpservicesApp')
  .directive('stickynavigation', function (mousePlannerService, ngDialog) {
    return {
      restrict: 'A',
      scope: true,
      templateUrl: 'views/templates/sticky-navigation.html',
      link: function(scope, elem, attrs) {

        scope.$watch(
          function(){
            return scope.$eval(attrs.stickynavigation);
          },
          function(val){
            scope.active = val;
          }
        );

        scope.checklists = [
          {
            name: 'Getting Started',
            filter: function(item){
              return item.milestone === 0;
            }
          },
          {
            name: '180 days before',
            filter: function(item){
              return item.milestone === 180;
            }
          },
          {
            name: '60 days before',
            filter: function(item){
              return item.milestone === 60;
            }
          }
        ];

        scope.showChecklist = function(){
          ngDialog.open({
            template: 'views/templates/checklist-modal.html',
            className: 'ngdialog-theme-default modal--checklist',
            scope: scope,
            showClose: false
          });
        };

      }
    };
  });
