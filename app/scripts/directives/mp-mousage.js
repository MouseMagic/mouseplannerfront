'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:mpPlan
 * @description
 * # mpPlan
 */
angular.module('mpservicesApp')
.directive('mpPlan', function($filter, mousePlannerService){
	return {
		restrict: 'A',
		scope:true,
		link: function(scope) {
			if(!scope.plan || !scope.plan.id){
				return;
			}
			mousePlannerService.vacation.mousages(scope.plan.id)
			.then(
				function(response){
		          	scope.mousages = $filter('orderBy')(response, "'updated_at'", true);
		      	}
		    );
		}
	};
});
