'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:mpFastPass
 * @description
 * # mpFastPass
 */
angular.module('mpservicesApp')
.directive('mpFastPass', function($filter){
	return {
		restrict: 'A',
		scope:true,
		link: function(scope, elem, attrs){
			var getFastPass = function(){
				if(scope.vacation.options && scope.vacation.options.experiences){
					var match = $filter('filter')(scope.vacation.options.experiences, {id: scope.input.external_id}, true);
					if(match.length){
						scope.fastpassItem = match[0];
					}
				}
			};
			scope.$watch(
				function(){
					return scope.$eval(attrs.mpFastPass);
				},
				function(val){
					if(val){
						scope.input = val;
						getFastPass();
					}
				}
			);
			scope.$watch('vacation.options.experiences', function(val){
				if(val && val.length && scope.input){
					getFastPass();
				}
			});
		}
	};
});
