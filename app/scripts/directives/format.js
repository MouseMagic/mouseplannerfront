'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:format
 * @description
 * # format
 */
angular.module('mpservicesApp')
  .directive('mpFormat', function ($filter) {
    return {
      require: '?ngModel',
      link: function (scope, elem, attrs, ctrl) {
        if (!ctrl) {
          return;
        }

        ctrl.$formatters.unshift(function () {
          if(ctrl.$modelValue === undefined || ctrl.$modelValue === ''){
            return '';
          }

          var formatters = attrs.mpFormat.split(':');
          if(formatters.length > 1){
            if(formatters[0] === 'date'){
               var currentDate = Date.parse(ctrl.$modelValue);
               if(isNaN(currentDate)){
                 return '';
               }

               ctrl.$modelValue = currentDate;

            }
            return $filter(formatters[0])(ctrl.$modelValue, formatters[1]);
          }

          return $filter(attrs.format)(ctrl.$modelValue);
        });

        ctrl.$parsers.unshift(function (viewValue) {
          return viewValue;
        });
    }
    };
  });
