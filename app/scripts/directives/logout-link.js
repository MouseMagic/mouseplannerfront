'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:format
 * @description
 * # format
 */
angular.module('mpservicesApp')
  .directive('logoutLink', function (mousePlannerService, $state) {
    return {
      restrict: 'A',
      link: function (scope, elem, attrs, ctrl) {
        elem.click(function() {
          mousePlannerService.user.logout()
            .then(
              function(res){
                if (res.success) {
                  $state.go('login', {}, { reload: true });
                }
              },
              function(err){
                $state.go('login', {}, { reload: true });
              }
            );
        });
      }
    };
  });
