'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:compareTo
 * @description
 * # compareTo
 */
angular.module('mpservicesApp')
    .directive('dateFormat', function () {
        return {
            restrict : 'A',
            scope : {
                ngModel : '='
            },
            link: function (scope) {
                if (scope.ngModel) {
                    scope.ngModel = new Date(scope.ngModel);
                }
            }
        };
    });
