'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:app-global
 * @description
 * # app-global
 */
angular.module('mpservicesApp')
.directive('appGlobal', function(){
  return {
    restrict: 'A',
    scope: {
      appGlobal: '='
    },
    link: function (scope) {
      scope.appGlobal = {};
    }
  };
});
