'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:mpTimepicker
 * @description
 * # mpTimepicker
 */
angular.module('mpservicesApp')
.directive('mpTimepicker', function(configPage, $timeout) {
	return {
		templateUrl: 'views/templates/timepicker.html',
		scope: {
			time: '=mpTimepicker',
			name: '@name',
			placeholder: '@placeholder',
			min: '=?',
			max: '=?'
		},
  		link: function(scope, element, attr){
  			scope.active = false;
  			scope.minuteStep = configPage.minuteStep;

        // rewrite default value by utc (timepicker used local time by default)
        scope.time = moment(scope.time).utc().format('YYYY-MM-DDTHH:mm');
        // scope.time = moment(scope.time).format();
  			scope.changed = function(){
                // rewrite after each changes by utc format
                scope.time = moment(scope.time).format('YYYY-MM-DDTHH:mm');
  			};
  		}
	};
})
.directive('mpTimepickerCustom', function(configPage, $timeout) {
	return {
		templateUrl: 'views/templates/timepicker-custom.html',
		scope: {
			time: '=mpTimepickerCustom',
			name: '@name',
			placeholder: '@placeholder',
			min: '=?',
			max: '=?'
		},
  		link: function(scope, element, attr){
  			scope.active = false;
  			scope.minuteStep = configPage.minuteStep;
  		}
	};
});
