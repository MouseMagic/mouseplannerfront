'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:backAndNext
 * @description
 * # backAndNext
 */
angular.module('mpservicesApp')
  .directive('backandnext', function ($q, $state, $filter, $document, configPage, mousePlannerService, ngDialog, saveStepService){
    return {
      restrict: 'E',
      scope: true,
      templateUrl: 'views/templates/backandnext.html',
      link: function(scope) {
          var settings = {
              currentStep: scope.currentStep,
              vacation: scope.vacation,
              main: scope.main,
              config: scope.config
          };
        var steps = configPage.navigation;
        var currentStep = $filter('filter')(steps, {state: $state.current.name}, true);
        scope.currentStep = currentStep.length ? (currentStep[0].number - 1) : ((settings.vacation.data && settings.vacation.data.stepNumber >= 0) ? settings.vacation.data.stepNumber : 0);
        scope.showPopup = true;

        var redirect = function(){
          if(scope.currentStep + 1 >= 6){
            var newMousePlan = {
              vacation_id: scope.vacation.data.id,
              dashboard_img: 'img'
            };
            mousePlannerService.mouseplan.create(newMousePlan)
            .then(
              function(res){
                scope.vacation.data.mouse_plan = res;
                $state.go('main.trip.scheduler', {id: scope.vacation.data.id});
                $document[0].body.scrollTop = $document[0].documentElement.scrollTop = 0;
              },
              function(err){
                scope.config.notification.error();
              }
            );
          }
          else{
            $state.go(steps[scope.currentStep + 1].state, {id: scope.vacation.data.id});
          }
        };

        var exit = function(){
          $state.go('main.dashboard', {tripId: scope.vacation.trip.id});
        };

        var moveNextStep = function (exitApp) {
              var def;
              if(scope.vacation.data.id){
                  def = saveStepService.updateVacation(scope.vacation.data, settings);
              }
              else {
                  def = saveStepService.saveVacation(scope.vacation.trip, scope.vacation.data, settings);
              }
              def.then(
                  function(){
                      if(exitApp){
                          exit();
                      }
                      else{
                          redirect();
                      }
                  },
                  function(err){
                      scope.config.notification.error();
                  }
              );
        };

        scope.save = function(form, exitApp){
              if(form && form.$valid){
                  if(!exitApp){
                      scope.vacation.data.stepNumber = scope.currentStep + 1;
                  }

                  if(scope.currentStep + 1 >= 6 && scope.vacation.data.mouse_plan){
                      // call new generate mouse_plan data
                      var saveData = angular.copy(scope.vacation.data.mouse_plan);
                      var newMousePlan = {
                          vacation_id: settings.vacation.data.id,
                          dashboard_img: 'img'
                      };
                      mousePlannerService.mouseplan.create(newMousePlan).then(function (res) {
                          var plan = scope.vacation.data;
                          var newMousePlan = res;
                          var oldMousePlan = scope.vacation.data.mouse_plan;
                          var _isWizardChanged = $filter('mousePlanChanges')(oldMousePlan, newMousePlan);

                          if (_isWizardChanged && scope.showPopup) {
                              var completedEdit = false;
                              scope.form = {
                                  description: "This change may modify your current MousePlan, including any notes or customizations you've added. These changes cannot be undone. Would you like to save and proceed? Or would you like to create a new version?",
                                  modal: ngDialog.open({
                                      template: 'views/templates/confirm-modal-move-to-step.html',
                                      scope: scope
                                  }),
                                  submit: function (form) {
                                      if (!form.$valid) {
                                          completedEdit = false;
                                          return;
                                      }
                                      mousePlannerService.vacation.duplicate(plan).then(function (res) {
                                          var newCopyPlan = res;
                                          scope.main.plans.data.push(newCopyPlan);
                                          mousePlannerService.mouseplan.update(saveData);
                                          $state.go('main.trip.scheduler', {id: newCopyPlan.id});
                                      });
                                      this.modal.close();
                                  },
                                  cancel: function () {
                                      this.modal.close();
                                      scope.vacation.data.mouse_plan = newMousePlan;
                                      $state.go('main.trip.scheduler', {id: scope.vacation.data.id});
                                  },
                                  move: function () {
                                      this.modal.close();
                                      mousePlannerService.mouseplan.update(saveData);
                                      $state.go('main.trip.scheduler', {id: scope.vacation.data.id});
                                  }
                              };

                          }
                          else {
                              moveNextStep(exitApp);
                          }
                      });
                  }
                  else {
                      moveNextStep(exitApp);
                  }

              }
              else {
                  if(exitApp && scope.vacation.data.id) {
                      saveStepService.updateVacation(scope.vacation.data, settings).then(function(){
                              exit();
                          },
                          function(err){
                              scope.config.notification.error();
                          });
                  }
                  else {
                      scope.config.notification.error();
                  }
              }
        };

        scope.prev = function(){
      		$state.go(steps[scope.currentStep - 1].state);
      	};

        var updateVacation = function(vacation) {
            saveStepService.updateVacation(vacation, settings);
      	};

          scope.onNextClick = function (mpForm, exitApp) {
              scope.save(mpForm, exitApp);
          };

      }
    };
  });
