'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:mpAddActivity
 * @description
 * # mpAddActivity
 */
angular.module('mpservicesApp')
.directive('mpAddActivity', function($filter){
	return {
		restrict: 'A',
		scope:true,
		template: '<div ng-if="blockPlaceholder.startTime" class="add-activity-link" ng-style="{\'padding-top\': blockPlaceholder.menuHeight+\'px\'}" uib-popover-template="\'views/templates/new-block.html\'" popover-is-open="blockPlaceholder.formOpen" popover-placement="right-top" popover-trigger="click outsideClick"><a ng-show="blockPlaceholder.menuHeight > 10"ng-click="addBlockMobile(blockPlaceholder);"><div><i style="vertical-align: middle;" class="addblockmobile"></i></div><div>Add Block</div></a></div>',
		link: function(scope, elem) {
			var minHeight = 10;
			var padding = 2;
			var getStart = function(){
				if(scope.block && scope.block.endTime){
					if(scope.block.parent_id){
						return null;
					}
					var dayEnd = $filter('parseTime')(scope.vacation.data.endDayTime, scope.day.date);
					if(new Date(scope.block.endTime) > dayEnd){
						return null; //date out of scope
					}
					return new Date(scope.block.endTime);
				}
				return $filter('parseTime')(scope.vacation.data.beginDayTime, scope.day.date);
			};
			var getEnd = function(start, blocks){
				if(!start){
					return null;
				}
				if(!blocks.length){
					return $filter('parseTime')(scope.vacation.data.endDayTime, scope.day.date); //there are no blocks on this day, so show the whole thing
				}
				var dt = new Date(start);
				var test = {startTime: start, endTime: new Date(dt.setMinutes(dt.getMinutes()+minHeight))};
				var blocksSharedTime = $filter('blockOverlap')(test, blocks);
				if(blocksSharedTime.length){ //this block, set to min height, would overlap another block, so dont show it
					return null;
				}
				else{
					if(scope.block && scope.block.endTime){
						blocksSharedTime = $filter('blockOverlap')(scope.block, blocks);
						for(var i = 0; i < blocksSharedTime.length; i++){
							if(blocksSharedTime[i].id !== scope.block.id){
								blocks.splice(blocks.indexOf(blocksSharedTime[i]), 1);//remove shared blocks from blocks array prior to getting index
							}
						}
						var index = blocks.indexOf(scope.block);
						if(index+1 < blocks.length){
							return new Date(blocks[index+1].startTime); //return the start time of the next block as the end time of this block
						}
						else{
							return $filter('parseTime')(scope.vacation.data.endDayTime, scope.day.date); //this is the last block, so set end time to end day time
						}
					}
					else{
						return new Date(blocks[0].startTime);
					}
				}
			};
			var getMenuHeight = function(b){
				return parseInt(Math.max((((b.bottom - b.top)/2)-30),0));
			};
			var fail = function(){
				elem.removeClass('add-activity-block');
				return; //dont show block
			};
			var calculate = function(blocks){
				scope.blockPlaceholder = {};
				if(!blocks){
					return;
				}
				blocks = $filter('filter')(blocks, {parent_id: '!'});
				blocks = $filter('orderBy')(blocks, function(block){return new Date(block.startTime);}, false);
				var b = {
					dayId: scope.day.id,
					startTime: getStart(),
				};
				b.endTime = getEnd(b.startTime, blocks);
				if(!b.endTime){
					return fail();
				}
				else{
					b.top = $filter('toMinutes')(b.startTime, scope.vacation.data.beginDayTime);
					b.bottom = $filter('toMinutes')(b.endTime, scope.vacation.data.beginDayTime);
					b.height = (b.bottom-b.top);
					if(b.height < minHeight || b.height < 0){
						return fail();
					}
					else{
						elem.addClass('add-activity-block');
						b.menuHeight = getMenuHeight(b);
						elem.css({'height': (b.height-(padding*2))+'px', top: (b.top+padding)+'px'});
						scope.blockPlaceholder = b;
					}
				}
			};
			scope.$watchCollection('day.blocks', calculate);
		}
	};
});
