'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:subscriptionPurchase
 * @description
 * # STEP 1 - subscription description
 * # STEP 2 - stripe payment form
 * # STEP 3 - payment result message
 */
angular.module('mpservicesApp')
  .directive('subscriptionPurchase', function (mousePlannerService, $state, userService, $window) {
    return {
      restrict: 'E',
      scope: {
        user: '=user',
        mode: '=mode',
        show: '=show'
      },
      templateUrl: 'views/templates/subscription-purchase.html',
      controller: function($scope) {
        // Set main variables.
        $scope.paymentStep = 1;
        if ($state.params && $state.params.step && parseInt($state.params.step) === 2) {
          $scope.paymentStep = parseInt($state.params.step);
        }
        $scope.modeType = $scope.mode === 'page' ? $scope.mode : 'popup';
        if ($scope.modeType === 'popup') {
          $scope.paymentStep = 2;
        }
        // If user wont to retry payment we open popup by default
        if ($window.sessionStorage.subscriptionPopupOpen &&  $window.sessionStorage.subscriptionPopupOpen === "true") {
          $scope.show = true;
          $window.sessionStorage.subscriptionPopupOpen = false;
        }

        /** STEP 1 - subscription description. */
        $scope.subscription = {
          were_subscribed_before: $scope.user.were_subscribed_before,
          isOnTrial: $scope.user.is_on_trial,
          isSubscribed: $scope.user.is_subscribed,
          submit: function(){
            $scope.paymentStep = 2;
          }
        };

        /** STEP 2 - stripe payment form. */
        $scope.stripePublickKey = '';
        $scope.subscriptionPlan = {};
        $scope.subscriptionCoupon = {};
        $scope.basket = {};
        $scope.paymentErrors = {};
        $scope.paymentIsSuccessful = false;
        $scope.showLoader = false;
        $scope.paymentProcessing = false;
        $scope.disableButton = false;
        $scope.spinnerCoupon = 'default'; // default, loading, apply

        // Add stripe script
        (function() {
          if (!window.Stripe) {
            var po = document.createElement('script');
            po.src = 'https://js.stripe.com/v3/';
            var size = document.getElementsByTagName('script').length;
            var s = document.getElementsByTagName('script')[size - 1];
            s.parentNode.insertBefore(po, s);
          }
        })();

        // Run stripe and stripe elements
        function runStripe() {
          $scope.stripe = window.Stripe($scope.stripePublickKey);
          $scope.elements = $scope.stripe.elements();

          var elementStyles = {
            base: {
              color: '#414042',
              fontWeight: 400,
              fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
              fontSize: '19px',
              lineHeight: '25px',
              fontSmoothing: 'antialiased',
              ':focus': {
                color: '#414042'
              },
              '::placeholder': {
                color: '#9b9b9b'
              },
              ':focus::placeholder': {
                color: '#9b9b9b'
              }
            },
            invalid: {
              color: '#414042',

              ':focus': {
                color: '#414042'
              },
              '::placeholder': {
                color: '#9b9b9b'
              }
            }
          };

          var elementClasses = {
            focus: 'focus',
            empty: 'empty',
            invalid: 'invalid'
          };

          $scope.cardNumber = $scope.elements.create('cardNumber', {
            style: elementStyles,
            classes: elementClasses
          });
          $scope.cardNumber.mount('#card-element-number');

          $scope.cardExpiry = $scope.elements.create('cardExpiry', {
            style: elementStyles,
            classes: elementClasses
          });
          $scope.cardExpiry.mount('#card-element-expiry');

          $scope.cardCvc = $scope.elements.create('cardCvc', {
            style: elementStyles,
            classes: elementClasses,
            placeholder: 'CVC Code'
          });
          $scope.cardCvc.mount('#card-element-cvc');


          // Stripe elements errors
          var stripeElements = [$scope.cardNumber, $scope.cardExpiry, $scope.cardCvc];
          stripeElements.forEach(function(element) {
            element.addEventListener('change', function(event) {
              if (event.error) {
                $scope.$apply(function () {
                  $scope.paymentErrors[element._componentName] = event.error;
                });
              } else {
                $scope.$apply(function () {
                  $scope.paymentErrors[element._componentName] = null;
                });
              }
            });
          });
        }

        // Set interval for checking stripe script
        function initStripe() {
          function checkSripeScripts() {
            if (window.Stripe) {
              runStripe();
              clearInterval(intervalId);
            }
          }
          var intervalId = setInterval(checkSripeScripts, 100);
        }

        // Calculate cart amounts
        function setBasketData() {
          if ($scope.subscriptionPlan) {
            var coupon = $scope.subscriptionCoupon;
            var planAmount = $scope.subscriptionPlan.amount;
            var totalAmount = $scope.subscriptionPlan.amount;
            var discount = null;

            // Calculate discount
            if ($scope.subscriptionCoupon.id) {
              // If fixed
              if (coupon.amount_off) {
                discount = parseFloat(coupon.amount_off);
              }
              else if (coupon.percent_off) {
                discount = (parseFloat(planAmount) / 100) * parseFloat(coupon.percent_off);
              }

              totalAmount = parseFloat(planAmount) - parseFloat(discount);
            }

            discount = discount ? (parseFloat(discount) / 100).toFixed(2) : discount;
            totalAmount = totalAmount ? (parseFloat(totalAmount) / 100).toFixed(2) : totalAmount;
            planAmount = planAmount ? (parseFloat(planAmount) / 100).toFixed(2) : planAmount;

            $scope.basket = {
              planName: $scope.subscriptionPlan.name,
              planAmount: planAmount,
              currency: '$',
              discountAmount: discount,
              totalAmount: totalAmount
            };
          }
        }

        // Get Stripe public key
        mousePlannerService.subscription.getStripePublicKey().then(
          function(res){
            $scope.stripePublickKey = res;
            initStripe();
          },
          function(err){}
        );

        // Get subscription plans
        mousePlannerService.subscription.getSubscriptionPlans().then(
          function(res){
            $scope.subscriptionPlan = res[0];
            setBasketData();
          },
          function(err){}
        );

        // Stripe payment.
        $scope.payment = {
          data: {
            name: '',
            coupon: ''
          },
          checkCouponCode: function() {
            if (!$scope.subscriptionCoupon.id) {
              var couponeCode = $scope.payment.data.coupon ? $scope.payment.data.coupon.trim() : '';
              if (couponeCode.length < 1) {
                $scope.paymentErrors.coupon = {
                  code: 'coupon_validation',
                  message: "Please enter a Coupon code."
                };
                return;
              }
              $scope.disableButton = true;
              $scope.spinnerCoupon = 'loading';
              mousePlannerService.subscription.checkCouponCode(couponeCode).then(
                function(res){
                  if (res.coupon && res.coupon.valid) {
                    $scope.subscriptionCoupon = res.coupon;
                    $scope.paymentErrors.coupon = null;
                    $scope.spinnerCoupon = 'apply';
                    setBasketData();
                  } else {
                    $scope.paymentErrors.coupon = {
                      code: 'coupon_invalid',
                      message: "Your coupon code is invalid."
                    };
                    $scope.spinnerCoupon = 'default';
                  }
                  $scope.disableButton = false;
                },
                function(err){
                  $scope.paymentErrors.coupon = {
                    code: 'coupon_invalid',
                    message: "Your coupon code is invalid."
                  };
                  $scope.disableButton = false;
                  $scope.spinnerCoupon = 'default';
                }
              );
            }
          },
          submit: function(form){
            var additionalData = {
              name: $scope.payment.data.name ? $scope.payment.data.name : undefined
            };
            $scope.paymentProcessing = true;
            $scope.showLoader = true;
            $scope.disableButton = true;
            $scope.stripe.createToken($scope.cardNumber, additionalData).then(function(result) {
              if (result.error) {
                $scope.$apply(function () {
                  $scope.disableButton = false;
                  $scope.showLoader = false;
                  $scope.paymentProcessing = false;
                });
              } else {
                var paymentData = {
                  card_token: result.token.id,
                  plan_id: $scope.subscriptionPlan.id,
                  coupon_id: $scope.subscriptionCoupon.id
                };

                // Subscribe user
                mousePlannerService.subscription.subscribe(paymentData).then(
                  function(res){
                    $scope.paymentIsSuccessful = true;
                    $scope.paymentStep = 3;
                    $scope.disableButton = false;
                    $scope.showLoader = false;
                    $scope.paymentProcessing = false;
                  },
                  function(err){
                    if (err.status === 409) {
                      $scope.paymentErrors.main = {
                        code: 'payment_conflict',
                        message: err.data && err.data.message ? err.data.message : ''
                      };
                    } else {
                      $scope.paymentErrors.main = {
                        code: 'payment_error',
                        message: 'Payment error'
                      };
                    }
                    $scope.paymentStep = 3;
                    $scope.disableButton = false;
                    $scope.showLoader = false;
                    $scope.paymentProcessing = false;
                  }
                );
              }
            });
          }
        };

        /** STEP 3 - payment result message. */
        $scope.goToAfterPayment = function() {
          if ($scope.paymentIsSuccessful) {
            if ($scope.modeType === 'popup') {
              $state.reload();
            } else {
              userService.redirectAfterLogin();
            }
          } else {
            if ($scope.modeType === 'popup') {
              $window.sessionStorage.subscriptionPopupOpen = true;
              $state.reload();
            } else {
              $state.go('subscription', {step: 2}, { reload: true });
            }
          }
        };

        // Close popup handler
        $scope.closePopup = function() {
          if ($scope.modeType === 'popup') {
            if ($scope.paymentStep === 3) {
              if ($scope.paymentIsSuccessful) {
                $state.reload();
              }
            }
            if (!$scope.paymentProcessing) {
              $scope.show = false;
            }
          } else if ($scope.modeType === 'page') {
            if ($scope.paymentStep === 3) {
              if ($scope.paymentIsSuccessful) {
                userService.redirectAfterLogin();
              } else {
                $state.go('subscription',  {step: null}, { reload: true });
              }
            }
          }
        };

        $scope.logoutUser = function() {
          mousePlannerService.user.logout()
            .then(
              function(res){
                if (res.success) {
                  $state.go('login');
                }
              },
              function(err){
                $state.go('login');
              }
            );
        };
      }
    };
  });
