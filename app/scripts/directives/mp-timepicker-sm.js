'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:mpTimepickerSm
 * @description
 * # mpTimepickerSm
 */
angular.module('mpservicesApp')
.directive('mpTimepickerSm', function(configPage, $timeout) {
	return {
		templateUrl: 'views/templates/timepicker-sm.html',
		scope: {
			time: '=mpTimepickerSm',
			name: '@name',
			placeholder: '@placeholder',
			min: '=',
			max: '='
		},
  		link: function(scope, element, attr){
  			scope.active = false;
  			scope.minuteStep = configPage.minuteStep;
  			scope.required = (attr.required ? true : false);

  		}
	};
});
