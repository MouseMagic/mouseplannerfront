'use strict';

/**
 * @ngdoc directive
 * @name mpservicesApp.directive:mpRequired
 * @description
 * # mpRequired
 */
 angular.module('mpservicesApp')
   .directive('mpRequired', function () {
     return {
       restrict: 'A',
       require: 'ngModel',
       link: function(scope, elm, attr, ctrl) {

         if (!ctrl) { return; }
         //attr.mpRequired = false; // force truthy in case we are on non input element

         var validator = function(value) {
           
           var mpRequired = attr.mpRequired === true || attr.mpRequired === 'true' || attr.mpRequired === 'mp-required' || attr.mpRequired === '';
           if (mpRequired && ctrl.$isEmpty(value) ||
               mpRequired && (value === 0 || value === '0')) {
             ctrl.$setValidity('mpRequired', false);
             return;
           } else {
             ctrl.$setValidity('mpRequired', true);
             return value;
           }
         };

           ctrl.$formatters.push(validator);
           ctrl.$parsers.unshift(validator);

           attr.$observe('mpRequired', function() {
             validator(ctrl.$viewValue);
           });
       }
     };
   });
