'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:minuteStep
 * @function
 * @description
 * # minuteStep
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('minuteStep', function(configPage){
    return function(minutes){
    	if(!minutes){
    		return;
    	}
  		return (parseInt((minutes % 60) / configPage.minuteStep)) * configPage.minuteStep;
  	};
  });
