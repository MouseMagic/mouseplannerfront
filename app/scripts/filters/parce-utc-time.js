'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:parseUtcTime
 * @function
 * @description
 * # parseUtcTime
 * Filter in the mpservicesApp.
 */

angular.module('mpservicesApp')
  .filter('parseUtcTime', [function() {
    return function(original, merge){
      if(!original){
        return;
      }
      original = original;
      merge = merge ? merge : original;

      var date = moment(merge).utc().format('YYYY-MM-DD');
      var time = moment(original).utc().format("HH:mm");

      return moment(date + " " + time);
    };
  }]);
