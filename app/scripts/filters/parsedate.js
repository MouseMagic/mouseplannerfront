'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:parsedate
 * @function
 * @description
 * # parsedate
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('parsedate', function () {
    return function (date){
	        if(!date){
            return null;
        }
        var currentDate = Date.parse(date.replace( /(\d{4})-(\d{2})-(\d{2})/, "$2/$3/$1"));
        if(isNaN(currentDate)){
        	return null;
        }
        else{
        	return moment.utc(date);
        }
	};
});
