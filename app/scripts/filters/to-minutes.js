'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:toMinutes
 * @function
 * @description
 * # toMinutes
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('toMinutes', function(configPage, $filter){
    return function(d, beginDayTime, block){
    	if(!d){
    		return;
    	}
  		var hours = new Date(d).getUTCHours() === 0 ? 0 : new Date(d).getHours();
  		var minutes = new Date(d).getMinutes();
      var beginTime = $filter('timeToMinutes')(configPage.startDayDefault);
      if(beginDayTime){
        beginTime = $filter('timeToMinutes')(beginDayTime, true);
      }
  		return ((hours * 60) + minutes - beginTime);
  	};
  });
