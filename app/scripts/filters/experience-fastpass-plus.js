'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:experienceFastpassPlus
 * @function
 * @description
 * # experienceFastpassPlus
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('experienceFastpassPlus', function ($filter) {
    return function (experiences) {
    	if(!experiences || !experiences.length){
    		return [];
    	}
    	return $filter('filter')(experiences, {fastPassPlus: true}, true);
	};
  });
