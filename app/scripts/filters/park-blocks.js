'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:blockType
 * @function
 * @description
 * # blockType
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('parkBlocks', ['$filter', function($filter) {
    return function(blocks, blockTypes){
      if(!blocks || !blockTypes){
        return;
      }

      var array = [];
      angular.forEach(blocks, function(block) {
        var b = $filter('blockType')(block.block_type_id, blockTypes);
        if(b.name === 'park' && array.indexOf(b) === -1) {
          array.push(b);
        }
      });

      return array;
  	};
  }]);
