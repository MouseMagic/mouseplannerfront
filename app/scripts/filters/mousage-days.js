'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:mousageDays
 * @function
 * @description
 * # mousageDays
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('mousageDays', function($filter) {
    return function(days, mousages){
      if(!days || !mousages || !mousages.length){
    		return [];
      }

      var dates = [];

      angular.forEach(mousages, function(mousage) {
        var mousageDay = $filter('mousageDay')(mousage.mousageable_id, days);
        if (mousageDay.id !== -1 && mousage.mousageable_type === 'days') {
          if(!mousageDay.mousages){
            mousageDay.mousages = [];
          }
          if(mousageDay.mousages.indexOf(mousage) === -1) {
            mousageDay.mousages.push(mousage);
          }
          if(dates.indexOf(mousageDay) === -1) {
            dates.push(mousageDay);            
          }
        }
      });

      return dates;
    };
  });
