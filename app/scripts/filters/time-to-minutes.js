'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:toMinutes
 * @function
 * @description
 * # toMinutes
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('timeToMinutes', function(){
    return function(hoursAndMinutes, isDate){
    	if(!hoursAndMinutes){
    		return 0;
    	}
      if(isDate){
        return ((new Date(hoursAndMinutes).getHours()*60)+(new Date(hoursAndMinutes).getMinutes()));
      }
  		return ((parseInt(hoursAndMinutes.split(':')[0])*60) + parseInt(hoursAndMinutes.split(':')[1]));
  	};
  });
