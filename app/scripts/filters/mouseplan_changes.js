'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:mousePlanChanges
 * @function
 * @description
 * # mousePlanChanges
 * Filter in the mpservicesApp.
 */

angular.module('mpservicesApp')
    .filter('mousePlanChanges', ['$filter', function() {
        return function (oldMousePlan, newMousePlan) {
            if (oldMousePlan.tripScore !== newMousePlan.tripScore) {
                return true;
            }
            if(oldMousePlan.days.length !== newMousePlan.days.length) {
                return true;
            }

            for (var day in oldMousePlan.days) {
                if (oldMousePlan.days[day].blocks.length !== newMousePlan.days[day].blocks.length) {
                    return true;
                }
                else {
                    for (var block in oldMousePlan.days[day].blocks) {
                        if (oldMousePlan.days[day].blocks[block].block_type_id !== newMousePlan.days[day].blocks[block].block_type_id) {
                            return true;
                        }
                    }
                }
            }

            return false;
        };

}]);

