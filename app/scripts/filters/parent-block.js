'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:parentBlock
 * @function
 * @description
 * # parentBlock
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('parentBlock', ['$filter', function($filter) {
    return function(block, days){
    	if(!block || !block.parent_id || !days || !days.length){
    		return null;
    	}
      var day = $filter('filter')(days, {id: block.day_id}, true);
      if(day.length){
        var parent = $filter('filter')(day[0].blocks, {id: block.parent_id}, true);
        if(parent.length){
          return parent[0];
        }
      }
      return null;
	 };
  }]);
