'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:blockOverlap
 * @function
 * @description
 * # blockOverlap
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('blockOverlap', ['$filter', function($filter) {
    return function(b, blocks){
    	if(!b || !blocks || !blocks.length){
    		return [];
    	}
    	return $filter('filter')(blocks, function(block){
        var bStart = new Date(b.startTime);
        var blockStart = new Date(block.startTime);
        var bEnd = new Date(b.endTime);
        var blockEnd = new Date(block.endTime);
        return new Date(block.startTime) < new Date(b.endTime) && new Date(block.endTime) > new Date(b.startTime);
	    });
	};
  }]);
