'use strict';

angular.module('mpservicesApp')
  .filter('numToBoolean', ['$filter', function($filter){
    return function(titles, traveler){
      if(!titles || !traveler || traveler.is_adult === null || traveler.is_adult === undefined){
        return null;
      }

      var activeTitles = [];
      for(var i=0; i<titles.length; i++){
        var isAdult = titles[i].is_adult;
        if((!traveler.is_adult && !isAdult) || (traveler.is_adult && isAdult)){
          activeTitles.push(titles[i]);
        }
      }
      return activeTitles;
    };
  }]);
