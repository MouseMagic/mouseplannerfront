'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:experiencePark
 * @function
 * @description
 * # experiencePark
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('experiencePark', function ($filter) {
    return function (experiences, park, mappings) {
    	if(!experiences || !experiences.length || !park || !mappings || !mappings.length){
    		return [];
    	}
    	var mappedValues = $filter('filter')(mappings, { park_id: park }, true);
    	return $filter('filter')(experiences, function(experience){
          var locs = (experience.relatedLocations && experience.relatedLocations.primaryLocations) ? experience.relatedLocations.primaryLocations : [experience];
			return $filter('filter')(mappedValues, function(mappingItem){
				return $filter('filter')(locs, function(loc){
					return (loc.links && (loc.links.ancestorThemePark && loc.links.ancestorThemePark.title === mappingItem.disneyParkInterestValue) || (loc.links.ancestorResortArea && loc.links.ancestorResortArea.title === mappingItem.disneyParkInterestValue)); //match any mapped disney values to this experience values
				}).length;
			}).length;
        });
	};
  });
