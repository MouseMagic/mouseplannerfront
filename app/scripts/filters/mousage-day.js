'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:mousageDay
 * @function
 * @description
 * # mousageDay
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('mousageDay', function($filter) {
    return function(mousageId, days){
    	if(!mousageId || !days){
    		return [];
    	}

      var day = $filter('filter')(days, {id: mousageId}, true);
      if(day.length) {
        return day[0];
      }
      else {
        return {id: -1};
      }
	 };
  });
