'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:phonenumber
 * @function
 * @description
 * # phonenumber
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('phonenumber', function () {
    return function (number) {
	        if (!number) { return ''; }
	        number = String(number);
	        var formattedNumber = number;
			var c = (number[0] === '1') ? '1 ' : '';
			number = number[0] === '1' ? number.slice(1) : number;
			var area = number.substring(0,3);
			var front = number.substring(3, 6);
			var end = number.substring(6, 10);
			if (front) {
				formattedNumber = (c + '(' + area + ') ' + front);	
			}
			if (end) {
				formattedNumber += ('-' + end);
			}
			return formattedNumber;
	    };
  });
