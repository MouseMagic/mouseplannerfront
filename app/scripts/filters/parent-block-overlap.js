'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:parentBlockOverlap
 * @function
 * @description
 * # parentBlockOverlap
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('parentBlockOverlap', ['$filter', function($filter) {
    return function(b, blocks){
    	if(!b || !blocks || !blocks.length){
    		return [];
    	}
      else if(blocks.length < 3){ //if block only overlaps with one other block, then return
        return blocks;
      }
      var arr = []; //initialize array with first item in other blocks
    	for(var i = 0; i < blocks.length; i++){
        if(blocks[i].id !== b.id){ //dont check the initial parent because it would return all the same blocks (which is what got passed in)
          arr.push($filter('blockOverlap')(blocks[i], blocks));
        }
      }
      return $filter('orderBy')(arr, function(subArray){
        return subArray.length; //return the array with the most overlapping items. this should be our baseline
      }, true)[0]; //reverse order
	 };
  }]);
