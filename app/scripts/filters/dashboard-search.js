'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:dashboard-search
 * @function
 * @description
 * # blockType
 * Filter in the mpservicesApp.
 */

angular.module('mpservicesApp')
    .filter('searchFor', function(){

    return function(arr, searchPlans){

        if(!searchPlans){
            return arr;
        }

        var result = [];

        searchPlans = searchPlans.toLowerCase();

        // Using the forEach helper method to loop through the array
        angular.forEach(arr, function(item){

            if(item.email.toLowerCase().indexOf(searchPlans) !== -1){
                result.push(item);
            }

        });


        return result;
    };

});