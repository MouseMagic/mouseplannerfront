'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:blockType
 * @function
 * @description
 * # blockType
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('blockType', ['$filter', function($filter) {
    return function(id, blockTypes){
    	if(!id || !blockTypes){
    		return {};
    	}
        var block = $filter('filter')(blockTypes, {id: id}, true);
        if(block.length){
            return block[0];
        }
        else{
            return {};
        }
	};
  }]);
