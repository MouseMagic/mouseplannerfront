'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:park
 * @function
 * @description
 * # park
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('park', function ($filter) {
    return function (arr, hideParks) {
    	if(!arr || !arr.length || !hideParks || !hideParks.length){
    		return [];
    	}
		  return $filter('filter')(arr, function(park) {
        return !$filter('filter')(hideParks, {park_id: park.id}, true).length;
	    });
	};
  });
