'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:parseEndTime
 * @function
 * @description
 * # parseEndTime
 * Filter in the mpservicesApp.
 * Parse endTime from 4th step for park block's endTime when endTime on the 4th step > park block's endTime per 1 day
 */

angular.module('mpservicesApp')
  .filter('parseEndTime', function($filter) {
    return function(begin,end, block){
     var endTime = (moment(begin).utc().days() < moment(end).utc().days()) ? $filter('parseUtcTime')(end, moment(block).add(1,'days')) : $filter('parseUtcTime')(end, block);

     return endTime;
    };
  });
