'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:idIndex
 * @function
 * @description
 * # idIndex
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('idIndex', function () {
    return function (arr, id, prop){ //prop is an optional parameter to specify a match on the array for a property other than arr[i].id; example: arr[i].disney_id
		if(!arr || !arr.length || !id){
			return -1;
		}
		var p = prop || 'id';
		for(var i = 0; i < arr.length; i++){
			if(arr[i][p] && arr[i][p] === id){
				return i;
			}
		}
		return -1;
	};
  });
