'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:blockProperties
 * @function
 * @description
 * # blockProperties
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('blockProperties', ['$filter', function($filter) {
    return function(b, blockTypes, day, vacation){
    	if(!b || !blockTypes || !day || !day.blocks || !vacation){
    		return {};
    	}

        var p = angular.copy($filter('blockType')(b.block_type_id, blockTypes));
        p.block_id = b.id;
        var blocksSharedTime = $filter('blockOverlap')(b, day.blocks);
        var minTimeInMinutes = 0;
        var maxTimeInMinutes = $filter('timeToMinutes')(vacation.data.endDayTime, true);
        var parent;
        if(b.parent_id){
            var parents = $filter('filter')(day.blocks, {id: b.parent_id}, true);
            if(parents.length){
                parent = parents[0];
            }
        }
        p.parentStart = function(){
            if(parent){
                return Math.max($filter('toMinutes')(parent.startTime, vacation.data.beginDayTime, b), minTimeInMinutes);
            }
            else{
                return 0;
            }
        };
        p.startTimeInMinutes = function(){
            var time = Math.max($filter('toMinutes')(b.startTime, vacation.data.beginDayTime, b)-p.parentStart(), minTimeInMinutes);
            return time;
        };
        p.endTimeInMinutes = function(){
            var time = Math.min($filter('toMinutes')(b.endTime, vacation.data.beginDayTime, b)-p.parentStart(), maxTimeInMinutes);
            return time;
        };
        p.diffTimeInMinutes = function(){
            var s = new Date(b.startTime);
            var s1 = moment($filter('parseUtcTime')(vacation.data.beginDayTime, b.startTime).valueOf() + moment().utcOffset() * 60000).toDate();
            if(s < s1){
                s = s1;
            }
            var time = parseInt((new Date(b.endTime) - s)/60000 );
            return time;
        };
        p.height = function(){
            var height = p.diffTimeInMinutes() - 2;
            return height;
        };
        p.menuHeight = 20;
        p.imageHeight = 60;

        var getNameHeight = function(height) {
          var nameHeight;
          if(parent && p.parentWidth !== '100%') {
            nameHeight = Math.max(0, height - 40);
          }

          else if (height >= 38 && height < 53) {
            nameHeight = 18;
          }

          else {
            nameHeight = height - 20;
          }

          return nameHeight;
        };

        p.fastpassHeight = function() {
          if(!(p.subtype === 'DS' || p.subtype === 'BB' || p.subtype === 'TL')){
            var fastpassContainer = angular.element(document.querySelectorAll('#block-fastpass-' + b.id));
            var height = fastpassContainer.prop('offsetHeight');
            return height;
          }

          else {
            return 0;
          }
        };

        p.class = function(){
            var blockClasses = ['object-box','js-object-box'];
            if(blocksSharedTime.length > 1){
                if(parent){
                    blockClasses.push('object-box__name--min-force');
                }
                else{
                    blockClasses.push('object-box__name--min');
                }
            }

            if(p.height() < p.menuPos() + p.menuHeight + p.imageHeight + 9) {
              blockClasses.push('no-image');
            }
            // FastPass+ will be hidden for the beta release - March, 2017
            // if (p.height() < p.menuPos() + p.menuHeight + p.imageHeight + p.fastpassHeight() + 19) {
            //   blockClasses.push('no-fastpass');
            // }

            if(p.height() < 110) {
                if(p.height() < 18){
                    blockClasses.push('extra-short');
                }
                else if(p.height() < 30){
                    blockClasses.push('short');
                }
                else if(p.height() < 60) {
                    blockClasses.push('short-med');
                }
                else {
                    blockClasses.push('med');
                }
            }

            if(parent) {
              if(p.parentWidth !== '100%') {
                blockClasses.push('narrow');
              }
            }
            else {
              if(p.width !== '100%') {
                blockClasses.push('narrow');
              }
            }

            if(p.cssClass && p.cssClass !== 'css'){
                blockClasses.push('object-box--'+p.cssClass);
            }
            if(p.name === 'fixed'){
                blockClasses.push('object-box--fixed');
            }
            if(p.name === 'resort'){
                blockClasses.push('object-box--warning');
            }
            if(p.name === 'custom'){
                blockClasses.push('object-box--custom');
            }
            if(p.name === 'park'){
                blockClasses.push('object-box--default');
                blockClasses.push('object-box--blue');
                blockClasses.push('no-fastpass');
            }
            if(p.name === 'dining'){
                blockClasses.push('object-box--primary');
            }
            if(p.name === 'transit'){
                blockClasses.push('object-box--inverse');
                blockClasses.push('object-box--transit-new');
            }

            /*
            ng-class="{
                'js-addblock': (block.block_type_id | blockType) == 'empty' && block.class != 'object-box--transparent',
                'object-box--fixed': (block.block_type_id | blockType) == 'fixed',
                'object-box--warning': (block.block_type_id | blockType) == 'resort',
                'object-box--custom': (block.block_type_id | blockType) == 'custom',
                'object-box--default': (block.block_type_id | blockType) == 'park',
                'object-box--blue': (block.block_type_id | blockType) == 'park',
                'object-box--primary': (block.block_type_id | blockType) == 'dining',
                'object-box--inverse': (block.block_type_id | blockType) == 'transit',
                'object-box--empty': (block.block_type_id | blockType) == 'empty' && block.class == 'object-box--transparent',
                'object-box__logo': (block.block_type_id | blockType) == 'logo',
                'object-box__reservation': (block.block_type_id | blockType) == 'fastpass',
                'js-object-box': (block.block_type_id | blockType) == 'park' || (block.block_type_id | blockType) == 'resort' || (block.block_type_id | blockType) == 'dining' || (block.block_type_id | blockType) == 'transit' || (block.block_type_id | blockType) == 'custom',
                'object-box--transit-new': (block.block_type_id | blockType) =='transit' }"
            */

            return blockClasses.join(' ');
        };

        var addMinutes = function(startTime, minutes){
            var dt = new Date(startTime);
            return new Date(dt.setMinutes(dt.getMinutes()+minutes));
        };

        p.menuPos = function(){
            if(parent || !b.id){ //child block
                return 0;
            }
            var menuBlock = {
                startTime: b.startTime,
                endTime: addMinutes(b.startTime, (p.menuHeight+p.imageHeight))
            };
            var children = $filter('filter')(day.blocks, {parent_id: b.id}, true);
            children = $filter('orderBy')(children, function(child){return new Date(child.startTime);}, false);
            for(var i = 0; i < children.length; i++){
                if(!$filter('blockOverlap')(menuBlock, [children[i]]).length){ //if menu block overlaps a child block, set menu block start time to child end time
                    return Math.max(($filter('toMinutes')(menuBlock.startTime, vacation.data.beginDayTime) - p.startTimeInMinutes())-p.parentStart(), 0);
                }
                menuBlock = {
                    startTime: children[i].endTime,
                    endTime: addMinutes(children[i].endTime, (p.menuHeight+p.imageHeight))
                };
            }
            return Math.max(($filter('toMinutes')(menuBlock.startTime, vacation.data.beginDayTime, b) - p.startTimeInMinutes())-p.parentStart(), 0);
        };

        p.fastpassPos = function() {
          if(parent || !b.id || !day.selectedFastpass || !day.selectedFastpass.length){ //child block
              return 0;
          }
          var blockStart = b.startTime;
          var menuPos = p.menuPos();
          var menuHeight = p.menuHeight;
          var imageHeight = p.imageHeight;
          var startFromBlock = menuPos + menuHeight + imageHeight + 19;
          var fpBlock = {
              startTime: addMinutes(blockStart, startFromBlock),
              endTime: addMinutes(b.startTime, (p.menuPos() + p.menuHeight+p.imageHeight + p.fastpassHeight() + 19))
          };
          var children = $filter('filter')(day.blocks, {parent_id: b.id}, true);
          children = $filter('orderBy')(children, function(child){return new Date(child.startTime);}, false);

          for(var i = 0; i < children.length; i++){
            var overlap = $filter('blockOverlap')(fpBlock, [children[i]]);
              if(!overlap.length){ //if menu block overlaps a child block, set menu block start time to child end time

                return 0;
              }
              fpBlock = {
                  startTime: children[i].endTime,
                  endTime: addMinutes(children[i].endTime, (p.fastpassHeight())),
                  marginTopTime: children[i].startTime
              };
            }
            var startPos = $filter('toMinutes')(fpBlock.startTime, vacation.data.beginDayTime);
            var childStart = $filter('toMinutes')(fpBlock.marginTopTime, vacation.data.beginDayTime);
            var fpMargin = startPos - childStart;
            return Math.max(fpMargin + p.fastpassHeight() + 10, 0);
        };

        var parentBlocks;
        if(parent){ //im a child block
            var parentSharedTime = $filter('blockOverlap')(parent, day.blocks);
            parentBlocks = $filter('filter')(parentSharedTime, function(block){
                return !block.parent_id;
            });
            parentBlocks = $filter('parentBlockOverlap')(parent, parentBlocks);
            p.parentWidth = parentBlocks.length > 1 ? ((100 / parentBlocks.length) - 2) + '%' : '100%';

            var childBlocks = $filter('filter')(blocksSharedTime, function(block){
                return block.parent_id === b.parent_id;
            });
            var childIndex = $filter('idIndex')(childBlocks, b.id);

            if(childIndex === -1){
                childIndex = 0;
            }
            p.width = (100/childBlocks.length) + '%';
            p.left = (((100/childBlocks.length) * childIndex) - 2) + '%';
            p.nameHeight = getNameHeight(p.height());
        }
        else{ //im a parent block
            parentBlocks = $filter('filter')(blocksSharedTime, function(block){
                return !block.parent_id;
            });
            parentBlocks = $filter('parentBlockOverlap')(b, parentBlocks);
            var parentIndex = $filter('idIndex')(parentBlocks, b.id);
            if(parentIndex === -1){
                parentIndex = 0;
            }
            p.width = parentBlocks.length > 1 ? ((100 / parentBlocks.length) - 2) + '%' : '100%';
            p.left = parentBlocks.length > 1 ? ((100 / parentBlocks.length) * parentIndex) + '%' : '0%';
            p.nameHeight = getNameHeight(p.height());
        }
        return p;
	};
  }]);
