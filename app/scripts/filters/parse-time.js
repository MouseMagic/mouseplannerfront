'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:parseTime
 * @function
 * @description
 * # parseTime
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
.filter('parseTime', [function() {
  return function(original, merge){
  	if(!original){
  		return;
  	}
  	original = new Date(original);
  	merge = merge ? new Date(merge) : new Date(original);

  	return new Date(merge.getFullYear(), merge.getMonth(), merge.getDate(), original.getHours(), original.getMinutes(), 0, 0);

 };
}]);
