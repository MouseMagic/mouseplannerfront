'use strict';

/**
 * @ngdoc filter
 * @name mpservicesApp.filter:parkInterest
 * @function
 * @description
 * # parkInterest
 * Filter in the mpservicesApp.
 */
angular.module('mpservicesApp')
  .filter('parkInterest', function ($filter) {
    return function (parks, selected) {
    	var arr = [];
    	if(!parks || !parks.length){
    		return arr;
    	}
		var values = $filter('filter')(selected, function(value) {
	    	return (value.hearts && value.hearts > 0);
	    });
	    values = $filter('orderBy')(values, 'hearts', true);
	    for(var i = 0; i < values.length; i++){
	    	var p = $filter('filter')(parks, {id: values[i].id}, true);
	    	if(p.length){
	    		arr.push(p[0]);
	    	}
	    }
	    return arr;
	};
  });
