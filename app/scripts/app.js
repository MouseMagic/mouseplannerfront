'use strict';

/**
 * @ngdoc overview
 * @name mpservicesApp
 * @description
 * # mpservicesApp
 *
 * Main module of the application.
 */
angular
  .module('mpservicesApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngMessages',
    'ngTouch',
    'datePicker',
    'datetime',
    'autocomplete',
    'duScroll',
    'ui.bootstrap',
    'ngDialog',
    'ui.router',
    'daterangepicker',
    'ngDragDrop',
    'ui.bootstrap.timepicker.custom',
    'angularResizable',
    'config',
    'angularUtils.directives.dirPagination'
  ])
  .value('duScrollDuration', 2000)
  .value('duScrollOffset', 30)
  .run(['$transitions', '$state', '$rootScope', function($transitions, $state, $rootScope){
    $transitions.onStart({}, function(){
      $rootScope.previousState = {
          Name: $state.current.name,
          Params: angular.copy($state.params),
          URL: $state.href($state.current.name, $state.params)
      };
    });

    $transitions.onError({}, function(transition){
      var routeTo = transition.to().name;
      var error = transition.error();
      if (error.type !== 5 && error.type !== 2) {
        if(routeTo === 'main.trip.wizard'){
          return $state.go('main.dashboard');
        }
        else{
          return $state.go('error', {id: 500});
        }
      }
    });

    $transitions.onBefore({to: function(state){return state.defaultSubstate != null;}}, function($transition$){
        return $state.target($transition$.to().defaultSubstate);
    });

    $transitions.onSuccess({}, function(transition) {
      var transitionName = transition.to().name;
      if (transitionName &&
        transitionName !== 'login' &&
        transitionName !== 'registration' &&
        transitionName !== 'registrationVerify' &&
        transitionName !== 'passwordReset' &&
        transitionName !== 'passwordResetEmail'
      ) {
        $rootScope.app.fullHeightPage = false;
      }
    });
}])
.config(function ($stateProvider, $sceProvider, $urlRouterProvider) {
  $sceProvider.enabled(false);

  var url = '';

  $urlRouterProvider.otherwise('/dashboard');

  $stateProvider
    .state('login',  {
      url: '/login',
      controller:'LoginCtrl',
      templateUrl:  url + 'views/partials/login.html'
    })
    .state('registration',  {
      url: '/registration',
      controller:'RegistrationCtrl',
      templateUrl:  url + 'views/partials/registration.html'
    })
    .state('registrationVerify',  {
      url: '/registration/verify/:id',
      controller:'RegistrationVerifyCtrl',
      templateUrl:  url + 'views/partials/registration-verify.html'
    })
    .state('passwordReset',  {
      url: '/password/reset/:id',
      controller:'PasswordResetCtrl',
      templateUrl:  url + 'views/partials/password-reset.html'
    })
    .state('passwordResetEmail',  {
      url: '/password/email',
      controller:'PasswordResetCtrl',
      templateUrl:  url + 'views/partials/password-reset.html'
    })
    .state('subscription',  {
      url: '/subscription?step',
      controller:'SubscriptionCtrl',
      templateUrl:  url + 'views/partials/subscription.html',
      resolve: {
        data: ['$q','mousePlannerService', '$state', function($q, mousePlannerService, $state){
          return new Promise(function (resolve, reject) {
            $q.all({user: mousePlannerService.user.me(), vacation: mousePlannerService.vacation.get()}).then(
              function(res){
                if (res.user && res.user.is_subscribed) {
                  resolve($state.go('error', {id: 404}));
                }
                resolve(res);
              },
              function(err){
                reject(err);
              }
            );
          });
        }]
      }
    })
    .state('error',  {
      url: '/error/:id',
      controller:'ErrorCtrl',
      templateUrl:  url + 'views/partials/error.html',
    })
    .state('main',  {
      abstract:true,
      template:  '<div ui-view></div>',
      controller: 'MainCtrl',
      resolve: {
        main: ['$q','mousePlannerService', function($q, mousePlannerService){
          return $q.all([mousePlannerService.user.me(), mousePlannerService.trip.get(), mousePlannerService.vacation.searchVacations(), mousePlannerService.checklist.get()]);
        }]
      }
    })
    .state('main.dashboard',  {
      url: '/dashboard?tripId',
      templateUrl:  url + 'views/general/dashboard.html',
      controller: 'DashboardCtrl'
    })

    .state('main.newtrip',  {
      url: '/new',
      abstract:true,
      templateUrl:  url + 'views/general/wizard.html',
      controller: 'TripCtrl',
      resolve: {
        init: ['$q', '$timeout', function($q, $timeout){
          var def = $q.defer();
          $timeout(function(){
            def.resolve([{}, {stepNumber: 0, celebration_types: []}]); //reolve data - only works with delay when minified
          },500);
          return def.promise;
        }]
      }
    })
    .state('main.newtrip.start',  {
      url: '/trip',
      templateUrl:  url + 'views/partials/start.html',
      controller: 'StartCtrl',
      controllerAs: 'start'
    })
    .state('main.newplan',  {
      url: '/new/:id',
      abstract:true,
      templateUrl:  url + 'views/general/wizard.html',
      controller: 'TripCtrl',
      resolve: {
        init: ['$q', 'main', '$filter', '$stateParams', '$timeout', function($q, main, $filter, $stateParams, $timeout){
          var def = $q.defer();
          $timeout(function(){
            var trips = $filter('filter')(main[1], {id: parseInt($stateParams.id)}, true);
            if(trips.length){
              def.resolve([trips[0],{stepNumber: 0, celebration_types: []}]);
            }
            else{
              def.reject('cannot find trip');
            }
          }, 500);
          return def.promise;
        }]
      }
    })
    .state('main.newplan.start',  {
      url: '/plan',
      templateUrl:  url + 'views/partials/start.html',
      controller: 'StartCtrl',
      controllerAs: 'start'
    })
    .state('main.trip',  {
      url: '/vacation/:id',
      abstract:true,
      template:  '<div ui-view></div>',
      controller: 'TripCtrl',
      resolve: {
        init: ['$q', 'main', '$filter', '$stateParams', 'mousePlannerService', function($q, main, $filter, $stateParams, mousePlannerService){
          var def = $q.defer();
          var plans = $filter('filter')(main[2].data, {id: parseInt($stateParams.id)}, true);
          if(plans.length){
            var trips = $filter('filter')(main[1], {id: plans[0].trip_id}, true);
            if(trips.length){
              mousePlannerService.vacation.mousages(plans[0].id)
              .then(
                function(res){
                  def.resolve([trips[0],plans[0],res]);
                },
                function(err){
                  console.log('could not find mousages for this plan');
                  def.resolve([trips[0],plans[0],[]]);
                }
              );
            }
            else{
              def.reject('cannot find trip');
            }
          }
          else{
            def.reject('cannot find plan');
          }
          return def.promise;
        }]
      }
    })
    .state('main.trip.edit',  {
      url: '/edit',
      onEnter: ['configPage', 'init', '$state', function(configPage, init, $state){
        if(!init || !init.length){
          return $state.target('main.dashboard');
        }
        if(init[1].mouse_plan){
          return $state.target('main.trip.scheduler', {id: init[1].id});
        }
        return $state.target(configPage.navigation[parseInt(init[1].stepNumber)].state, {id: init[1].id});
      }]
    })
    .state('main.trip.wizard', {
      abstract: true,
      templateUrl: url + 'views/general/wizard.html',
      resolve: {
        completed: ['$q', 'init', function($q, init){
          return true;
        }]
      }
    })
    .state('main.trip.wizard.start', {
      url: '/start',
      templateUrl: url + 'views/partials/start.html',
      controller: 'StartCtrl',
      controllerAs: 'start'
    })
    .state('main.trip.wizard.guest', {
      url: '/guest',
      templateUrl: url + 'views/partials/guest.html',
      controller: 'GuestCtrl',
      controllerAs: 'guest'
    })
    .state('main.trip.wizard.travel', {
      url: '/travel',
      templateUrl: url + 'views/partials/travel.html',
      controller: 'TravelCtrl'
    })
    .state('main.trip.wizard.preferences', {
      url: '/preferences',
      templateUrl: url + 'views/partials/preferences.html',
      controller: 'PreferencesCtrl'
    })
    .state('main.trip.wizard.activities', {
      url: '/activities',
      templateUrl: url + 'views/partials/activities.html',
      controller: 'ActivitiesCtrl'
    })
    .state('main.trip.wizard.wishlist', {
      url: '/wishlist',
      templateUrl: url + 'views/partials/wishlist.html',
      controller: 'WishlistCtrl',
      resolve: {
        experience_lists: ['$q', 'mousePlannerService', 'init', function($q, mousePlannerService, init){
          return $q.all([
            mousePlannerService.vacation.wishlist(),
            mousePlannerService.vacation.experiences(init[1].id),
            mousePlannerService.vacation.defaultExperiences()
          ]);
        }]
      }
    })
    .state('main.trip.scheduler',  {
      url: '/scheduler',
      templateUrl:  url + 'views/general/scheduler.html',
      controller: 'SchedulerCtrl',
      resolve: {
        scheduler: ['$q','mousePlannerService', 'init', function($q, mousePlannerService, init){
          return $q.all([mousePlannerService.block_type.get(), mousePlannerService.vacation.mousages(init[1].id), mousePlannerService.checklist.get()]);
        }]
      }
    });
})
.config(['$httpProvider', function($httpProvider) {
  $httpProvider.interceptors.push('Auth');
}]);
